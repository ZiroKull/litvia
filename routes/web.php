<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// imports
Route::get('/admin/import', [App\Http\Controllers\Admin\ImportController::class, 'index'])->name('import.index');
Route::get('/admin/import/importB1', [App\Http\Controllers\Admin\ImportController::class, 'importB1'])->name('import.importB1');
Route::get('/admin/import/importPigu', [App\Http\Controllers\Admin\ImportController::class, 'importPigu'])->name('import.importPigu');
Route::get('/admin/import/importCompetitorsPrices', [App\Http\Controllers\Admin\ImportController::class, 'importCompetitorsPrices'])->name('import.importCompetitorsPrices');
Route::get('/admin/import/generateXml', [App\Http\Controllers\Admin\ImportController::class, 'generateXml'])->name('import.generateXml');
Route::get('/admin/import/downloadXml', function (){
    $xml = Storage::get('xml/1.xml');
    //var_dump($xml); die;
    return response()->make($xml, 200)->header('Content-Type', 'application/xml');
    //return Storage::download('xml/1.xml', '1.xml', $headers);
});
Route::get('/admin/import/generateDownloadXls', [App\Http\Controllers\Admin\ImportController::class, 'generateDownloadXls'])->name('import.generateDownloadXls');
Route::get('/admin/import/clearParserTable', [App\Http\Controllers\Admin\ImportController::class, 'clearParserTable'])->name('import.clearParserTable');


// products
Route::get('/admin/products', [App\Http\Controllers\Admin\ProductsController::class, 'index'])->name('products.index');
Route::get('/admin/products/getProducts', [App\Http\Controllers\Admin\ProductsController::class, 'getProducts'])->name('products.getProducts');
Route::get('/admin/products/searchProductLink', [App\Http\Controllers\Admin\ProductsController::class, 'searchProductLink'])->name('products.searchProductLink');
Route::get('/admin/products/getProductPricesById', [App\Http\Controllers\Admin\ProductsController::class, 'getProductPricesById'])->name('products.getProductPricesById');
Route::patch('/admin/products/update/{id}', [App\Http\Controllers\Admin\ProductsController::class, 'update'])->name('products.update');

// product links
Route::get('/admin/products/links/{id}', [App\Http\Controllers\Admin\ProductLinksController::class, 'show'])->name('productLinks.show');
Route::get('/admin/products/links/{id}/getLinks', [App\Http\Controllers\Admin\ProductLinksController::class, 'getLinks'])->name('productLinks.getLinks');
Route::post('/admin/products/links/{id}/storeLinks', [App\Http\Controllers\Admin\ProductLinksController::class, 'store'])->name('productLinks.store');
Route::post('/admin/products/links/deleteProductLink', [App\Http\Controllers\Admin\ProductLinksController::class, 'destroy'])->name('productLinks.delete');
Route::get('/admin/products/links/{id}/getSupplierProductLinks', [App\Http\Controllers\Admin\ProductLinksController::class, 'getSupplierProductLinks'])->name('productLinks.getSupplierProductLinks');

// price and sell
Route::get('/admin/priceAndSell', [App\Http\Controllers\Admin\PriceAndSellController::class, 'index'])->name('priceAndSell.index');
Route::get('/admin/priceAndSell/getPriceAndSell', [App\Http\Controllers\Admin\PriceAndSellController::class, 'getPriceAndSell'])->name('priceAndSell.getPriceAndSell');
Route::post('/admin/priceAndSell/update', [App\Http\Controllers\Admin\PriceAndSellController::class, 'update'])->name('getPriceAndSell.update');

// competitors
Route::get('/admin/competitors', [App\Http\Controllers\Admin\CompetitorsController::class, 'index'])->name('competitors.index');
Route::get('/admin/competitors/getCompetitors', [App\Http\Controllers\Admin\CompetitorsController::class, 'getCompetitors'])->name('competitors.getCompetitors');
Route::post('/admin/competitors/store', [App\Http\Controllers\Admin\CompetitorsController::class, 'store'])->name('competitors.store');
Route::post('/admin/competitors/update', [App\Http\Controllers\Admin\CompetitorsController::class, 'update'])->name('competitors.update');
Route::post('/admin/competitors/delete', [App\Http\Controllers\Admin\CompetitorsController::class, 'destroy'])->name('competitors.destroy');
Route::post('/admin/competitors/checkUniqueUrl', [App\Http\Controllers\Admin\CompetitorsController::class, 'checkUniqueUrl'])->name('competitors.checkUniqueUrl');

// before purchase
Route::get('/admin/beforePurchase', [App\Http\Controllers\Admin\BeforePurchaseController::class, 'index'])->name('beforePurchase.index');
Route::get('/admin/beforePurchase/getPurchase', [App\Http\Controllers\Admin\BeforePurchaseController::class, 'getPurchase'])->name('beforePurchase.getPurchase');
Route::get('/admin/beforePurchase/getHistoryBySku/{sku}', [App\Http\Controllers\Admin\BeforePurchaseController::class, 'getHistoryBySku'])->name('beforePurchase.getHistoryBySku');

// suppliers
Route::get('/admin/suppliers', [App\Http\Controllers\Admin\SuppliersController::class, 'index'])->name('suppliers.index');
Route::get('/admin/suppliers/getSuppliers', [App\Http\Controllers\Admin\SuppliersController::class, 'getSuppliers'])->name('suppliers.getSuppliers');
Route::get('/admin/suppliers/{id}/getSupplierLinks', [App\Http\Controllers\Admin\SuppliersController::class, 'getSupplierLinks'])->name('suppliers.getSupplierLinks');
Route::get('/admin/suppliers/create', [App\Http\Controllers\Admin\SuppliersController::class, 'create'])->name('suppliers.create');
Route::post('/admin/suppliers/store', [App\Http\Controllers\Admin\SuppliersController::class, 'store'])->name('suppliers.store');
Route::get('/admin/suppliers/edit/{id}', [App\Http\Controllers\Admin\SuppliersController::class, 'edit'])->name('suppliers.edit');
Route::patch('/admin/suppliers/update/{id}', [App\Http\Controllers\Admin\SuppliersController::class, 'update'])->name('suppliers.update');
Route::post('/admin/suppliers/open', [App\Http\Controllers\Admin\SuppliersController::class, 'open'])->name('suppliers.open');
Route::get('/admin/suppliers/searchSmallestGroup', [App\Http\Controllers\Admin\SuppliersController::class, 'searchSmallestGroup'])->name('suppliers.searchSmallestGroup');
Route::post('/admin/suppliers/addSmallestGroup', [App\Http\Controllers\Admin\SuppliersController::class, 'addSmallestGroup'])->name('suppliers.addSmallestGroup');

// hs codes
Route::get('/admin/hs-codes', [App\Http\Controllers\Admin\HSCodesController::class, 'index'])->name('hscodes.index');
Route::get('/admin/hs-codes/getHSCodes', [App\Http\Controllers\Admin\HSCodesController::class, 'getHSCodes'])->name('hscodes.getHSCodes');
Route::post('/admin/hs-codes/store', [App\Http\Controllers\Admin\HSCodesController::class, 'store'])->name('hscodes.store');
Route::post('/admin/hs-codes/update', [App\Http\Controllers\Admin\HSCodesController::class, 'update'])->name('hscodes.update');

// product details
Route::get('/admin/products/links/product/{id}', [App\Http\Controllers\Admin\ProductPageController::class, 'show'])->name('productPage.show');
Route::patch('/admin/products/links/product/update/{id}', [App\Http\Controllers\Admin\ProductPageController::class, 'update'])->name('productPage.update');

// pigu fee
Route::get('/admin/pigu-fees/', [App\Http\Controllers\Admin\PiguFeesController::class, 'index'])->name('piguFee.index');
Route::get('/admin/pigu-fees/getPiguFees', [App\Http\Controllers\Admin\PiguFeesController::class, 'getPiguFees'])->name('piguFee.getPiguFees');
Route::post('/admin/pigu-fees/store', [App\Http\Controllers\Admin\PiguFeesController::class, 'store'])->name('piguFee.store');
Route::post('/admin/pigu-fees/update', [App\Http\Controllers\Admin\PiguFeesController::class, 'update'])->name('piguFee.update');

// settings
Route::get('/admin/settings/other', [App\Http\Controllers\Admin\SettingsController::class, 'other'])->name('settings.other');
Route::post('/admin/settings/update', [App\Http\Controllers\Admin\SettingsController::class, 'update'])->name('settings.update');

// menu warnings
Route::get('menuWarnings', [App\Http\Controllers\Admin\MenuWarningsController::class, 'getWarnings']);
