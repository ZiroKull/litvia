$(document).ready(function () {
    let sku;
    let table = $('#before-purchase-table').DataTable({
        orderCellsTop: true,
        "autoWidth": false,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "searching": true,
        "lengthChange": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/beforePurchase/getPurchase',
            type: "GET",
            dataType: 'json',
            data: function (data) {
                serviceClass.filter('#before-purchase-table',data);
            }
        },
        columns: [
            {data: 'local_vendor_name', orderable: false, "width": "20%"},
            {data: 'large_group', orderable: false, searchable: false},
            {data: 'product_name'},
            {data: 'local_name', orderable: true,},
            {data: 'pigu_product_count', orderable: true,},
            {data: 'many_days_product_last', orderable: true,},
            {data: 'avg_sales', orderable: true,},
            {data: 'average_calculation_started_from', orderable: true,},
            {data: 'action', orderable: false, searchable: false}
        ],
        initComplete: function () {
            let api = this.api();
            //initFields(api);
            serviceClass.initFields(api, {disableColumns: 9});
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[2, "asc"]],
        "processing": true,
        "serverSide": true
    });

    $(document).on("click", ".open-sku-history", function () {
        sku = $(this).attr('data-sku');

        $('#before-purchase-modal').modal('show');
    });

    $('#before-purchase-modal').on('shown.bs.modal', function (e) {
        if(sku !== undefined) {
            $.ajax({
                method: "GET",
                url: "/admin/beforePurchase/getHistoryBySku/" + sku,
            })
                .done(function (msg) {
                    let html = '<p>Товар продавался <strong>'+msg.count+'</strong> дней.</p>' +
                               '<p>За этот период продано  <strong>'+msg.salesCount+'</strong> штук</p>' +
                               '<p>Средние продажи расчитаны за переод между<br><strong>'+msg.resDates+'</strong></p>';
                    $('#before-purchase-modal').find('.modal-body').html(html);
                    $('#before-purchase-modal .overlay').hide();
                    console.log(msg);
                });
        }
    });

    $('#before-purchase-modal').on('hidden.bs.modal', function (e) {
        $(this).find('.modal-body').html('');
        $('#before-purchase-modal .overlay').show();
    });

});