let infoText;

// initialize and autoload classes
let serviceClass = new service();

$(document).ready(function () {

    serviceClass.checkMenuWarnings();

    $( document ).tooltip({
        content: function () {
            return $( this ).prop( 'title' ).replaceAll( '|', '<br />' );
        }
    });

    // initialize tooltip
    if (typeof tooltips !== 'undefined') {
        if (tooltips[0]) {
            //console.log(tooltips[0],1111);
            $.each(tooltips[0], function (index, value) {
                if ($('#' + index).length) {
                    $('.tooltipable').find('#' + index).after('<i class="ml-1 far fa-fw fa-question-circle"></i>');

                }
            });
        }
    }

    // menu tooltips
    if (typeof tooltipsMenu !== 'undefined') {
        if (tooltipsMenu[0]) {
            console.log(tooltipsMenu[0]);
            $.each(tooltipsMenu[0], function (index, value) {
                $('.sidebar').find('.' + index + ' p').append('<i class="ml-1 far fa-fw fa-question-circle"></i>');
            });
        }
    }

    $('.fa-question-circle').click(function (event) {
        event.preventDefault();
        event.stopPropagation();

        //for fields
        let id = $(this).attr('id');
        let tableId = $(this).prev().attr('id');
        let menuId = $(this).parent().parent().parent().find('.nav-link').attr('class');


        if (typeof id !== 'undefined') {
            if (typeof tooltips !== 'undefined') {
                infoText = tooltips[0][id];
            }
        } else if (typeof tableId !== 'undefined') {
            if (typeof tooltips !== 'undefined') {
                infoText = tooltips[0][tableId];
            }
        } else if (typeof menuId !== 'undefined') {
            if (typeof tooltipsMenu !== 'undefined') {
                let classArr = menuId.split(/\s+/);
                infoText = tooltipsMenu[0][classArr[1]];
            }
        }

        //console.log(tooltips[0][id], id, tableId);
        $('#info-modal').modal('show');

        return false;
    });

    $('#info-modal').on('shown.bs.modal', function (e) {
        $('#info-modal .overlay').hide();
        $(this).find('.modal-body').html('<p>' + infoText + '</p>');
    });

    $('#info-modal').on('hidden.bs.modal', function (e) {
        $(this).find('.modal-body').html('');
        $('#info-modal .overlay').show();
    });

});
