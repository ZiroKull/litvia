let service = function (options) {

    let pattern = /^[0-9]+[.,]?[0-9]*$/g;

    /*
     * Variables accessible
     * in the class
     */
    let vars = {
        params: {}
    };

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    let root = this;

    /*
     * Constructor
     */
    this.construct = function (options) {
        $.extend(vars, options);
        this.validateNumericFields();
        this.validateIntFields();
    };

    /*
     * Public method
     * Can be called outside class
     */
    this.initFields = function (api, params) {

        if (params.disableColumns !== undefined) {
            $('.filterhead', api.table().header()).each(function (i) {

                // let col = api.column( i ).header();
                // console.log(col);
                // generate filter input
                if($(this).hasClass('filter')) {
                    generateFilterSelect($(this), i, params);
                }

                // generate inputs
                if ($(this).hasClass('input')) {
                    generateInput($(this), api, params, i);
                } else if ($(this).hasClass('select')) {
                    generateSelect($(this), api, params, i);
                }

            });
        } else {
            console.error('Please add params example {disableColumns: 12}');
        }

        processActions(api);

    };

    this.filter = function (id, data) {
        //data.data = 'test';
        let col = $(id+' tr:nth-child(1) th').length;
        //let fieldVal = $("select[data-index='2']").val();
        //console.log(data.columns[2].search);
        //console.log(col);
        for(let i = 0; i < col; i++){
            //console.log("select[data-index='"+i+"']");
            let isElementFilterSelect =  $("select[data-index='"+i+"']").length;

            if(isElementFilterSelect){
                data.columns[i].search.param = $("select[data-index='"+i+"']").val();
            } else {
                data.columns[i].search.param = '';
            }
            //console.log(data.columns[i].search);

        };

        //console.log(fieldVal);
        //console.log(col);
        return data;
    };

    this.hideSearchInputs = function (columns) {
        for (let i = 0; i < columns.length; i++) {
            if (columns[i]) {
                $('.filterhead:eq(' + i + ')').show();
            } else {
                $('.filterhead:eq(' + i + ')').hide();
            }
        }
    };

    this.prepareEditorData = function(editor) {
        let that = this;
        editor.on( 'preSubmit', function ( e, data, action ) {
            Object.keys( data.data ).forEach(key => {
                Object.getOwnPropertyNames( data.data[key] ).forEach(name => {
                    let value = that.replaceNumberWithCommaToDot(data.data[key][name]);

                    if ( typeof value === 'number' && !isNaN(value)) {
                        data.data[key][name] = that.replaceNumberWithCommaToDot(data.data[key][name]);
                    }
                });
            });
        } );
    };

    /*
     * Private method
     * Can only be called inside class
     */
    let generateInput = function (object, api, params, i) {
        if (params.disableColumns != i) {
            let countWithHidden = (params.hidden !== undefined) ? i + params.hidden : i;
            let column = api.column(countWithHidden);
            $('<input class="form-control" style="width: 100%;">').appendTo(object)
                .on('change', function () {
                    $('.ui-tooltip').remove();
                    column.search($(this).val(), false, false, true).draw();
                });
        }
    };

    let generateSelect = function (object, api, params, i) {
        let column = api.column(i);
        let select = $('<select class="form-control"><option value="0">Все</option></select>').appendTo(object).on('change', function () {
            column.search($(this).val(), false, false, true).draw();
        });

        if (i == 11 || object.hasClass('selectOptions')) {
            $.each(selectOptions, function (key, value) {
                select.append('<option value="' + key + '">' + value + '</option>');
            });
        } else if (object.hasClass('selectOptionsQuality')) {
            $.each(selectOptionsQuality, function (key, value) {
                select.append('<option value="' + key + '">' + value + '</option>');
            });
        } else {
            $.each(selectOptions2, function (key, value) {
                select.append('<option value="' + key + '">' + value + '</option>');
            });
        }
    };

    let generateFilterSelect = function (object, i, params) {
        if (params.disableColumns != i) {
            let countWithHidden = (params.hidden !== undefined) ? i + params.hidden : i;

            let filterSelect = $('<select data-index="' + countWithHidden + '" class="form-control filter-select"><option value="~">- Включает</option></select>').appendTo(object);

            if (filterOptions) {
                $.each(filterOptions, function (key, value) {
                    filterSelect.append('<option value="' + key + '">' + key + ' - ' + value + '</option>');
                });
            }
        }



    };

    let processActions = function (api) {

        $('.filter-select').on('change', function() {

            let val = $(this).val();
            let i = $(this).attr('data-index');
            let column =  api.column(i);
            //console.log(val, (val === ''));

            if(val !== '#' && val !== '!#') {
                $(this).next().prop("disabled", false);
                $(this).next().val('');
            } else {
                column.search(null, false, false, true).draw();
                $(this).next().prop("disabled", true);
                $(this).next().val('');
            }

            if(val === ''){
                column.search(false, false, false, true).draw();
            }
        });

        $('.input input').on('click', function(){
            $(this).select();
        });

    };

    this.checkMenuWarnings = function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            method: "GET",
            url: "/menuWarnings",
        })
        .done(function (data) {
            Object.keys( data ).forEach(key => {
                if (data[key]) {
                    $('.' + key).find('.fa-question-circle').append('<i class="ml-1 fas fa-exclamation-triangle text-danger"></i>');
                } else {
                    $('.' + key + ' .fa-exclamation-triangle').remove();
                }
            });
        });
    };

    this.validateNumericFields = function() {
        $(document).on("keypress", '.numeric-type', function (event) {
            let keyConditionOnlyInt = event.which >= 48 && event.which < 58;
            let checkExistDorOrComma = $(this).val().indexOf('.') != -1 || $(this).val().indexOf(',') != -1;
            let keyPressDotOrComma = event.which === 44 || event.which === 46;

            if ((!keyConditionOnlyInt && !keyPressDotOrComma) || (keyPressDotOrComma && checkExistDorOrComma)) {
                event.preventDefault();
            }
        });
    }

    this.validateIntFields = function() {
        $(document).on("keypress", '.int-type', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    }

    this.setNumericTypeToField = function(names = []) {
        names.forEach(name => {
            editor.field(name).input().addClass('numeric-type');
        });
    },

     this.replaceNumberWithCommaToDot = function(val) {
         if (val.match(pattern)) {
             return parseFloat(val.replaceAll(',', '.'));
         }

         return val;
    }

    /*
     * Pass options when class instantiated
     */
    this.construct(options);

};



