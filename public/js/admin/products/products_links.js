$(document).ready(function () {
    let productId = $('input[name="product_id"]').val();
    let productLinkId;

    let table = $('#products-links-table').DataTable({
        orderCellsTop: true,
        "autoWidth": false,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "searching": true,
        "lengthChange": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/products/links/'+productId+'/getLinks',
            type: "GET",
            dataType: 'json',
            data:  function (data) {
            }
        },
        columns: [
            {data: 'local_sku',  orderable: true, "width": "20%"},
            {data: 'local_name', orderable: true, searchable: false},
            {data: 'pigu_barcode'},
            {data: 'pigu_product_count', orderable: true,},
            {data: 'action', orderable: false, searchable: false}
        ],
        "order": [[1, "asc"]],
        "processing": true,
        "serverSide": true
    });

    let $modal = $('#myModalDelete');

    $("#products-links-table").on('click', '.del_', function (e) {
        e.preventDefault();
        $modal.modal('show');
        productLinkId = $(this).attr('data-id');
        console.log(productLinkId);
    });

    $('#delete-product-link').click(function () {
        console.log('delete cover!', productLinkId);
        if (productLinkId !== null) {
            $.ajax(
                {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: "/admin/products/links/deleteProductLink",
                    data: {id: productLinkId}
                }).done(function (msg) {
                console.log(msg);
                table.ajax.reload();
            });

        }
    });


});