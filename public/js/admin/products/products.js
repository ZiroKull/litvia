$(document).ready(function () {

    let table = $('#products-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "searching": true,
        "lengthChange": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/products/getProducts',
            type: "GET",
            dataType: 'json',
            data: function (data) {
               serviceClass.filter('#products-table', data);
            }
        },
        columns: [
            {data: 'b1_id', orderable: true},
            /*{data: 'change_user_id', orderable: true, searchable: false,},
            {data: 'change_user_date'},
            {data: 'read_user_id', orderable: true,},
            {data: 'updated_at', orderable: true,},*/
            {data: 'product_name', name: 'product_name', orderable: true, searchable: true},
            {data: 'cost_per_documents', orderable: true},
            {data: 'balance_of_documents', orderable: true},
            {data: 'local_sku', orderable: true},
            {data: 'local_name', orderable: true},
            {data: 'pigu_barcode', orderable: true},
            {data: 'pigu_product_count', orderable: true},
            /*{data: 'action', orderable: false, searchable: false}*/
        ],
        initComplete: function () {
            let api = this.api();
            serviceClass.initFields(api, {disableColumns: 12})
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[0, "desc"]],
        "processing": true,
        "serverSide": true
    });

    table.on('responsive-resize', function (e, datatable, columns) {
        serviceClass.hideSearchInputs(columns);
    });

});