$(document).ready(function () {

    let table = $('#suppliers-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "autoWidth": false,
        "searching": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/suppliers/getSuppliers',
            type: "GET",
            dataType: 'json',
            data:  function (data) {
            }
        },
        columnDefs: [
            { "width": "33%", "targets": "_all" }
        ],
        columns: [
            {data: 'local_company_name'},
            {data: 'legal_company_name'},
            {data: 'manager_name'},
            {data: 'action', orderable: false, searchable: false}
        ],
        initComplete: function () {
            let api = this.api();
            serviceClass.initFields(api, {disableColumns: 12})
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[0, "desc"]],
        "processing": true,
        "serverSide": true
    });

    table.on('responsive-resize', function ( e, datatable, columns ) {
        serviceClass.hideSearchInputs( columns );
    } );

    $( document ).on( "click", ".supplier-link", function() {
        let that = $(this);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: "/admin/suppliers/open",
            data: {
                'id': $(this).data('id'),
                'url': $(this).attr('href')
            },
            success: function(data) {
                if(data.status === 'ok') {
                    window.open( that.attr('href') );
                }
            }
        });

        return false;
    });

});
