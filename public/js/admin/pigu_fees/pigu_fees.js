$(document).ready(function () {

    editor = new $.fn.dataTable.Editor({
        ajax: {
            url: "/admin/pigu-fees/update",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        table: "#pigu-fees-table",
        idSrc: 'id',
        fields: [
            {
                label: "Название pigu категории",
                name: "category_name",
            },
            {
                label: "Комиссия pigu",
                name: "pigu_fee",
            },
        ],
        i18n: {
            edit: {
                button: "Сохранить",
                title: "Сохранить изменения",
                submit: "Сохранить"
            },
        }
    });

    serviceClass.prepareEditorData(editor);
    serviceClass.setNumericTypeToField(['pigu_fee']);

    // Activate an inline edit on click of a table cell
    $('#pigu-fees-table').on('click', 'tbody td:not(:first-child):not(.not-editable)', function (e) {
        editor.inline(
            table.cell(this).index(),
            {
                onBlur: 'submit'
            });
    });

    editor.on( 'submitSuccess', function ( e, data, action ) {
        serviceClass.checkMenuWarnings();
    });

    let table = $('#pigu-fees-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "autoWidth": false,
        "searching": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/pigu-fees/getPiguFees',
            type: "GET",
            dataType: 'json',
            data:  function (data) {
                serviceClass.filter('#pigu-fees-table',data);
            }
        },
        columns: [
            {data: 'category_number'},
            {data: 'category_name'},
            {data: 'pigu_fee'},
        ],
        initComplete: function () {
            let api = this.api();
            serviceClass.initFields(api, {disableColumns: 12})
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[0, "desc"]],
        "processing": true,
        "serverSide": true
    });

    table.on('responsive-resize', function ( e, datatable, columns ) {
        serviceClass.hideSearchInputs( columns );
    } );

});
