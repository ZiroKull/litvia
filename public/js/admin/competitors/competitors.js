let status;

$(document).ready(function () {

    let finded = false;
    let selectedProductId = null;
    let selectedProductName = null;
    let competitor_id = null;
    let rowData;
    let conf = {disableColumns: 11, hidden: 1};

    editor = new $.fn.dataTable.Editor({
        ajax: {
            url: "/admin/competitors/update",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        table: "#competitors-table",
        idSrc: 'id',
        fields: [
            {
                label: "Коэффециент",
                name: "coefficient_price",
            },
        ],
        i18n: {
            edit: {
                button: "Сохранить",
                title: "Сохранить изменения",
                submit: "Сохранить"
            },
        }
    });

    serviceClass.prepareEditorData(editor);
    serviceClass.setNumericTypeToField(['coefficient_price']);

    let editIcon = function (data, type, row) {
        if (type === 'display') {
            return data + ' <i class="fas fa-fw fa-pencil-alt coef-edit"></i>';
        }
        return data;
    };

    let table = $('#competitors-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "searching": true,
        "lengthChange": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/competitors/getCompetitors',
            type: "GET",
            dataType: 'json',
            data: function (data) {
                serviceClass.filter('#competitors-table', data);
            }
        },
        columns: [
            {data: 'id', orderable: true, visible: false, searchable: false},
            {data: 'local_name', orderable: true},
            {data: 'minimal_price', name: 'product_prices.minimal_price', orderable: true},
            {data: 'target_price', name: 'product_prices.target_price'},
            //{data: 'price_without_discount', orderable: true,},
            {data: 'selling_price', name: 'product_prices.selling_price', orderable: true},
            {data: 'type', orderable: true, searchable: true},
            {
                data: 'coefficient_price', orderable: true,
                render: editIcon
            },
            {data: 'competitor_name', orderable: true},
            {data: 'competitor_price', orderable: true},
            {data: 'competitor_hours_delivery', orderable: true},
            {data: 'competitor_min_order_amount', orderable: true},
            {data: 'action', orderable: false, searchable: false}
        ],
        initComplete: function () {
            let api = this.api();
            serviceClass.initFields(api, conf);
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[0, "desc"]],
        "processing": true,
        "serverSide": true
    });

    $('#competitors-table').on('click', 'tbody td i.coef-edit', function (e) {
        rowData = table.row($(this).parent()).data();
        editor.bubble($(this).parent());
    });

    editor.on('open', function (e, mode, action) {
        //console.log(1244);
        $('#DTE_Field_coefficient_price').select();
        let val = $('#DTE_Field_coefficient_price').val();
        let coefficientPrice = (rowData.coefficient_price) ? rowData.coefficient_price : 0;
        let coefficientPriceTotal = Math.floor((rowData.coefficient_price * rowData.competitor_price).toFixed(3) * 100) / 100;
        let competitorPrice = (rowData.competitor_price) ? rowData.competitor_price : 0;

        $('.info-container').remove();
        $('#DTE_Field_coefficient_price').after('<div class="info-container"><b>Цена конкурента:</b> <span class="coefficient_price_val">' + competitorPrice + '</span> <br><b>Коэффициент*цена конкурента=</b><span class="coefficient_price_total">' + coefficientPriceTotal + '</span></div>');
    });

    $(document).on("keyup", '#DTE_Field_coefficient_price', function () {

        console.log(rowData);
        let val = $(this).val();
        let coefficientPriceTotal = Math.floor((val * rowData.competitor_price).toFixed(3) * 100) / 100;
        let competitorPrice = (rowData.competitor_price) ? rowData.competitor_price : 0;
        $('.coefficient_price_val').html(competitorPrice);
        $('.coefficient_price_total').html(coefficientPriceTotal);
        console.log(val);
    });

    table.on('responsive-resize', function (e, datatable, columns) {
        serviceClass.hideSearchInputs(columns);
    });

    $('#local-name').autocomplete({
        source: function (request, response) {

            console.log(request);

            let url = $('#url').val();
            checkUniqueUrl(request.term,url);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "GET",
                url: " /admin/products/searchProductLink",
                data: {q: request.term}
            })
                .done(function (data) {
                    console.log(data);

                    response(data);
                });

        },
        minLength: 2,
        select: function (event, ui) {
            finded = false;
            console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
            if (ui.item.id !== undefined) {
                let url = '';
                url = $('#url').val();
                selectedProductId = ui.item.id;
                selectedProductName = ui.item.value;
                finded = true;
                checkHost(url);
                checkUniqueUrl(selectedProductName,url);
                console.log(url);
            }


        },
        response: function (event, ui) {
            if (!ui.content.length) {
                let noResult = {value: "", label: "Ничего не найдено"};
                ui.content.push(noResult);
            }
        }
    });

    $('#local-name').change(function () {

        let localName = $(this).val();
        let url = $('#url').val();

        if (localName) {
            checkHost(url);
            checkUniqueUrl(localName,url);
        } else {
            $('#minimal_price').val('0.00');
            $('#target_price').val('0.00');
            $('#selling_price').val('0.00');
        }
    });

    $('#url').change(function () {
        //console.log();
        let url = $(this).val();

        if (url) {
            checkHost(url);
        } else {
            $('#minimal_price').val('0.00');
            $('#target_price').val('0.00');
            $('#selling_price').val('0.00');
        }


    });


    $("#url").bind('paste', function (e) {

        e.preventDefault();

        if (navigator.clipboard) {
            navigator.clipboard.readText()
                .then(text => {
                    // `text` содержит текст, прочитанный из буфера обмена
                    if (text) {
                        $("#url").val(text);
                        let name = $('#local-name').val();
                        checkHost(text);
                        checkUniqueUrl(name,text);

                        console.log(status);
                    } else {
                        $('#minimal_price').val('0.00');
                        $('#target_price').val('0.00');
                        $('#selling_price').val('0.00');
                    }
                })
                .catch(err => {
                    // возможно, пользователь не дал разрешение на чтение данных из буфера обмена
                    console.log('Something went wrong', err);
                });
        } else {
            // поддержки нет. Придётся пользоваться execCommand или не включать эту функцию.
        }
    });

    $("#url").bind('cut', function (e) {
        $('#minimal_price').val('0.00');
        $('#target_price').val('0.00');
        $('#selling_price').val('0.00');
    });

    var $modal = $('#myModalDelete');

    // show the modal when delete button clicked
    $('#competitors-table').on('click', '.del_', function (e) {
        e.preventDefault();
        e.stopPropagation();
        competitor_id = $(this).attr('data-id');
        $modal.modal('show');
    });

    $('#delete-product-link').click(function () {

        console.log('delete competitor!', competitor_id);

        if (competitor_id !== null) {
            $.ajax(
                {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: "/admin/competitors/delete",
                    data: {id: competitor_id}
                }).done(function (msg) {
                console.log(msg);
                table.ajax.reload();
            });
        }

    });

    $('.field-button').click(function () {
        // clear fields
        $('#local-name').val('');
        $('#coefficient_price').val('0.9999');
        $('#minimal_price').val('0.00');
        $('#target_price').val('0.00');
        $('#selling_price').val('0.00');
    });


    $(".competitors .btn").click(function (event) {
        event.preventDefault();
        let name = $('#local-name').val();
        let url = $('#url').val();

        checkUniqueUrl(name , url);

        return true;
    });

    $(document).on('uniqueUrlEvent', function (e, arg1) {
        $(document).on('click', '.competitors .btn', function (e) {
            e.preventDefault();
            if (arg1.callback) {
                console.log(arg1.callback);
                $('.competitors').submit();
            }
        });
        if (!arg1.callback) {
            $("#url").val('');
        }
        return true;
    });

    function checkUniqueUrl(name, url) {
        if (name && url) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: " /admin/competitors/checkUniqueUrl",
                data: {url: url, local_name: name}
            })
                .done(function (data) {
                    console.log(data);
                    if (data.count) {
                        toastr.error('Данный конкурент уже добавлен к этому продукту', 'Ошибка');
                        $(document).trigger('uniqueUrlEvent', {callback: false});
                    } else {
                        $(document).trigger('uniqueUrlEvent', {callback: true});
                    }
                });
        }
        //return status;
    }

    function checkHost(url) {
        if (url) {

            let hosts = ['pigu.lt', '220.lv', 'kaup24.ee', 'hobbyhall.fi'];

            try {
                const remoteUrl = new URL(url);

                if (remoteUrl.hostname) {
                    console.log(remoteUrl.hostname);
                    let exist = hosts.indexOf(remoteUrl.hostname);
                    console.log(exist);
                    if (exist < 0) {
                        toastr.error('Введите верный URL (pigu.lt, 220.lv, kaup24.ee, hobbyhall.fi)', 'Ошибка');
                    } else {
                        let name = (selectedProductName) ? selectedProductName : $('#local-name').val();
                        if (name && url) {
                            console.log('get params', remoteUrl.hostname.split('.').pop());
                            let type = remoteUrl.hostname.split('.').pop();

                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                method: "GET",
                                url: " /admin/products/getProductPricesById",
                                data: {type: type, local_name: name}
                            })
                                .done(function (data) {
                                    console.log(data['minimal_price']);
                                    if (data['minimal_price'])
                                        $('#minimal_price').val(data['minimal_price']);
                                    if (data['target_price'])
                                        $('#target_price').val(data['target_price']);
                                    if (data['selling_price'])
                                        $('#selling_price').val(data['selling_price']);
                                });
                        } else {
                            $('#minimal_price').val('0.00');
                            $('#target_price').val('0.00');
                            $('#selling_price').val('0.00');
                        }
                    }
                }
            } catch (e) {
                console.log('invalid url');
                toastr.error('Введите верный URL', 'Ошибка');
            }

        }
    }

});
