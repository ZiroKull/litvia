$(document).ready(function () {

    editor = new $.fn.dataTable.Editor({
        ajax: {
            url: "/admin/hs-codes/update",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        table: "#hs-codes-table",
        idSrc: 'id',
        fields: [
            {
                label: "Таможеный тариф",
                name: "tariff",
            },
        ],
        i18n: {
            edit: {
                button: "Сохранить",
                title: "Сохранить изменения",
                submit: "Сохранить"
            },
        }
    });

    // Activate an inline edit on click of a table cell
    $('#hs-codes-table').on('click', 'tbody td:not(:first-child):not(.not-editable)', function (e) {
        editor.inline(
            table.cell(this).index(),
            {
                onBlur: 'submit'
            });
    });

    serviceClass.prepareEditorData(editor);
    serviceClass.setNumericTypeToField(['tariff']);

    editor.on('open', function (e, type) {
        // Type is 'main', 'bubble' or 'inline'
        let openVals = editor.get();
        let formulaTarif = parseFloat(openVals.tariff);

        if (!isNaN(formulaTarif))
            $('#DTE_Field_tariff').attr('placeholder', parseFloat(openVals.tariff).toFixed(2));
        else
            $('#DTE_Field_tariff').attr('placeholder', '');
    });

    let table = $('#hs-codes-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "autoWidth": false,
        "searching": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/hs-codes/getHSCodes',
            type: "GET",
            dataType: 'json',
            data:  function (data) {
                serviceClass.filter('#hs-codes-table',data);
            }
        },
        columns: [
            {data: 'hs_code'},
            {data: 'tariff'},
        ],
        initComplete: function () {
            let api = this.api();
            serviceClass.initFields(api, {disableColumns: 12})
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[0, "desc"]],
        "processing": true,
        "serverSide": true
    });

    table.on('responsive-resize', function ( e, datatable, columns ) {
        serviceClass.hideSearchInputs( columns );
    } );

    $('input', editor.node()).on('focus', function () {
        this.select();
    });
});
