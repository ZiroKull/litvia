$(document).ready(function () {
    $( "#set_price_to_limit" ).change(function() {

        let val;
        if(this.checked) {
            val = 1;
        } else {
            val = 0;
        }

        let data = "settings[set_price_to_limit]="+val;

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: "/admin/settings/update",
            data: data
        })
        .done(function( msg ) {
             if(msg.status = 'ok')
                  toastr.success('Данные сохранены', 'Успех!');
              else
                  toastr.error('Возникла ошибка на сервере', 'Ошибка!');
        });

    });
});