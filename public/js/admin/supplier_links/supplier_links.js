$(document).ready(function () {

    let supplierId = $('.supplier_id').val();
    let finded = false;
    let selectedProductId = null;
    let supplierQualityValue = null;
    let isAccurateValue = null;

    editor = new $.fn.dataTable.Editor({
        ajax: {
            url: "/admin/hs-codes/update",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        table: "#supplier-links-table",
        idSrc: 'id',
        fields: [
            {
                label: "Качество поставщика",
                name: "supplier_quality",
                type: "select",
                options: [
                    {label: 'Не покупать', value: 10},
                    {label: 'Средний поставщик', value: 20},
                    {label: 'Лучший поставщик', value: 30},
                ],
                className: "dt-select-full"
            },
            {
                label: "Коментарий от поставщика",
                name: "comments",
            },
            {
                label: "Кол-во в коробке",
                name: "quantity_per_carton",
            },
            {
                label: "Объем коробки м3",
                name: "carton_volume_m3",
            },
            {
                label: "Вес коробки kg",
                name: "carton_weight_kg",
            },
            {
                label: "Кол-во, вес, объем точные",
                name: "is_accurate",
                type: "select",
                options: [
                    {label: 'Нет', value: 0},
                    {label: 'Да', value: 1},
                ],
                className: "dt-select-full"
            },
        ],
        i18n: {
            edit: {
                button: "Сохранить",
                title: "Сохранить изменения",
                submit: "Сохранить"
            },
        }
    });

    editor.on(`open`, function(e, mode, action)
    {
        supplierQualityValue = editor.field(`supplier_quality`).val();
        isAccurateValue = editor.field(`is_accurate`).val();
    });

    // Activate an inline edit on click of a table cell
    $('#supplier-links-table').on('click', 'tbody td:not(:first-child):not(.not-editable)', function (e) {
        editor.inline(
            table.cell(this).index(),
            {
                onBlur: 'submit',
            });
    });

    $(document).on('change', '#DTE_Field_supplier_quality', function () {
        if (parseInt(supplierQualityValue) !== parseInt($(this).val())) {
            editor
                .edit($(this).closest('tr'), false)
                .set('supplier_quality', $(this).val())
                .submit();
        }
    });

    $(document).on('change', '#DTE_Field_is_accurate', function () {
        if (parseInt(isAccurateValue) !== parseInt($(this).val())) {
            editor
                .edit($(this).closest('tr'), false)
                .set('is_accurate', $(this).val())
                .submit();
        }
    });

    let table = $('#supplier-links-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "autoWidth": false,
        "searching": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/suppliers/'+ supplierId +'/getSupplierLinks',
            type: "GET",
            dataType: 'json',
            data:  function (data) {
                serviceClass.filter('#supplier-links-table', data);
            }
        },
        columns: [
            {data: 'product_id'},
            {data:
                'supplier_quality',
                orderable: false,
                render: function (data, type, row) {
                    if (type === 'display') {
                        return (data == 30) ? 'Лучший поставщик' : ((data == 20) ? 'Средний поставщик' : 'Не покупать');
                    } else {
                        return '';
                    }
                }
            },
            {data: 'rohs_file'},
            {data: 'ce_file'},
            {data: 'comments'},
            {data: 'quantity_per_carton'},
            {data: 'carton_volume_m3'},
            {data: 'carton_weight_kg'},
            {
                data: 'is_accurate',
                render: function (data, type, row) {
                    if (type === 'display') {
                        return (data == 1) ? 'Да' : 'Нет';
                    } else {
                        return '';
                    }
                },
                className: "dt-body-center"
            },
        ],
        initComplete: function () {
            let api = this.api();
            serviceClass.initFields(api, {disableColumns: 12})
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "order": [[0, "desc"]],
        "processing": true,
        "serverSide": true
    });

    table.on('responsive-resize', function ( e, datatable, columns ) {
        serviceClass.hideSearchInputs( columns );
    } );

    $('input', editor.node()).on('focus', function () {
        this.select();
    });

    let $modal = $('#add-smollest-group');

    $(document).on('click', '.open-add-smollest-group', function (e) {
        e.preventDefault();
        $('#smallest-group').val('');
        $modal.modal('show');
    });

    $('.field-button').click(function () {
        // clear fields
        $('#smallest-group').val('');
    });

    $('#smallest-group').autocomplete({
        source: function (request, response) {

            console.log(request);

            let url = $('#url').val();
            // checkUniqueUrl(request.term,url);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "GET",
                url: " /admin/suppliers/searchSmallestGroup",
                data: {q: request.term}
            })
                .done(function (data) {
                    console.log(data);

                    response(data);
                });

        },
        minLength: 2,
        select: function (event, ui) {
            console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
            finded = false;
            if (ui.item.id !== undefined) {
                selectedProductId = ui.item.id;
                finded = true;
            }
        },
        response: function (event, ui) {
            if (!ui.content.length) {
                let noResult = {value: "", label: "Ничего не найдено"};
                ui.content.push(noResult);
            }
        }
    });

    $(document).on('click', '#save-smallest-group', function (e) {
        e.preventDefault();

        if (finded) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: " /admin/suppliers/addSmallestGroup",
                data: {
                    supplier_id: supplierId,
                    product_id: selectedProductId,
                }
            })
            .done(function (data) {
                console.log(data);

                if (data.status) {
                    table.ajax.reload();
                } else {
                    toastr.error('Mельчайшая группа уже добавлена', 'Ошибка!');
                }
            });
        }
    });
});
