$(document).ready(function () {
    $( document ).on( "click", ".save-setting", function() {
        let name = $(this).parent().prev().find('input.form-control').attr('name');
        let key = name.match(/\[(\S+)\]/)[1];

        let val = $(this).parent().prev().find('input.form-control').val();

        if(key == 'divide_by_x_specify_x' && val == '0'){
            //$('#divide_by_x_specify_x').val('');
            toastr.error('Деление на 0 невозможно', 'Ошибка!');
            return;
        }

        if(key == 'more_than_x_hours' && (val % 24) ){
            //$('#more_than_x_hours').val('');
            toastr.error('Введите число кратное 24', 'Ошибка!');
            return;
        }

        if((key == 'creating_xml_specify_x' || key == 'calculation_average_sales') && val <= 0) {
            toastr.error('Значение должно быть больше 0', 'Ошибка!');
            return;
        }

        if(key == 'email_send_sell_product' && (!val.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))){
            toastr.error('Введите верный формат email', 'Ошибка!');
            return;
        }

        if(val) {
            //make ajax request
            val = serviceClass.replaceNumberWithCommaToDot(val);

            let data = "settings["+key+"]="+val;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "/admin/settings/update",
                data: data
            })
            .done(function( msg ) {
                if(msg.status === 'ok')
                    toastr.success('Данные сохранены', 'Успех!');
                else
                    toastr.error(msg.errors, 'Ошибка!');
            });
        }
    });
});
