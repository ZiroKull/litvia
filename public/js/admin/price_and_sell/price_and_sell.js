$(document).ready(function () {

    editor = new $.fn.dataTable.Editor({
        ajax: {
            url: "/admin/priceAndSell/update",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        table: "#price-and-sell-table",
        idSrc: 'id',
        fields: [
            {
                label: "Мин цена",
                name: "minimal_price",
            },
            {
                label: "Целевая цена",
                name: "target_price",
            },
            {
                label: "Цена без скидки",
                name: "price_without_discount",
            },
            {
                label: "Текущая цена продажи",
                name: "selling_price",
            },
            {
                label: "Добавлять в XML",
                name: "is_xml",
                type: "checkbox",
                separator: "|",
                options: [
                    {label: '', value: 1}
                ]
            },
            {
                label: 'Дата доставки',
                name: 'delivery_date',
                type: 'datetime',
                def: function () {
                    return new Date();
                }
            },
            {
                label: "Количество для XML",
                name: "xml_count",
            },
            {
                name: "formula_set_price",
            },
            {
                label: "Одна из цен меньше 8 евро",
                name: "less_8_euro",
                type: "checkbox",
                separator: "|",
                options: [
                    {label: '', value: 1}
                ]
            }

        ],
        i18n: {
            datetime: {
                previous: 'Предыдущий',
                next: 'Следующий',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            }
        }
    });

    // Activate an inline edit on click of a table cell
    $('#price-and-sell-table').on('click', 'tbody td:not(:first-child):not(.not-editable):not(:nth-child(2)):not(:nth-child(8)):not(:nth-child(11)):not(:nth-child(12)):not(:nth-child(13))', function (e) {

        if ($(this).hasClass('control') || $(this).hasClass('select-checkbox')) {
            return;
        }

        editor.inline(table.cell(this).index(), {onBlur: 'submit'});
    });


    $('#price-and-sell-table').on('click', 'tbody ul.dtr-details li', function (e) {
        // Edit the value, but this selector allows clicking on label as well
        editor.inline($('span.dtr-data', this));
    });

    serviceClass.prepareEditorData(editor);
    serviceClass.setNumericTypeToField([
        'minimal_price',
        'target_price',
        'price_without_discount',
        'selling_price',
    ]);

    function hideSearchInputs(columns) {
        for (let i = 0; i < columns.length; i++) {
            if (columns[i]) {
                $('.filterhead:eq(' + i + ')').show();
            } else {
                $('.filterhead:eq(' + i + ')').hide();
            }
        }
    }

    let table = $('#price-and-sell-table').DataTable({
        orderCellsTop: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        "autoWidth": false,
        /*rowReorder: {
            selector: 'td:nth-child(2)',
            dataSrc: 'position'
        },*/
        "searching": true,
        "ordering": false,
        "lengthChange": true,
        "lengthMenu": [32, 64, 128],
        "pageLength": 128,
        responsive: false,
        ajax: {
            url: '/admin/priceAndSell/getPriceAndSell',
            type: "GET",
            dataType: 'json',
            data: function (data) {
                serviceClass.filter('#price-and-sell-table',data);
            }
        },
        columns: [
            {data: 'local_name', orderable: true},
            {data: 'type', name: 'product_prices.type', orderable: false, searchable: true},
            {data: 'minimal_price', name: 'product_prices.minimal_price', orderable: false},
            {data: 'target_price', name: 'product_prices.target_price', searchable: true},
            {data: 'price_without_discount', name: 'product_prices.price_without_discount',orderable: false,},
            {
                data: 'selling_price', name: 'product_prices.selling_price', orderable: false,
                render: function (data, type, row) {
                    if (row.selling_price) {
                        return row.selling_price;
                    } else if (row.formula_set_price) {
                        return '<span style="color:#ccc;">' + row.formula_set_price.toFixed(2) + '</span>';
                    } else {
                        return '';
                    }

                }
            },
            {data: 'competitors_count', name: 'product_prices.competitors_count', searchable: true},
            {
                data: 'is_xml',
                orderable: false,
                searchable: true,
                render: function (data, type, row) {
                    //console.log(row);
                    if (type === 'display' && row.type == 'LT') {
                        return '<input type="checkbox" class="editor-is-xml">';
                    } else {
                        return '';
                    }
                    //return data;
                },
                className: "dt-body-center"
            },
            {
                data: 'delivery_date',
                orderable: false,
                render: function (data, type, row) {
                    if (type === 'display' && row.type == 'LT') {
                        return data;
                    } else {
                        return '';
                    }
                }
            },
            {
                data: 'xml_count',
                orderable: false,
                render: function (data, type, row) {
                    if (type === 'display' && row.type == 'LT') {
                        return data;
                    } else {
                        return '';
                    }
                }
            },
            {
                data: 'less_8_euro', orderable: true,
                render: function (data, type, row) {
                    if (type === 'display' && row.type == 'LT') {
                        return '<input type="checkbox" class="editor-less-8-euro">';
                    } else {
                        return '';
                    }
                },
                className: "dt-body-center"
            },
            {
                data: 'xml_located',
                orderable: false,
                render: function (data, type, row) {
                    if (type === 'display' && row.type == 'LT') {
                        return (data == 1) ? 'Да' : 'Нет';
                    } else {
                        return '';
                    }
                },
                className: "dt-body-center"
            },
            {data: 'warning', name: 'product_prices.warning', orderable: false, searchable: true}
        ],
        initComplete: function () {
            let api = this.api();
            //initFields(api);
            serviceClass.initFields(api, {disableColumns: 12});
            //hideSearchInputs(api.columns().responsiveHidden().toArray())
        },
        "rowCallback": function (row, data) {
            //console.log(data.type);
            if (data.type != 'LT') {
                $("td:eq(6),td:eq(7),td:eq(8),td:eq(9),td:eq(10)", row).addClass("not-editable");
            }
            if (data.type == 'FI') {
                $(row).addClass('st-border-bottom');
            }
            // Set the checked state of the checkbox in the table
            $('input.editor-is-xml', row).prop('checked', data.is_xml == 1);
            $('input.editor-less-8-euro', row).prop('checked', data.less_8_euro == 1);
            $('input.editor-xml-located', row).prop('checked', data.xml_located == 1);
        },
        order: [[0, 'asc']],
        "processing": true,
        "serverSide": true
    });

    editor.on('open', function (e, type) {
        // Type is 'main', 'bubble' or 'inline'
        let openVals = editor.get();
        let formulaSetPrice = parseFloat(openVals.formula_set_price);

        if (!isNaN(formulaSetPrice))
            $('#DTE_Field_selling_price').attr('placeholder', parseFloat(openVals.formula_set_price).toFixed(2));
        else
            $('#DTE_Field_selling_price').attr('placeholder', '');

        //console.log(openVals);
    });

    $('#price-and-sell-table').on('change', 'input.editor-is-xml', function () {
        editor
            .edit($(this).closest('tr'), false)
            .set('is_xml', $(this).prop('checked') ? 1 : 0)
            .submit();
    });

    $('#price-and-sell-table').on('change', 'input.editor-less-8-euro', function () {
        editor
            .edit($(this).closest('tr'), false)
            .set('less_8_euro', $(this).prop('checked') ? 1 : 0)
            .submit();
    });

    table.on('responsive-resize', function (e, datatable, columns) {
        serviceClass.hideSearchInputs(columns);
    });

    $('input', editor.node()).on('focus', function () {
        this.select();
    });

});
