<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToProductLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_links', function (Blueprint $table) {
            //
            $table->float('minimal_price');
            $table->float('target_price');
            $table->float('price_without_discount');
            $table->float('selling_price');
            $table->boolean('is_xml');
            $table->dateTime('delivery_date');
            $table->integer('xml_count');
            $table->boolean('less_8_euro');
            $table->boolean('xml_located');
            $table->text('warning');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_links', function (Blueprint $table) {
            //
            $table->dropColumn('minimal_price');
            $table->dropColumn('target_price');
            $table->dropColumn('price_without_discount');
            $table->dropColumn('selling_price');
            $table->dropColumn('is_xml');
            $table->dropColumn('delivery_date');
            $table->dropColumn('xml_count');
            $table->dropColumn('less_8_euro');
            $table->dropColumn('xml_located');
            $table->dropColumn('warning');
        });
    }
}
