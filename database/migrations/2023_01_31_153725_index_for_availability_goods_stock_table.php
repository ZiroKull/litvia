<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IndexForAvailabilityGoodsStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('availability_goods_stock', function (Blueprint $table) {
            $table->index('local_sku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('availability_goods_stock', function (Blueprint $table) {
            $table->dropIndex('local_sku');
        });
    }
}
