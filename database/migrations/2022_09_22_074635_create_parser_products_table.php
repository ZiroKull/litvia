<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParserProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser_products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('product_link');
            $table->string('product_name');
            $table->float('reviews_frequency');
            $table->integer('views_per_week');
            $table->integer('interested_similar');
            $table->integer('reviews_count');
            $table->float('product_price');
            $table->integer('product_count');
            $table->integer('delivery_time');
            $table->integer('question_number');
            $table->float('average_product_rating');
            $table->float('product_weight');
            $table->integer('sellers_count');
            $table->float('5_star_ratings');
            $table->float('4_star_ratings');
            $table->float('3_star_ratings');
            $table->float('2_star_ratings');
            $table->float('1_star_ratings');
            $table->float('product_length');
            $table->float('product_width');
            $table->float('product_height');
            $table->float('product_barcode');
            $table->dateTime('date_viewed_page');
            $table->dateTime('time_viewed_page');
            $table->dateTime('last_review_date');
            $table->string('catalog_group_title');
            $table->string('catalog_category_title');
            $table->string('catalog_sub_category_title');
            $table->integer('ds_delivery_hours');
            $table->integer('seller_min_cart_sum');
            $table->integer('packages_count');
            $table->string('brand_name');
            $table->string('provider');
            $table->string('shipping_information');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parser_products');
    }
}
