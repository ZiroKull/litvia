<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('weight_g')->after('hs_code_id')->nullable();
            $table->integer('lenght_cm')->after('weight_g')->nullable();
            $table->integer('height_cm')->after('lenght_cm')->nullable();
            $table->integer('width_cm')->after('height_cm')->nullable();
            $table->text('forwarder_description')->after('width_cm')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('weight_g');
            $table->dropColumn('lenght_cm');
            $table->dropColumn('height_cm');
            $table->dropColumn('width_cm');
            $table->dropColumn('forwarder_description');
        });
    }
}
