<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumsProductLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_links', function (Blueprint $table) {
            DB::statement('ALTER TABLE `product_links` CHANGE `delivery_date` `delivery_date` DATETIME NULL DEFAULT NULL');
            $table->dropColumn('minimal_price');
            $table->dropColumn('target_price');
            $table->dropColumn('price_without_discount');
            $table->dropColumn('selling_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_links', function (Blueprint $table) {
            $table->float('minimal_price');
            $table->float('target_price');
            $table->float('price_without_discount');
            $table->float('selling_price');
        });
    }
}
