<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsParserProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('parser_products', function (Blueprint $table) {
            $table->dateTime('last_review_date')->nullable()->change();
            $table->integer('seller_min_cart_sum')->nullable()->change();
            $table->string('brand_name')->nullable()->change();
            $table->string('catalog_sub_category_title')->nullable()->change();
            $table->time('time_viewed_page')->change();
            $table->date('last_sell_date')->nullable()->after('shipping_information');
            $table->time('last_sell_time')->nullable()->after('last_sell_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
