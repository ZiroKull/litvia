<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AvailabilityGoodsStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('availability_goods_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('local_sku');
            $table->dateTime('date');
            $table->boolean('is_stock');
            $table->integer('sales_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('availability_goods_stock');
    }
}
