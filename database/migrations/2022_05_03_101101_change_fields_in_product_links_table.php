<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsInProductLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_links', function (Blueprint $table) {
            //
            DB::statement('ALTER TABLE `product_links` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `warning`');
            DB::statement('ALTER TABLE `product_links` CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_links', function (Blueprint $table) {
            //
        });
    }
}
