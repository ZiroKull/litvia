<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('local_company_name');
            $table->string('legal_company_name');
            $table->string('manager_name');
            $table->string('manager_phone');
            $table->string('manager_email');
            $table->string('chat_link');
            $table->string('shop_link');
            $table->string('warehouse_name');
            $table->string('warehouse_phone');
            $table->string('warehouse_email');
            $table->string('warehouse_adress');
            $table->integer('export_license')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
