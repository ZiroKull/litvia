<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmallestGroupsSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smallest_groups_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id');
            $table->integer('product_id');
            $table->integer('supplier_quality');
            $table->string('rohs_file')->nullable();
            $table->string('ce_file')->nullable();
            $table->string('comments')->nullable();
            $table->integer('quantity_per_carton')->default(0);
            $table->decimal('carton_volume_m3', 5, 4)->default(0);
            $table->float('carton_weight_kg')->default(0);
            $table->integer('is_accurate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smallest_groups_suppliers');
    }
}
