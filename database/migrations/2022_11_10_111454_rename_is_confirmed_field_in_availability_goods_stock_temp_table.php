<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameIsConfirmedFieldInAvailabilityGoodsStockTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('availability_goods_stock_temp', function (Blueprint $table) {
            //
            $table->renameColumn('is_confirmed', 'is_completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('availability_goods_stock_temp', function (Blueprint $table) {
            //
            $table->renameColumn('is_completed', 'is_confirmed');
        });
    }
}
