<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('change_user_id');
            $table->dateTime('change_user_date');
            $table->integer('read_user_id');
            $table->string('product_name');
            $table->integer('cost_per_documents');
            $table->integer('balance_of_documents');
            $table->string('barcode');
            $table->integer('remaining_items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
