<?php

namespace App\Exports;

use App\Models\Admin\ParserProducts;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;

class ParserProductsExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    use Exportable;
    
    public function query()
    {
        return ParserProducts::select('product_link', 'product_name', 'reviews_frequency', 'views_per_week', 'interested_similar', 'reviews_count', 'product_price', 'product_count', 'delivery_time', 'question_number', 'average_product_rating', 'product_weight', 'sellers_count', '5_star_ratings', '4_star_ratings', '3_star_ratings', '2_star_ratings', '1_star_ratings', 'product_length', 'product_width', 'product_height', 'product_barcode', 'date_viewed_page', 'time_viewed_page', 'last_review_date', 'catalog_group_title', 'catalog_category_title', 'catalog_sub_category_title', 'ds_delivery_hours', 'seller_min_cart_sum', 'packages_count', 'brand_name', 'provider', 'shipping_information');
    }

    public function headings(): array
    {
        return [
            'Ссылка на товар',
            'Название товара',
            'Частота отзывов',
            'Просмотров в неделю',
            'Интересуются похожими',
            'Количество отзывов',
            'Цена товара',
            'Количество товаров',
            '-',
            'Количество вопросов',
            'Средняя оценка товара',
            'Вес товара',
            'Количество продавцов',
            'Часть оценок в 5 звёзд',
            '4 звёзды',
            '3 звёзды',
            '2 звёзды',
            '1 звёзда',
            'Длина товара',
            'Ширина товара',
            'Высота товара',
            'Баркод товара',
            'Дата когда программа просмотрела страницу',
            'Время когда страница была просмотерна',
            'Дата последнего отзыва',
            'catalogGroupTitle',
            'catalogCategoryTitle',
            'catalogSubCategoryTitle',
            'delivery_hours',
            'seller_min_cart_sum',
            'packages_count',
            'BrandName',
            'Поставшик',
            'Информация по доставке',
        ];
    }

}