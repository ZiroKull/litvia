<?php
/**
 * Created by PhpStorm.
 * User: onzone
 * Date: 21.06.2022
 * Time: 15:47
 */

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;
use App\Models\Admin\Settings;
use DateTime;
use App\Helpers\Helper;

class PiguServices
{
    public function getResponse($url){
        $response = Http::get($url);
        return $response;
    }

    public function getProductData($responseData, $request = NULL)
    {
        $data = array();

        if ($responseData->successful()) {
            $html = $responseData->body();
            $crawler = new Crawler($html);
            $errors = 0;
            
            //
            $data = array();
            
            $data['competitor_name'] = $crawler->filter('.site-block h1')->text();

            if (!empty($crawler->filter('.product-price.fl meta[itemprop="price"]')->count()))
                $data['competitor_price'] = (float)$crawler->filter('.product-price.fl meta[itemprop="price"]')->attr('content');
            
            if (!empty($crawler->filter('#productPage')->count())) {
                $widgetData = json_decode($crawler->filter('#productPage')->attr('widget-data'), TRUE);
                $dataColorKeys = array_keys($widgetData["colors"]);
                
                if (!empty($dataColorKeys[0])) {
                    if (!empty($widgetData["colors"][$dataColorKeys[0]]["modifications"])) {
                        $dataAdditionalKey = array_keys($widgetData["colors"][$dataColorKeys[0]]["modifications"]);
                        if (!empty($dataAdditionalKey[0])) {
                            $data['competitor_hours_delivery'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["delivery_hours"];
                            $data['competitor_min_order_amount'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["seller_min_cart_sum"];
                        }
                    }
                }

                if(empty($data['competitor_price']))
                    $data['competitor_price'] = 88888;
                
                if(array_key_exists('competitor_hours_delivery', $data)){
                    if(is_null($data['competitor_hours_delivery']))
                        $data['competitor_hours_delivery'] = 4800;
                } else {
                    $data['competitor_hours_delivery'] = 4800;
                }

                if(array_key_exists('competitor_min_order_amount', $data)){
                    if(is_null($data['competitor_min_order_amount']))
                        $data['competitor_min_order_amount'] = 0;
                } else {
                    $data['competitor_min_order_amount'] = 1000;
                }
                    
                if (!empty($crawler->filter('.supplierTitle a')->count())) {
                    $data['supplier_title'] = $crawler->filter('.supplierTitle a')->text();
                    if ($data['supplier_title'] == 'Tetro' || $data['supplier_title'] == 'Svajoklių namai, MB') {
                        $data['competitor_price'] = 99999;
                    }
                }
                
            } else {
                $data['competitor_price'] = 77777;
                $data['competitor_min_order_amount'] = 0;
                $data['competitor_hours_delivery'] = 2400;
            }

            //calculate changed_competitor_price
            $settings = Settings::all();

            $valArray = array();

            foreach ($settings as $value) {
                $valArray[$value->key] = $value->value;
            }
            
            $result = Helper::calculateCompetitorPrice(array(
                'more_than_x_hours' => $valArray['more_than_x_hours'],
                'customer_pays_x_euros' => $valArray['customer_pays_x_euros'],
                'divide_by_x_specify_x' => $valArray['divide_by_x_specify_x'],
                'competitor_hours_delivery' => $data['competitor_hours_delivery'],
                'competitor_min_order_amount' => $data['competitor_min_order_amount'],
                'competitor_price' => $data['competitor_price'],
                'coefficient_price' => (float)$request['coefficient_price']
            ));
            
            if(empty($result['error'])) {
                $data['changed_competitor_price'] = $result['changed_competitor_price'];
            } else {
                $errors++;
            }
            
            //move current
            if($errors == 1){
                $data['errors'] = $result['error'];
                return $data;
            }

        }
        
        return $data;
    }

    public function getProductDataXls($responseData, $request = NULL){
        $data = array();
        if ($responseData->successful()) {

            $html = $responseData->body();
            //var_dump($html); die;
            $crawler = new Crawler($html);

            $data = array();
            $data['product_link'] = $request['product_link'];
            $data['product_name'] = $crawler->filter('.site-block h1')->text();

            if (!empty($crawler->filter('div[rel-widget-id="ReviewRatings"] .qa_list ul li:last-child .q_date')->count())) {
                $firstReviewDate = $crawler->filter('div[rel-widget-id="ReviewRatings"] .qa_list ul > li[widget="ReviewRatings"]:first-child .q_date')->text();
                $lastReviewDate = $crawler->filter('div[rel-widget-id="ReviewRatings"] .qa_list ul > li[widget="ReviewRatings"]:last-child .q_date')->text();

                $earlier = new DateTime($firstReviewDate);
                $later = new DateTime($lastReviewDate);
                $diff = $later->diff($earlier)->format("%a");

                $reviewsCounter = $crawler->filter('div[rel-widget-id="ReviewRatings"] .qa_list > ul li')->count();

                if ($reviewsCounter > 1) {
                    $data['reviews_frequency'] = $diff / ($reviewsCounter - 1);
                }
            }

            if (!empty($crawler->filter('#productStatusGrid li:nth-child(1) p.value')->count())) {
                $data['views_per_week'] = preg_replace('/[^0-9]/', '', $crawler->filter('#productStatusGrid li:nth-child(1) p.value')->text());
            }

            if (!empty($crawler->filter('#productStatusGrid  li:nth-child(2) p.value')->count())) {
                $data['interested_similar'] = preg_replace('/[^0-9]/', '', $crawler->filter('#productStatusGrid  li:nth-child(2) p.value')->text());
            }

            if (!empty($crawler->filter('#productRatingsTab span[itemprop="reviewCount"]')->count())) {
                $data['reviews_count'] = preg_replace('/[^0-9]/', '', $crawler->filter('#productRatingsTab span[itemprop="reviewCount"]')->text());
            }

            if (!empty($crawler->filter('.product-price.fl meta[itemprop="price"]')->count())) {
                $data['product_price'] = $crawler->filter('.product-price.fl meta[itemprop="price"]')->attr('content');
            }

            if(!empty($crawler->filter('#productPage')->count())) {
                $widgetData = json_decode($crawler->filter('#productPage')->attr('widget-data'), TRUE);
                $dataColorKeys = array_keys($widgetData["colors"]);
                if (!empty($dataColorKeys[0])) {
                    if (!empty($widgetData["colors"][$dataColorKeys[0]]["modifications"])) {
                        $dataAdditionalKey = array_keys($widgetData["colors"][$dataColorKeys[0]]["modifications"]);
                        if (!empty($dataAdditionalKey[0])) {
                            $data['product_count'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["quantity"];
                            $data['product_weight'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["weight"];
                            $data['product_length'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["length"];
                            $data['product_width'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["width"];
                            $data['product_height'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["height"];
                            $data['ds_delivery_hours'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["delivery_hours"];
                            $data['seller_min_cart_sum'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["seller_min_cart_sum"];
                            $data['packages_count'] = $widgetData["colors"][$dataColorKeys[0]]["modifications"][$dataAdditionalKey[0]]["packages_count"];
                        }
                    }
                }
            }

            if (!empty($crawler->filter('.product_rating #question_answerTab')->count())) {
                $data['question_number'] = preg_replace('/[^0-9]/', '', $crawler->filter('.product_rating #question_answerTab')->text());
            }

            if (!empty($crawler->filter('#productPage span.expandable-item span span')->count())) {
                $data['average_product_rating'] = $crawler->filter('#productPage span.expandable-item span span')->text();
            }

            $data['sellers_count'] = 1;
            if (!empty( $crawler->filter('span.smooth_scroll.supplier-scroll')->count())) {
                $sellersCountValue = preg_replace('/[^0-9]/', '', $crawler->filter('span.smooth_scroll.supplier-scroll')->text());

                if (!empty($sellersCountValue)) {
                    $data['sellers_count'] = $data['sellers_count'] + 1;
                }

            }


            if (!empty($crawler->filter('.rating-details')->count())) {
                $data['5_star_ratings'] = trim($crawler->filter('.rating-details div:nth-child(2) div.rating-details__percents')->text());
                $data['4_star_ratings'] = trim($crawler->filter('.rating-details div:nth-child(3) div.rating-details__percents')->text()) / $data['reviews_count'];
                $data['3_star_ratings'] = trim($crawler->filter('.rating-details div:nth-child(4) div.rating-details__percents')->text()) / $data['reviews_count'];
                $data['2_star_ratings'] = trim($crawler->filter('.rating-details div:nth-child(5) div.rating-details__percents')->text()) / $data['reviews_count'];
                $data['1_star_ratings'] = trim($crawler->filter('.rating-details div:nth-child(6) div.rating-details__percents')->text()) / $data['reviews_count'];
            }

            if (!empty($crawler->filter('#flix-description script')->count())) {
                $data['barcode'] = $crawler->filter('#flix-description script')->text();
                $re = '/flixScript\.setAttribute\(\'data-flix-ean\', ["|\']{0,}([\d]{0,})/s';
                preg_match($re, $data['barcode'], $matches);
                $data['barcode'] = (!empty($matches[1])) ? $matches[1] : NULL;
            }

            $data['date_viewed_page'] = date("Y-m-d");
            $data['time_viewed_page'] = date("H:i:s");

            if (!empty($crawler->filter('div[rel-widget-id="ReviewRatings"] .qa_list ul li:first-child')->count())) {
                $data['last_review_date'] = $crawler->filter('div[rel-widget-id="ReviewRatings"] .qa_list ul li:first-child .q_date')->text();
            } else {
                $data['last_review_date'] = NULL;
            }

            preg_match('/"catalogGroupTitle":"(.*?)"/m', $html, $matches);
            $data['catalog_group_title'] = (!empty($matches[1])) ? $matches[1] : NULL;
            preg_match('/"catalogCategoryTitle":"(.*?)"/m', $html, $matches);
            $data['catalog_category_title'] = (!empty($matches[1])) ? $matches[1] : NULL;
            preg_match('/"catalogSubCategoryTitle":"(.*?)"/m', $html, $matches);
            $data['catalog_sub_category_title'] = (!empty($matches[1])) ? $matches[1] : NULL;
            preg_match('/"@type":"Thing","name":"(.*?)"/m', $html, $matches);
            $data['brand_name'] = (!empty($matches[1])) ? $matches[1] : NULL;

            if (!empty($crawler->filter('.supplierTitle a')->count())) {
                $data['provider'] = $crawler->filter('.supplierTitle a')->text();
            }

            if(!empty($crawler->filter('[rel-widget-id="delivery-collapse"] #productPageDeliveryTerms:first-child  p strong')->count())){
                $deliveryInfo = $crawler->filter('[rel-widget-id="delivery-collapse"] #productPageDeliveryTerms:first-child  p strong');
                $data['shipping_information'] = '';

                foreach ($deliveryInfo as $key => $info) {
                    $domElement = new Crawler($info);
                    $data['shipping_information'] .= $domElement->text();
                }
            }

            /*if(!empty( $crawler->filter('.product-id')->count())) {
                $productId = explode(":", $crawler->filter('.product-id')->text());

                if (!empty($productId[1])) {
                    $response = file_get_contents('https://pigu.lt/ru/widgets/notifications/lastTimeProductOrder?productId=' . trim($productId[1]));
                    $responseArray = json_decode($response, TRUE);
                    $data['last_sell_date'] = date("Y-m-d", strtotime($responseArray['last_time']));
                    $data['last_sell_time'] = date("H:i", strtotime($responseArray['last_time']));
                }
            }*/


        }

        return $data;

    }
}