<?php
namespace App\Services;

use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;

class GoogleDriveService
{
    protected $client;
    protected $service;
    protected $adapter;

    public function __construct()
    {
        $this->client = new \Google_Client();
        $this->client->setClientId(env('GOOGLE_DRIVE_CLIENT_ID'));
        $this->client->setClientSecret(env('GOOGLE_DRIVE_CLIENT_SECRET'));
        $this->client->refreshToken(env('GOOGLE_DRIVE_REFRESH_TOKEN'));

        $this->service = new \Google_Service_Drive($this->client);

        $this->adapter = new \Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter($this->service, env('GOOGLE_DRIVE_FOLDER_ID'));
    }

    public function listContents(){
        $response = $this->adapter->listContents();
        return $response;
    }

    public function getFileUrl($path){
        $url = $this->adapter->getUrl($path);
        return $url;
    }


}
