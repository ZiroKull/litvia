<?php

namespace App\Actions;

use App\Models\Admin\PiguFees;

class PiguFeesActions
{
    public function handle(): bool
    {
        return PiguFees::whereNull('pigu_fee')->exists();
    }
}
