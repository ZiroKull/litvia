<?php

namespace App\DataTables;
use App\Models\Admin\Supplier;

class SuppliersTable
{
    public static function getTable(){
        return datatables()->eloquent(Supplier::select()->orderBy('updated_at', 'desc'))
            ->filterColumn('local_company_name', function ($query, $keyword) {
                $query->whereRaw("local_company_name LIKE ?", ["%{$keyword}%"]);
            })
            ->filterColumn('legal_company_name', function ($query, $keyword) {
                $query->whereRaw("legal_company_name LIKE ?", ["%{$keyword}%"]);
            })
            ->filterColumn('manager_name', function ($query, $keyword) {
                $query->whereRaw("manager_name LIKE ?", ["%{$keyword}%"]);
            })
            ->editColumn('local_company_name', function ($supplier) {
                return '<a href="'. route('suppliers.edit', $supplier->id) .'" class="supplier-link" data-id="'. $supplier->id .'">'. $supplier->local_company_name .'</a>';
            })
            ->editColumn('legal_company_name', function ($supplier) {
                return '<a target="_blank" href="'. $supplier->shop_link .'" class="supplier-link" data-id="'. $supplier->id .'">'. $supplier->legal_company_name .'</a>';
            })
            ->editColumn('manager_name', function ($supplier) {
                return '<a target="_blank" href="'. $supplier->chat_link .'" class="supplier-link" data-id="'. $supplier->id .'">'. $supplier->manager_name .'</a>';
            })
            ->addColumn('action', function ($supplier) {
                return '<a href="'. route('suppliers.edit', $supplier->id) .'" class="btn btn-primary btn-xs mx-2 my-1 p-2" ><i class="fas fa-edit"></i></a>';
            })
            ->rawColumns(['local_company_name', 'legal_company_name', 'manager_name', 'action'])
            ->make(TRUE);
    }
}
