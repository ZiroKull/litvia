<?php

namespace App\DataTables\Filters;

class Conditions{
    public static function setConditions($query, $request){
        //var_dump($request); die;
        if(!empty($request)) {
            if (!empty($request['columns'])) {
                foreach ($request['columns'] as $item) {
                    if (isset($item["search"]["value"])) {
                        //conditions
                        if (!empty($item["search"]["param"])) {
                            switch ($item["search"]["param"]) {
                                case "#" :
                                    $query->whereNull($item['data']);
                                    break;
                                case "!#" :
                                    $query->whereNotNull($item['data']);
                                    break;
                                case "==" :
                                    $query->where($item['data'], $item["search"]["value"]);
                                    break;
                                case "~":
                                    $query->whereRaw($item['data'] . " LIKE ?", ["%{$item["search"]["value"]}%"]);
                                    break;
                                default:
                                    $query->where($item['data'], $item["search"]["param"], $item["search"]["value"]);
                                    break;
                            }
                        }
                        //like conditions
                    }
                }
                //die;
            }
        }
    }
}