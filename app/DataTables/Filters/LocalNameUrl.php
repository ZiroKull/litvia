<?php

namespace App\DataTables\Filters;

class LocalNameUrl{
    public static function setFilter($query, $keyword){
        $query->whereRaw("product_links.local_sku LIKE ?", ["{$keyword}"]);
        $query->orWhereRaw("product_links.local_name LIKE ?", ["%{$keyword}%"]);
        
        if(mb_strlen($keyword) > 10)
            $query->orWhereRaw("product_links.pigu_barcode LIKE ?", ["%{$keyword}%"]);
        
    }
}