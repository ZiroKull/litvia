<?php

namespace App\DataTables;
use App\DataTables\Filters\Conditions;
use App\Models\Admin\HSCodes;
use App\Models\Admin\Supplier;

class HSCodesTable
{
    public static function getTable(){
        return datatables()->eloquent(HSCodes::orderBy('created_at', 'desc')->select())
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('tariff', function ($query, $keyword) {
            })
            ->filterColumn('hs_code', function ($query, $keyword) {
                $sql = "hs_code LIKE ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('hs_code', function ($hsCodes) {
                return $hsCodes->hs_code;
            })
            ->editColumn('tariff', function ($hsCodes) {
                if (isset($hsCodes->tariff)) {
                    return bcdiv($hsCodes->tariff, 1, 4);
                }
            })
            ->rawColumns(['hs_code', 'tariff'])
            ->make(TRUE);
    }
}
