<?php

namespace App\DataTables;
use App\Helpers\Helper;
use App\Models\Admin\Product;
use App\DataTables\Filters\Conditions;

class ProductsTable{
    public static function getTable(){
        return datatables()->eloquent(Product::getProducts())
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('cost_per_documents', function ($query, $keyword) {
            })
            ->filterColumn('balance_of_documents', function ($query, $keyword) {
            })
            ->filterColumn('pigu_product_count', function ($query, $keyword) {
            })
            ->filterColumn('local_sku', function ($query, $keyword) {
                //$query->whereRaw("product_links.local_sku LIKE ?", ["%{$keyword}%"]);
            })
            ->filterColumn('local_name', function ($query, $keyword) {
                $query->whereRaw("product_links.local_name LIKE ?", ["%{$keyword}%"]);
            })
            ->filterColumn('pigu_barcode', function ($query, $keyword) {
                $query->whereRaw("product_links.pigu_barcode LIKE ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', function ($product) {
                return '<a href="/admin/sale/edit/' . $product->id . '" class="btn btn-xs btn-primary">
                              <i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0);" class="del_ btn btn-xs btn-danger" id="delete-sale" data-id="' . $product->id . '"><i class="fas fa-trash-alt"></i></a>';
            })
            ->editColumn('change_user_id', function ($product) {
                return 'Виктор Игоревич Жевжик';
            })
            ->editColumn('change_user_date', function ($product) {
                $changeUserDate = ($product->change_user_date) ? date('d.m.Y H:i', strtotime($product->updated_at)) : '-';
                return $changeUserDate;
            })
            ->editColumn('product_name', function ($product) {
                $productLink = '<a href="/admin/products/links/' . $product->id . '">' . $product->product_name . '</a>';
                return $productLink;
            })
            ->editColumn('local_sku', function ($product) {
                return ($product->local_sku) ? $product->local_sku : '-';
            })
            ->editColumn('local_name', function ($product) {

                $productLink = Helper::getLocalNameUrl($product->local_sku, $product->pigu_barcode, $product->pigu_product_count, $product->product_page_id, $product->local_name);

                return $productLink;
            })
            ->editColumn('updated_at', function ($product) {
                return date('d.m.Y H:i', strtotime($product->updated_at));
            })
            ->editColumn('pigu_barcode', function ($product) {
                $barcodes = (array)json_decode($product->pigu_barcode, TRUE);
                $barcodes = ($barcodes) ? implode(',', $barcodes) : '-';
                return $barcodes;
            })
            ->rawColumns(['product_name', 'local_sku', 'local_name', 'pigu_barcode', 'updated_at', 'remaining_items', 'action'])
            ->make(TRUE);
    }
}
