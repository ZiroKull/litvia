<?php
namespace App\DataTables;
use App\Helpers\Helper;
use App\Models\Admin\ProductLinks;
use App\DataTables\Filters\LocalNameUrl;
use App\DataTables\Filters\Conditions;

class PriceAndSellTable{
    public static function getTable(){
        return datatables()->eloquent(ProductLinks::getProductLinksWithPrices())
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('product_prices.minimal_price', function ($query, $keyword) {
            })
            ->filterColumn('product_prices.target_price', function ($query, $keyword) {
            })
            ->filterColumn('product_prices.price_without_discount', function ($query, $keyword) {
            })
            ->filterColumn('product_prices.selling_price', function ($query, $keyword) {
            })
            ->filterColumn('product_prices.competitors_count', function ($query, $keyword) {
            })
            ->filterColumn('xml_count', function ($query, $keyword) {
            })
            ->filterColumn('local_name', function($query, $keyword) {
                LocalNameUrl::setFilter($query, $keyword);
            })
            ->editColumn('local_name', function ($product) {
                $productLink = '';
                //if($product->type == 'LT') {

                $productLink = Helper::getLocalNameUrl($product->local_sku, $product->pigu_barcode, $product->pigu_product_count , $product->product_page_id, $product->local_name);
                //}
                return $productLink;
            })
            ->editColumn('minimal_price', function ($product) {
                if (isset($product->minimal_price)) {
                    return bcdiv($product->minimal_price,1 ,2);
                }
            })
            ->editColumn('target_price', function ($product) {
                if (isset($product->target_price)) {
                    return bcdiv($product->target_price, 1, 2);
                }
            })
            ->editColumn('price_without_discount', function ($product) {
                if (isset($product->price_without_discount)) {
                    return bcdiv($product->price_without_discount, 1, 2);
                }
            })
            ->editColumn('selling_price', function ($product) {
                if (isset($product->selling_price)) {
                    return bcdiv($product->selling_price, 1, 2);
                }
            })
            ->editColumn('delivery_date', function ($product) {
                $daliveryDate = '';
                if($product->type == 'LT' && !empty($product->delivery_date)) {
                    $daliveryDate = date('Y-m-d', strtotime($product->delivery_date));
                }
                return $daliveryDate;
            })
            ->rawColumns(['local_name', 'minimal_price', 'target_price', 'price_without_discount' , 'selling_price' , 'delivery_date', 'warning'])
            ->order(function ($query) {
                //var_dump(request()->input('order')); die;
                $query->orderBy('local_name', 'asc');
                $query->orderBy('type', 'asc');
            })
            ->make(TRUE);
    }
}
