<?php

namespace App\DataTables;
use App\DataTables\Filters\Conditions;
use App\Models\Admin\SupplierLinks;
use Illuminate\Http\Request;

class SupplierLinksTable
{
    public static function getTable(Request $request){
        return datatables()->eloquent(SupplierLinks::getProducts($request->id)->orderBy('updated_at', 'desc'))
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('smallest_groups_suppliers.quantity_per_carton', function ($query, $keyword) {
            })
            ->filterColumn('smallest_groups_suppliers.carton_volume_m3', function ($query, $keyword) {
            })
            ->filterColumn('smallest_groups_suppliers.carton_weight_kg', function ($query, $keyword) {
            })
            ->editColumn('product_id', function ($supplierLink) {
                return '<a href="'. route('productLinks.show', $supplierLink->product_id) .'">'. $supplierLink->product_name .'</a>';
            })
            ->editColumn('rohs_file', function ($supplierLink) {
                return '<a href="#">Добавить</a>';
            })
            ->editColumn('ce_file', function ($supplierLink) {
                return '<a href="#">Добавить</a>';
            })
            ->rawColumns(['product_id', 'supplier_quality', 'rohs_file', 'ce_file', 'comments', 'quantity_per_carton',
                'carton_volume_m3', 'carton_weight_kg', 'is_accurate'])
            ->make(TRUE);
    }
}
