<?php
namespace App\DataTables;

use App\Models\Admin\ProductLinks;
use Illuminate\Http\Request;

class ProductLinksTable{
    public static function getTable(Request $request){
        return datatables()->eloquent(ProductLinks::where('product_id', $request->id))
            ->addColumn('action', function ($product) {
                return '<a href="javascript:void(0);" class="del_ btn btn-xs btn-danger" id="delete-product-link" data-id="' . $product->id . '"><i class="fas fa-trash-alt"></i></a>';
            })
            ->editColumn('local_name', function ($product) {
                return '<a href="'. route('productPage.show', $product->id) .'" >'. $product->local_name .'</a>';
            })
            ->editColumn('pigu_barcode', function ($product) {
                $barcodes = (array)json_decode($product->pigu_barcode, TRUE);
                $barcodes = ($barcodes) ? implode(',', $barcodes) : '-';
                return $barcodes;
            })
            ->editColumn('pigu_product_count', function ($product) {

                $pigu_product_count = 0;

                if (!is_null($product->pigu_product_count))
                    $pigu_product_count = $product->pigu_product_count;
                else
                    $pigu_product_count = '-';

                return $pigu_product_count;
            })
            ->rawColumns(['local_sku', 'local_name', 'pigu_barcode', 'pigu_product_count', 'action'])
            ->make(TRUE);
    }
}
