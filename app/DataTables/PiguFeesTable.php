<?php

namespace App\DataTables;
use App\DataTables\Filters\Conditions;
use App\Models\Admin\PiguFees;

class PiguFeesTable
{
    public static function getTable(){
        return datatables()->eloquent(PiguFees::select())
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('pigu_fee', function ($query, $keyword) {
            })
            ->filterColumn('category_number', function ($query, $keyword) {
                $sql = "category_number LIKE ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('category_name', function ($query, $keyword) {
                $sql = "category_name LIKE ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('category_number', function ($piguFee) {
                return $piguFee->category_number;
            })
            ->editColumn('category_name', function ($piguFee) {
                return $piguFee->category_name;
            })
            ->editColumn('pigu_fee', function ($piguFee) {
                if (isset($piguFee->pigu_fee)) {
                    return bcdiv($piguFee->pigu_fee, 1, 2);
                }
            })
            ->rawColumns(['category_number', 'category_name', 'pigu_fee'])
            ->make(TRUE);
    }
}
