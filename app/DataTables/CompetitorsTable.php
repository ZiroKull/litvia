<?php
namespace App\DataTables;
use App\Helpers\Helper;
use App\Models\Admin\Competitors;
use App\DataTables\Filters\LocalNameUrl;
use App\DataTables\Filters\Conditions;

class CompetitorsTable{
    public static function getTable(){
        return datatables()->eloquent(Competitors::getCompetitorsWithPrices())
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('product_prices.minimal_price', function ($query, $keyword) {
            })
            ->filterColumn('product_prices.target_price', function ($query, $keyword) {
            })
            ->filterColumn('product_prices.selling_price', function ($query, $keyword) {
            })
            ->filterColumn('coefficient_price', function ($query, $keyword) {
            })
            ->filterColumn('competitor_price', function ($query, $keyword) {
            })
            ->filterColumn('competitor_hours_delivery', function ($query, $keyword) {
            })
            ->filterColumn('competitor_min_order_amount', function ($query, $keyword) {
            })
            ->filterColumn('local_name', function($query, $keyword) {
                LocalNameUrl::setFilter($query, $keyword);
            })
            ->editColumn('local_name', function ($product) {

                $productLink = Helper::getLocalNameUrl($product->local_sku, $product->pigu_barcode, $product->pigu_product_count, $product->product_page_id, $product->local_name);

                return $productLink;
            })
            ->editColumn('minimal_price', function ($product) {
                if (isset($product->minimal_price)) {
                    return bcdiv($product->minimal_price,1 ,2);
                }
            })
            ->editColumn('target_price', function ($product) {
                if (isset($product->target_price)) {
                    return bcdiv($product->target_price, 1, 2);
                }
            })
            ->editColumn('price_without_discount', function ($product) {
                if (isset($product->price_without_discount)) {
                    return bcdiv($product->price_without_discount, 1, 2);
                }
            })
            ->editColumn('selling_price', function ($product) {
                if (isset($product->selling_price)) {
                    return bcdiv($product->selling_price, 1, 2);
                } else {
                    return bcdiv($product->formula_set_price, 1, 2);
                }
            })
            ->editColumn('coefficient_price', function ($product) {
                return bcdiv($product->coefficient_price, 1, 4);
            })
            ->editColumn('type', function ($product) {
                $url = explode(".", parse_url($product->url, PHP_URL_HOST));
                $type = strtoupper(end($url));
                return '<a target="_blank" href="'.$product->url.'">'.$type.'</a>';
            })
            ->addColumn('action', function ($product) {
                return '<a href="javascript:void(0);" class="del_ btn btn-xs btn-danger" id="delete-competitor" data-id="' . $product->id . '"><i class="fas fa-trash-alt"></i></a>';
            })
            ->rawColumns(['local_name', 'minimal_price', 'target_price', 'price_without_discount' , 'selling_price', 'coefficient_price' , 'type', 'delivery_date', 'action'])
            ->make(TRUE);
    }
}
