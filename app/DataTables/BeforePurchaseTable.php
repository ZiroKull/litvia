<?php

namespace App\DataTables;
use App\Helpers\Helper;
use App\Models\Admin\ProductLinks;
use App\Models\Admin\AvailabilityGoodsStock;
use App\DataTables\Filters\LocalNameUrl;
use App\DataTables\Filters\Conditions;

class BeforePurchaseTable
{
    
    public static function getTable()
    {
        return datatables()->eloquent(ProductLinks::getBeforePurchase())
            ->filter(function ($query) {
                Conditions::setConditions($query, request()->all());
            })
            ->filterColumn('pigu_product_count', function ($query, $keyword) {
            })
            ->filterColumn('many_days_product_last', function ($query, $keyword) {
            })
            ->filterColumn('avg_sales', function ($query, $keyword) {
            })
            ->filterColumn('product_name', function ($query, $keyword) {
                $query->whereRaw("products.product_name LIKE ?", ["%{$keyword}%"]);
            })
            ->filterColumn('local_name', function ($query, $keyword) {
                LocalNameUrl::setFilter($query, $keyword);
            })
            ->addColumn('action', function ($product) {
                return '<a href="javascript:void(0);" class="btn btn-xs btn-primary">Заказать</a>';
            })
            ->editColumn('local_vendor_name', function ($product) {
               return '-';
            })
            ->editColumn('large_group', function ($product) {
                return '-';
            })
            ->editColumn('local_name', function ($product) {
                return '<a href="#">'.$product->local_name.'</a>';
            })
            ->editColumn('pigu_product_count', function ($product) {
                return $product->pigu_product_count;
            })
            ->editColumn('product_name', function ($product) {
                $productLink = '<a href="/admin/products/links/' . $product->id . '">' . $product->product_name . '</a>';
                return $productLink;
            })
            ->editColumn('local_sku', function ($product) {
                return ($product->local_sku) ? $product->local_sku : '-';
            })
            ->editColumn('many_days_product_last', function ($product) {
                return (isset($product->many_days_product_last)) ? $product->many_days_product_last : '-';
            })
            ->editColumn('local_name', function ($product) {
            
                $productLink = Helper::getLocalNameUrl($product->local_sku, $product->pigu_barcode, $product->pigu_product_count, $product->id, $product->local_name);
            
                return $productLink;
            })
            ->editColumn('avg_sales', function ($product) {
                return $product->avg_sales;
            })
            ->editColumn('average_calculation_started_from', function ($product) {
                return ($product->average_calculation_started_from) ? '<a class="open-sku-history" data-sku="'.$product->local_sku.'" href="javascript:void(0);">'.date('d.m.Y', strtotime($product->average_calculation_started_from)).'</a>' : '-';
            })
            ->editColumn('pigu_barcode', function ($product) {
                $barcodes = (array)json_decode($product->pigu_barcode, TRUE);
                $barcodes = ($barcodes) ? implode(',', $barcodes) : '-';
                return $barcodes;
            })
            ->rawColumns(['local_vendor_name','average_calculation_started_from','large_group','product_name', 'local_name', 'pigu_product_count', 'avg_sales', 'many_days_product_last', 'action'])
            ->make(TRUE);
    }
}