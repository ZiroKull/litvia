<?php

namespace App\Listeners;

use Spatie\Backup\Events\BackupZipWasCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Admin\Settings;
use Mail;
use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;
use Illuminate\Support\Facades\Storage;
use App\Services\GoogleDriveService;

class BackupZipWasCreatedListener
{
   /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Spatie\Backup\Events\BackupZipWasCreated  $event
     * @return void
     */
    public function handle(BackupZipWasCreated $event)
    {
        //
        $backupEmail = Settings::where('key', 'backup_email')->first();
        //echo $backupEmail->value.' '.$event->pathToZip.PHP_EOL;
        if(!empty($event->pathToZip)) {
            //var_dump($this->refreshToken);
            //upload to disk

            $fileNameData = pathinfo($event->pathToZip);
            //var_dump($fileNameData);
            try {
                $file = Storage::disk('google')->put($fileNameData['basename'], file_get_contents($event->pathToZip));

                $driveService = new GoogleDriveService();
                $response = $driveService->listContents();
                //var_dump($response);

                if (!$file) {
                    if (!empty($backupEmail->value)) {
                        Mail::send('admin/mails/backup_mail', array(), function ($message) use ($backupEmail, $event) {
                            $message->to($backupEmail->value)->subject(__('admin_mails.mail_backup_error_subject'));
                            $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                            //$message->attach($event->pathToZip);
                        });
                    }
                }
            } catch(\Exception $e){
                if (!empty($backupEmail->value)) {
                    Mail::send('admin/mails/backup_mail', array(), function ($message) use ($backupEmail, $event) {
                        $message->to($backupEmail->value)->subject(__('admin_mails.mail_backup_error_subject'));
                        $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                        //$message->attach($event->pathToZip);
                    });
                }
            }
        }
        //dd($event->pathToZip);
    }

}
