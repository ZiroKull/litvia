<?php

namespace App\Validators\ProductPage;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'local_name' => 'required|unique:product_links,local_name',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'local_name.required' => 'Заполните поле "HS Код"',
            'local_name.unique' => 'Такое локальное имя уже находится в базе',
        ];
    }
}
