<?php

namespace App\Validators\PriceAndSell;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdatePriceAndSellRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.*.minimal_price' => 'nullable|numeric|gte:0',
            'data.*.target_price' => 'nullable|numeric|gte:0',
            'data.*.price_without_discount' => 'nullable|numeric|gte:0',
            'data.*.selling_price' => 'nullable|numeric|gte:0',
            'data.*.xml_count' => 'numeric|gte:0',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'data.*.minimal_price.numeric' => 'Данное поле должно быть числом!',
            'data.*.minimal_price.gte' => 'Можно вводить только положительные числа',
            'data.*.target_price.numeric' => 'Данное поле должно быть числом!',
            'data.*.target_price.gte' => 'Можно вводить только положительные числа',
            'data.*.price_without_discount.numeric' => 'Данное поле должно быть числом!',
            'data.*.price_without_discount.gte' => 'Можно вводить только положительные числа',
            'data.*.selling_price.numeric' => 'Данное поле должно быть числом!',
            'data.*.selling_price.gte' => 'Можно вводить только положительные числа',
            'data.*.xml_count.numeric' => 'Данное поле должно быть числом!',
            'data.*.xml_count.gte' => 'Можно вводить только положительные числа',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        $errorArray = [];

        $i = 0;
        foreach ($errors as $key => $error) {
            $errorArray[$i]['name'] = explode('.', $key);
            $errorArray[$i]['name'] = end($errorArray[$i]['name']);
            $errorArray[$i++]['status'] = $error;
        }

        $response = response()->json([
            'fieldErrors' => $errorArray,
            'data' => [],
            'debug' => []
        ]);

        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
