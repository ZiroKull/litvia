<?php

namespace App\Validators\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hs_code' => 'nullable|numeric',
            'weight_g' => 'nullable|numeric|gte:20|lte:2000',
            'lenght_cm' => 'nullable|numeric|gte:2|lte:60',
            'height_cm' => 'nullable|numeric|gte:2|lte:60',
            'width_cm' => 'nullable|numeric|gte:2|lte:60',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'hs_code.numeric' => '"HS Код" поле должно быть числовым значением',
            'weight_g.numeric' => '"Вес" поле должно быть числовым значением',
            'weight_g.gte' => 'Проверьте правильно ли указан вес',
            'weight_g.lte' => 'Проверьте правильно ли указан вес',
            'lenght_cm.numeric' => '"Длина" поле должно быть числовым значением',
            'height_cm.numeric' => '"Высота" поле должно быть числовым значением',
            'width_cm.numeric' => '"Ширина" поле должно быть числовым значением',
            'lenght_cm.gte' => 'Проверьте правильно ли указаны размеры',
            'height_cm.gte' => 'Проверьте правильно ли указаны размеры',
            'width_cm.gte' => 'Проверьте правильно ли указаны размеры',
            'lenght_cm.lte' => 'Проверьте правильно ли указаны размеры',
            'height_cm.lte' => 'Проверьте правильно ли указаны размеры',
            'width_cm.lte' => 'Проверьте правильно ли указаны размеры',
        ];
    }
}
