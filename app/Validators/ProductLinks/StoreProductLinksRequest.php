<?php

namespace App\Validators\ProductLinks;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductLinksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'local_name' => 'required|unique:product_links,local_name',
            'local_sku' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'local_name.required' => 'Заполните поле "Название нового продукта"',
            'local_name.unique' => 'Такое локальное имя уже находится в базе',
            'local_sku.required' => 'Заполните поле "Локальный SKU"',
        ];
    }
}
