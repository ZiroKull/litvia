<?php

namespace App\Validators\Competitors;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateCompetitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.*.coefficient_price' => 'numeric|gt:0',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'data.*.coefficient_price.numeric' => 'Значение должно быть числом',
            'data.*.coefficient_price.gt' => 'Значение должно быть больше 0'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        $errorArray = [];

        $i = 0;
        foreach ($errors as $key => $error) {
            $errorArray[$i]['name'] = explode('.', $key);
            $errorArray[$i]['name'] = end($errorArray[$i]['name']);
            $errorArray[$i++]['status'] = $error;
        }

        $response = response()->json([
            'fieldErrors' => $errorArray,
            'data' => [],
            'debug' => []
        ]);

        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
