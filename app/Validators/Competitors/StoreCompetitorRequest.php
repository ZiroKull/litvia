<?php

namespace App\Validators\Competitors;

use App\Rules\ProductIsExist;
use Illuminate\Foundation\Http\FormRequest;

class StoreCompetitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'local_name' => ['required', new ProductIsExist],
            'url' => 'required',
            'coefficient_price' => 'regex:/^[0-9]+[.,]?[0-9]*$/|gt:0'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'local_name.required' => 'Заполните поле "Название нового продукта"',
            'url.required' => 'Заполните поле "Ссылка"',
            'coefficient_price.regex' => 'Значение должно быть числом',
            'coefficient_price.gt' => 'Значение должно быть больше 0'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'coefficient_price'    => str_replace(",", ".", $this->input('coefficient_price')),
        ]);
    }
}
