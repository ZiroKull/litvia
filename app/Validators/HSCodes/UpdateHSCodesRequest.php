<?php

namespace App\Validators\HSCodes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class UpdateHSCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.*.tariff' => 'nullable|regex:/^[0-9]+[.,]?[0-9]*$/|min:0|lt:1'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'data.*.tariff.regex' => 'Тариф должен быть в диапозоне от 0 до 1',
            'data.*.tariff.lt' => 'Числа тарифа должно быть меньше 1',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        $errorArray = [];

        $i = 0;
        foreach ($errors as $key => $error) {
            $errorArray[$i]['name'] = explode('.', $key);
            $errorArray[$i]['name'] = end($errorArray[$i]['name']);
            $errorArray[$i++]['status'] = $error;
        }

        $response = response()->json([
            'fieldErrors' => $errorArray,
            'data' => [],
            'debug' => []
        ]);

        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
