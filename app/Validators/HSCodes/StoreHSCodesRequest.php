<?php

namespace App\Validators\HSCodes;

use Illuminate\Foundation\Http\FormRequest;

class StoreHSCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hs_code' => 'required|regex:/^\d+$/|unique:hs_codes,hs_code',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'hs_code.required' => 'Заполните поле "HS Код"',
            'hs_code.unique' => 'Такой код уже находится в базе',
            'hs_code.regex' => '"HS Код" поле должно быть числовым значением без точек и запятых',
        ];
    }
}
