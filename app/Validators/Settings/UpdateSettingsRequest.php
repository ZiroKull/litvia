<?php

namespace App\Validators\Settings;

use App\Rules\DivBy24;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'settings.sell_and_price_x_val' => 'numeric|gt:0',
            'settings.minimal_x_sell_product_price' => 'numeric|gt:0',
            'settings.email_send_sell_product' => 'email',
            'settings.creating_xml_specify_x' => 'numeric|gt:0',
            'settings.calculated_number_of_days_y' => 'numeric|gt:0|integer',
            'settings.more_than_x_hours' => ['numeric', new DivBy24],
            'settings.customer_pays_x_euros' => 'numeric|gte:0',
            'settings.divide_by_x_specify_x' => 'numeric|gt:0',
            'settings.calculation_average_sales' => 'numeric|integer',
            'settings.backup_email' => 'email',
            'settings.competitor_price_war' => 'numeric|gt:0',
            'settings.competitor_price_war_min' => 'numeric|gt:0',
            'settings.price_history_email' => 'email',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'settings.sell_and_price_x_val.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.sell_and_price_x_val.gt' => __('admin_validation_errors.number_be_gt_0'),
            'settings.minimal_x_sell_product_price.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.minimal_x_sell_product_price.gt' => __('admin_validation_errors.number_be_gt_0'),
            'settings.email_send_sell_product.email' => __('admin_validation_errors.enter_valid_email_format'),
            'settings.creating_xml_specify_x.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.creating_xml_specify_x.gt' => __('admin_validation_errors.number_be_gt_0'),
            'settings.calculated_number_of_days_y.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.calculated_number_of_days_y.gt' => __('admin_validation_errors.number_be_gt_0'),
            'settings.calculated_number_of_days_y.integer' => __('admin_validation_errors.only_integer_numbers'),
            'settings.more_than_x_hours.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.customer_pays_x_euros.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.customer_pays_x_euros.gte' => __('admin_validation_errors.number_be_gte_0'),
            'settings.divide_by_x_specify_x.numeric' => __('admin_validation_errors.data_be_a_number'),
            'settings.divide_by_x_specify_x.gt' => __('admin_validation_errors.number_be_gt_0'),
            'settings.calculation_average_sales.numeric' => __('admin_validation_errors.only_integer_numbers'),
            'settings.calculation_average_sales.integer' => __('admin_validation_errors.only_integer_numbers'),
            'settings.backup_email.email' => __('admin_validation_errors.enter_valid_email_format'),
            'settings.competitor_price_war.numeric' =>  __('admin_validation_errors.data_be_a_number'),
            'settings.competitor_price_war.gt' =>  __('admin_validation_errors.number_be_gt_0'),
            'settings.competitor_price_war_min.numeric' =>  __('admin_validation_errors.data_be_a_number'),
            'settings.competitor_price_war_min.gt' =>  __('admin_validation_errors.number_be_gt_0'),
            'settings.price_history_email.email' => __('admin_validation_errors.enter_valid_email_format'),
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->first();

        $response = response()->json([
            'status' => 'error',
            'errors' => $errors
        ]);

        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
