<?php

namespace App\Validators\Suppliers;

use Illuminate\Foundation\Http\FormRequest;

class StoreSuppliersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'local_company_name' => 'required|unique:suppliers,local_company_name',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'local_company_name.required' => 'Заполните поле "Локальное название постовщика"',
            'local_company_name.unique' => '"Локальное название постовщика" уже находится в базе',
        ];
    }
}
