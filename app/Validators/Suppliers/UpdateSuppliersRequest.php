<?php

namespace App\Validators\Suppliers;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSuppliersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'local_company_name' => 'required|unique:suppliers,local_company_name,'. $this->id .'|min:3',
            'chat_link' => 'nullable|url',
            'shop_link' => 'nullable|url',
            'manager_email' => 'nullable|email',
            'warehouse_email' => 'nullable|email',
            'manager_phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:8',
            'warehouse_phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:8',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'local_company_name.required' => 'Заполните поле "Локальное название постовщика"',
            'local_company_name.unique' => '"Локальное название постовщика" уже находится в базе',
            'local_company_name.min' => '"Локальное название постовщика" слишком короткое',
            'chat_link.url' => 'Не верный формат ссылки у чата',
            'shop_link.url' => 'Не верный формат ссылки у магазина',
            'manager_email.email' => 'Не верный формат Email мэнеджера',
            'warehouse_email.email' => 'Не верный формат Email склада',
            'manager_phone.min' => 'Не верный формат номера телефона у менеджера',
            'manager_phone.regex' => 'Не верный формат номера телефона у менеджера',
            'warehouse_phone.min' => 'Не верный формат номера телефона у склада',
            'warehouse_phone.regex' => 'Не верный формат номера телефона у склада',
        ];
    }
}
