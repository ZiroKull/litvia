<?php

namespace App\Validators\PiguFees;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdatePiguFeesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.*.category_name' => 'nullable|unique:pigu_fees,category_name',
            'data.*.pigu_fee' => 'nullable|regex:/^[0-9]+[.,]?[0-9]*$/|gte:0.01|lte:0.4'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'data.*.category_name.unique' => 'Название категории уже введёно',
            'data.*.pigu_fee.regex' => 'Тариф должен быть в диапозоне больше 0 до 0.4',
            'data.*.pigu_fee.lte' => 'Числа тарифа должно быть меньше или равно 0.4',
            'data.*.pigu_fee.gte' => 'Числа тарифа должно быть больше или равно 0.01',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        $errorArray = [];

        $i = 0;
        foreach ($errors as $key => $error) {
            $errorArray[$i]['name'] = explode('.', $key);
            $errorArray[$i]['name'] = end($errorArray[$i]['name']);
            $errorArray[$i++]['status'] = $error;
        }

        $response = response()->json([
            'fieldErrors' => $errorArray,
            'data' => [],
            'debug' => []
        ]);

        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
