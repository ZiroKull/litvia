<?php

namespace App\Validators\PiguFees;

use Illuminate\Foundation\Http\FormRequest;

class StorePiguFeesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_number' => 'required|numeric|unique:pigu_fees,category_number|gte:0',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'category_number.required' => 'Заполните поле "Номер pigu категории	"',
            'category_number.unique' => 'Такой номер категории уже введён',
            'category_number.numeric' => '"Номер pigu категории" должно быть числовым значением',
            'category_number.gte' => '"Номер pigu категории" должно быть больше или равен 0',
        ];
    }
}
