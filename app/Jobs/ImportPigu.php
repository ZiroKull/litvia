<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Bus\Batchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use App\Models\Admin\Import;
use App\Models\Admin\Product;
use App\Models\Admin\ProductLinks;
use App\Models\Admin\ProductPrices;
use Illuminate\Support\Facades\DB;

class ImportPigu implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 0;

    public function __construct()
    {
        //
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...

            return;
        }

        //1. логинимся в pigu
        $response = Http::withBody('{"username": "' . env('PIGU_LOGIN') . '","password": "' . env('PIGU_PASSWORD') . '"}', 'application/json')->post('https://pmpapi.pigugroup.eu/v2/login');

        if ($response->successful()) {
            $tokenData = $response->json();
            
            if (!empty($tokenData['token'])) {
                
                $offersArray = array();
                //2. Делаем запрос /v2/sellers/{sellerId}/offers, вынимаем offers
                $responseOffers = Http::withHeaders([
                    'Authorization' => 'Pigu-mp '.$tokenData['token'],
                ])->get('https://pmpapi.pigugroup.eu/v2/sellers/'.env('PIGU_SELLER_ID').'/offers', ['limit'=>100]);
                
                $offersData = $responseOffers->json();
                
                //generate offers array
                $total = (int)floor($offersData['meta']['total_count'] / 100);
                $offersArray = $offersData['offers'];
                
                for($i = 1; $i <= $total; $i++){
                    $offset = $i * 100;
                    $responseOffers = Http::withHeaders([
                        'Authorization' => 'Pigu-mp '.$tokenData['token'],
                    ])->get('https://pmpapi.pigugroup.eu/v2/sellers/'.env('PIGU_SELLER_ID').'/offers', ['limit'=>100, 'offset' => $offset]);
                    $sdditionalOffersData = $responseOffers->json();
                    
                    $offersArray = array_merge($offersArray,$sdditionalOffersData['offers']);
                }
                
                // var_dump($offersArray);
                // var_dump($total , $offersData['offers']);
               
                //get fbp array
                $fbpArray = array();
    
                $responseFbpOffers = Http::withHeaders([
                    'Authorization' => 'Pigu-mp '.$tokenData['token'],
                ])->get('https://pmpapi.pigugroup.eu/v2/fbp/'.env('PIGU_SELLER_ID'));
    
                $fbpArray = $responseFbpOffers->json();
                //3. https://pmpapi.pigugroup.eu/v2/fbp/9992761 ищем по extelnal_id добавляем в общий массив с офферс
                // var_dump($fbpArray[0]['offer']['modification']['external_id']); die;
                // var_dump(array_column($fbpArray, '')); die;
                
                foreach ($offersArray as $key => $offer) {
                    foreach ($fbpArray as $fbp){
                        if($offer['modification']['external_id'] == $fbp['offer']['modification']['external_id']){
                            //var_dump($offer['modification']['external_id']); die;
                            $offersArray[$key]['fbp_amount'] = $fbp['amount'];
                        }
                    }
                }
    
                /*
                 * b1_id => связывающее поле для обновления или создания (https://www.screencast.com/t/nymeSgd8S)
                    barcode => https://www.screencast.com/t/wTDnnOkM
                    external_id => Поле для поиска в
                    /v2/fbp/{sellerId}
                    https://www.screencast.com/t/WLssqM85 - поиск по extelnal_id fbp
                    вынимаем amount
                 */
                
                $resultArray = array();
                foreach ($offersArray as $key=>$offer){
                    if($offer['app_name'] == 'pigu.lt') {
                        $resultArray[$key]['b1_id'] = $offer['modification']['sku'];
                        $resultArray[$key]['amount'] = (!empty($offer['fbp_amount'])) ? $offer['fbp_amount'] : 0;
                        $resultArray[$key]['ean_codes'] = json_encode($offer['modification']['ean_codes']);
                    }
                }
                
                // get links
                $productLinks = ProductLinks::all();
                $linksArray = array();
                foreach ($productLinks as $productLink){
                    foreach ($offersArray as $key => $offer) {
                        if($offer['app_name'] == 'pigu.lt') {
                            if ($productLink->local_sku == $offer['modification']['sku']) {
                                //var_dump($productLink->id); die;
                                foreach ($fbpArray as $fbp) {
                                    if ($offer['modification']['external_id'] == $fbp['offer']['modification']['external_id']) {
                                        //var_dump($offer['modification']['external_id']); die;
                                        $offersArray[$key]['fbp_amount'] = $fbp['amount'];
                                        //$linksArray[$key]['pigu_product_count'] = $fbp['amount'];
                                    }
                                }
                                
                                $linksArray[$key]['b1_id'] = $offer['modification']['sku'];
                                $linksArray[$key]['pigu_product_count'] = (!empty($offer['fbp_amount'])) ? $offer['fbp_amount'] : 0;
                                $linksArray[$key]['pigu_barcode'] = json_encode($offer['modification']['ean_codes']);
                            }
                        }
                    }
                }
    
                $priceTypes = ['LT','LV','EE','FI'];
                $position =  ProductPrices::max('position');
                $position =  ($position) ? $position : 1;

                foreach ($linksArray as $link){
                    $productLink = ProductLinks::updateOrCreate(
                        ['local_sku' => $link['b1_id']],
                        ['pigu_barcode' =>  $link['pigu_barcode'], 'pigu_product_count' => $link['pigu_product_count']]
                    );

                    foreach ($priceTypes as $type) {
                        $productPrice = ProductPrices::where('product_link_id', $productLink->id)->where('type', $type)->first();
                        if ($productPrice === null) {
                            ProductPrices::insert(
                                ['product_link_id' => $productLink->id, 'type' => $type, 'position' => $position++]
                            );
                        }
                    }
                }
                //var_dump($linksArray); die;
                
                //4. Сохраняем в таблицу
                /* foreach ($resultArray as $result) {
                     $product = Product::updateOrCreate(
                         ['b1_id' => $result['b1_id']],
                         ['remaining_items' => $result['amount'], 'barcode' => $result['ean_codes']]
                     );
                     //create links
                     ProductLinks::updateOrCreate(
                         ['local_sku' => $result['b1_id']],
                         ['pigu_barcode' =>  $link['pigu_barcode'], 'pigu_product_count' => $link['pigu_product_count']]
                     );
                }*/
                
                Import::where('id', 2)->update(['updated_at' =>  DB::raw('NOW()')]);
                //var_dump(json_encode($resultArray));
                //die;
            }
            
        } else {
            \Log::channel('importPigu')->error('Data: '.print_r($response->json(), true));
        }
        
    }
}
