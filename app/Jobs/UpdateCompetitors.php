<?php

namespace App\Jobs;

use App\Models\Admin\Import;
use Illuminate\Bus\Queueable;
use Illuminate\Bus\Batchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Competitors;
use App\Models\Admin\ParserProducts;
use App\Models\Admin\Settings;
use App\Services\PiguServices;
use Mail;


class UpdateCompetitors implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 0;
    
    public function __construct()
    {
        //
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...
            return;
        }
        
        //update all competitors price
        $competitors = Competitors::all();
        $piguServices = new PiguServices();
        foreach ($competitors as $competitor) {
            
            if (!empty($competitor->url)) {
                
                $requestData = array('coefficient_price' => $competitor->coefficient_price);
                $responseData = $piguServices->getResponse($competitor->url);
                
                
                $piguData = $piguServices->getProductData($responseData, $requestData);
                
                $competitorId = $competitor->id;
                $toEmail = Settings::where('key', 'email_send_sell_product')->first();

                //update competitor
                if (!empty($piguData)) {
                    if (empty($piguData['errors'])) {
                        
                        $updateData = ['competitor_name' => (!empty($piguData['competitor_name'])) ? $piguData['competitor_name'] : NULL,
                            'competitor_price' => (!empty($piguData['competitor_price'])) ? $piguData['competitor_price'] : NULL,
                            'competitor_hours_delivery' => (isset($piguData['competitor_hours_delivery'])) ? $piguData['competitor_hours_delivery'] : 2400,
                            'competitor_min_order_amount' => (!empty($piguData['competitor_min_order_amount'])) ? $piguData['competitor_min_order_amount'] : 0,
                            'changed_competitor_price' => (isset($piguData['changed_competitor_price'])) ? $piguData['changed_competitor_price'] : NULL
                        ];
                        
                        \Log::channel('updateCompetitors')->info('Update link: ' . $competitor->url . '!');
                        \Log::channel('updateCompetitors')->info('Data: ' . print_r($piguData, TRUE));
                        
                        Competitors::where('id', $competitor->id)->update($updateData);
                        
                    } else {
                        
                        // send mail
                        $errorsXml = array('error' => __('admin_mails.mail_update_competitor_price_error', array('competitorID' => $competitorId)), 'errorText' => $piguData['errors']);
                        
                        Mail::send('admin/mails/update_competitors', array('errors' => $errorsXml), function ($message) use ($toEmail, $competitorId) {
                            $message->to($toEmail->value)->subject(__('admin_mails.mail_update_competitor_price_error_submit'));
                            $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                        });
                        
                        \Log::channel('updateCompetitors')->error('Error update competitor (ID ' . $competitor->id . '): ' . $piguData['errors'] . '!');
                        
                    }
                } else {
                    
                    $errorText = __('admin_mails.mail_pigu_server_error_text_comp_id', array('competitorID' => $competitorId)) . '<br>' . __('admin_mails.mail_pigu_server_error_text_comp_link', array('competitorLink' => $competitor->url));
                    $errorsXml = array('error' => __('admin_mails.mail_pigu_server_error_text'), 'errorText' => $errorText);
                    
                    Mail::send('admin/mails/update_competitors', array('errors' => $errorsXml), function ($message) use ($toEmail, $competitorId) {
                        $message->to($toEmail->value)->subject(__('admin_mails.mail_pigu_server_error'));
                        $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                    });
                    
                    \Log::channel('updateCompetitors')->error('Server error: (competitor ID: ' . $competitor->id . '), Link: ' . $competitor->url);

                }

                //calculate click interval
                $seconds = 64800;
                $totalCompetitors = (int)$competitors->count();
                
                $avgClickCount = $seconds / $totalCompetitors;
                $minInterval = $avgClickCount * 0.4;
                $maxInterval = $avgClickCount * 1.6;
                
                $sleepCount = rand($minInterval, $maxInterval);
                
                \Log::channel('updateCompetitors')->info('Min interval is: ' . $minInterval . ', Max interval is: ' . $maxInterval . ', Total interval: ' . $sleepCount . '!');
                
                //add to parser_products
                $piguXlsData = $piguServices->getProductDataXls($responseData, array('product_link' => $competitor->url));
                ParserProducts::insert($piguXlsData);
                
                sleep($sleepCount);
                
            }
        }
        
        Import::where('id', 3)->update(['updated_at' => DB::raw('NOW()')]);
    }
}
