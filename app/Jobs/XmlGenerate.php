<?php

namespace App\Jobs;

use App\Models\Admin\ProductLinks;
use App\Models\Admin\Settings;
use Illuminate\Bus\Queueable;
use Illuminate\Bus\Batchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Import;
use App\Models\Admin\Competitors;
use App\Models\Admin\ProductPrices;
use App\Models\Admin\PriceHistory;
use App\Models\Admin\JobTimestamps;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Helper;
use Mail;


class XmlGenerate implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 0;
    
    public function __construct()
    {
        //
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...
            
            return;
        }
        //
        
        //price History
        $competitorPriceWar = Settings::where('key', 'competitor_price_war')->first();
        $competitorPriceWarMin = Settings::where('key', 'competitor_price_war_min')->first();
        $historyToMailArray = array();
        
        $competitorHistoryPrices = Competitors::getMinCompetitorPriceByTypeAll()->get();
        //var_dump($competitorHistoryPrices); die;
        foreach ($competitorHistoryPrices as $price) {
            $historyPrice = NULL;
            $addToHistory = FALSE;
            $addToHistorySecond = FALSE;
            $productPrice = ProductPrices::where('product_link_id', $price->product_link_id)->where('type', $price->type)->first();
            
            if ($price->min <= $productPrice->minimal_price) {
                $historyPrice = $productPrice->minimal_price;
                $addToHistory = TRUE;
            } else {
                if ($price->min > $productPrice->target_price) {
                    $historyPrice = $productPrice->target_price;
                } else {
                    $historyPrice = $price->competitor_price;
                    $addToHistorySecond = TRUE;
                }
            }
            
            if ($addToHistory || $addToHistorySecond) {
                if (is_null($productPrice->selling_price)) {
                    
                    
                    $getLatestPriceHistory = PriceHistory::where('competitor_id', $price->id)->latest()->first();
                    
                    //
                    if (!empty($getLatestPriceHistory)) {
                        if ($price->competitor_price > $getLatestPriceHistory->competitor_price) {
                            PriceHistory::where('competitor_id', $price->id)->delete();
                        } else if ($price->competitor_price == $getLatestPriceHistory->competitor_price) {
                            continue;
                        } else {
                            //clear oldest
                            $counter = 1;
                            $priceHistory = PriceHistory::where('competitor_id', $price->id)->orderBy('updated_at', 'desc')->get();
                            foreach ($priceHistory as $item) {
                                if ($counter > 10) {
                                    $item->delete();
                                }
                                $counter++;
                            }
                        }
                    }
                    
                    //var_dump($getLatestPriceHistory->updated_at->format('Y-m-d H:i:s')); die;
                    
                    if ($addToHistory) {
                        if (empty($getLatestPriceHistory) || ($getLatestPriceHistory->competitor_price >= $productPrice->minimal_price)) {
                            PriceHistory::create(['competitor_id' => $price->id, 'competitor_price' => $historyPrice]);
                        }
                    } else if ($addToHistorySecond) {
                        PriceHistory::create(['competitor_id' => $price->id, 'competitor_price' => $historyPrice]);
                    }
                    
                    
                    // prepared history to mail
                    //foreach ($competitors as $competitor) {
                    if (!empty($competitorPriceWar->value) && !empty($competitorPriceWarMin->value)) {
                        //var_dump($competitorPriceWar->value, $competitorPriceWarMin->value);
                        $historyToMail = PriceHistory::where('competitor_id', $price->id)->whereRaw('updated_at >= NOW() - INTERVAL ' . $competitorPriceWar->value . ' DAY')->get();
                        //var_dump(count($historyToMail), $competitorPriceWarMin->value);
                        if (count($historyToMail) > $competitorPriceWarMin->value) {
                            foreach ($historyToMail as $key => $history) {
                                $historyToMailArray[$price->id][] = $history;
                            }
                        }
                    }
                    //}
                }
            }
            
        }
    
        $mailTextHistory = array();
        $i = 1;
        //var_dump($historyToMailArray); die;
        foreach ($historyToMailArray as $key => $history){
            //var_dump($key);
            $productData = Competitors::leftJoin('product_links', 'product_links.id', '=', 'competitors.product_link_id')
                ->select('competitors.url', 'competitors.type', 'product_links.local_name', 'product_links.local_sku')
                ->where('competitors.id', $key)
                ->first();
        
            $mailTextHistory[$i]['productData'] = $productData;
        
            foreach ($history as $item){
                $mailTextHistory[$i]['productHistory'][] = $item;
            }
        
            $i++;
        }
        //var_dump($mailTextHistory); die;
        $timestampHistoryMail = JobTimestamps::where('type', 'send_xml_generate_history_mail')->first();
        //var_dump($timestampHistoryMail->change_time); die;
        if(!empty($timestampHistoryMail->change_time)) {
            $date1 = date_create($timestampHistoryMail->change_time);
            $date2 = date_create();
            $diff = date_diff($date1,$date2);
            $hour = $diff->h  + ($diff->days * 24);
            //var_dump($hour); die;
            //var_dump($hour);
            if($hour>=24) {
                $toHistoryEmail = Settings::where('key', 'price_history_email')->first();
                if (!empty($toHistoryEmail->value)) {
                    if (!empty($mailTextHistory)) {
                        Mail::send('admin/mails/price_history_mail', array('mailTextHistory' => $mailTextHistory), function ($message) use ($toHistoryEmail) {
                            $message->to($toHistoryEmail->value)->subject
                            (__('admin_mails.mail_competitor_down_prices'));
                            $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                        });
                    }
                }
                JobTimestamps::where('type', 'send_xml_generate_history_mail')->update(['change_time'=> DB::raw('NOW()')]);
            }
        }
        
        //echo '222';
        //die;
        //calculate formula set price
        $competitorPrices = Competitors::getMinCompetitorPriceByType()->get();
        $excludePriceIds = array();
        foreach ($competitorPrices as $price) {

            $formulaSetPrice = NULL;

            $productPrice = ProductPrices::where('product_link_id', $price->product_link_id)->where('type', $price->type)->first();

            if ($price->min <= $productPrice->minimal_price) {
                $formulaSetPrice = $productPrice->minimal_price;
            } else {
                if ($price->min > $productPrice->target_price) {
                    $formulaSetPrice = $productPrice->target_price;
                } else {
                    $formulaSetPrice = $price->min;
                }
            }
            //var_dump($formulaSetPrice);
            ProductPrices::where('id', $productPrice->id)->update(['formula_set_price' => $formulaSetPrice]);
            $excludePriceIds[] = $productPrice->id;
            //var_dump($productPrice); die;
        }
        //var_dump($historyToMailArray);
        
        //die;
        ProductPrices::whereNotIn('id', $excludePriceIds)->update(['formula_set_price' => DB::raw('target_price')]);
        
        
        ProductLinks::whereRaw('delivery_date <= NOW()')->update(['delivery_date' => NULL, 'xml_count' => 0]);
        
        //update competitors price
        $settings = Settings::all();
        
        $valArray = array();
        
        foreach ($settings as $value) {
            $valArray[$value->key] = $value->value;
        }
        
        $competitors = Competitors::all();
        $mailText = '';
        $errors = 0;
        
        foreach ($competitors as $competitor) {
            
            $result = Helper::calculateCompetitorPrice(array(
                'more_than_x_hours' => $valArray['more_than_x_hours'],
                'customer_pays_x_euros' => $valArray['customer_pays_x_euros'],
                'divide_by_x_specify_x' => $valArray['divide_by_x_specify_x'],
                'competitor_hours_delivery' => $competitor->competitor_hours_delivery,
                'competitor_price' => $competitor->competitor_price,
                'coefficient_price' => (float)$competitor->coefficient_price,
                'competitor_min_order_amount' => (!empty($competitor->competitor_min_order_amount)) ? $competitor->competitor_min_order_amount : 0
            ));
            
            if (empty($result['error'])) {
                $updateData = [
                    'changed_competitor_price' => (isset($result['changed_competitor_price'])) ? $result['changed_competitor_price'] : NULL
                ];
                Competitors::where('id', $competitor->id)->update($updateData);
            } else {
                $mailText .= __('admin_mails.mail_pigu_server_error_text_comp_id', array('competitorID' => $competitor->id)) . ' ' . $result['error'] . '<br>';
                $errors++;
            }
        }
        
        if ($errors > 0) {
            $toEmail = Settings::where('key', 'email_send_sell_product')->first();
            $errorsXml = array('error' => __('admin_mails.mail_pigu_xml_comp_err_error'), 'errorText' => $mailText);
            Mail::send('admin/mails/update_competitors', array('errors' => $errorsXml), function ($message) use ($toEmail) {
                $message->to($toEmail->value)->subject(__('admin_mails.mail_pigu_xml_comp_err_subject'));
                $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
            });
        }
        
        //check database
        
        //generate xml
        $dom = new \DOMDocument();
        $dom->encoding = 'utf-8';
        $dom->xmlVersion = '1.0';
        $dom->formatOutput = TRUE;
        
        $root = $dom->createElement('products');
        
        //clear warnings and add xml
        ProductPrices::query()->update(['warning' => '']);
        
        $xmlProducts = ProductLinks::getProductLinksWithPrices(NULL)
            ->orderBy('local_name', 'asc')
            ->orderBy('type', 'asc')
            ->get();
        
        ProductLinks::query()->update(['xml_located' => 0]);
        
        $temp = NULL;
        $product = NULL;
        $deletedProducts = array();
        $errors = array();
        $errorsXml = array();
        $localNames = array();
        $errorsDatabase = array();
        
        $setPriceToLimit = Settings::where('key', 'set_price_to_limit')->first();
        $minimalProductPrice = Settings::where('key', 'minimal_x_sell_product_price')->first();
        $sellPriceVal = Settings::where('key', 'sell_and_price_x_val')->first();
        $creatingXmlSpecifyX = Settings::where('key', 'creating_xml_specify_x')->first();
        $calculatedNumberOfDaysY = Settings::where('key', 'calculated_number_of_days_y')->first();
        
        // make validator
        foreach ($xmlProducts as $key => $xmlProduct) {
            $errorText = '';
            $errorDatabaseText = '';
            
            $localNames[$xmlProduct->product_link_id] = $xmlProduct->local_name;
            
            //database Checks
            if (!$xmlProduct->minimal_price || !$xmlProduct->target_price || !$xmlProduct->formula_set_price) {
                $errorDatabaseText .= __('admin_messages.warning_db_minimum_price_target_price') . "<br>";
                $deletedProducts[] = $xmlProduct->product_link_id;
            } else {
                // 5 пункт
                if ($xmlProduct->minimal_price > $xmlProduct->target_price ||
                    ($xmlProduct->minimal_price > $xmlProduct->price_without_discount && !is_null($xmlProduct->price_without_discount)) ||
                    $xmlProduct->minimal_price > $xmlProduct->formula_set_price) {
                    $deletedProducts[] = $xmlProduct->product_link_id;
                    $errorDatabaseText .= __('admin_messages.warning_lower_price_minimum_price') . "<br>";
                }
                
                // 7 пункт
                if ($xmlProduct->target_price > $xmlProduct->price_without_discount) {
                    if (!is_null($xmlProduct->price_without_discount)) {
                        $deletedProducts[] = $xmlProduct->product_link_id;
                        $errorDatabaseText .= __('admin_messages.warning_target_price_greater_undiscounted_price') . "<br>";
                    }
                }
                
                // 8 пункт
                if ($xmlProduct->target_price < $xmlProduct->formula_set_price) {
                    $deletedProducts[] = $xmlProduct->product_link_id;
                    $errorDatabaseText .= __('admin_messages.warning_formula_set_price_greater_target_price') . "<br>";
                }
                
                // 9 пункт
                if (!$xmlProduct->less_8_euro) {
                    if ($xmlProduct->minimal_price < $sellPriceVal->value ||
                        $xmlProduct->target_price < $sellPriceVal->value ||
                        (!is_null($xmlProduct->price_without_discount) && $xmlProduct->price_without_discount < $sellPriceVal->value) ||
                        (!is_null($xmlProduct->selling_price) && $xmlProduct->selling_price < $sellPriceVal->value) ||
                        $xmlProduct->formula_set_price < $sellPriceVal->value
                    ) {
                        $deletedProducts[] = $xmlProduct->product_link_id;
                        $errorDatabaseText .= __('admin_messages.warning_one_prices_is_less_x', array('sellPriceVal' => (float)$sellPriceVal->value)) . "<br>";
                    }
                }
                
                // 10 пункт
                if ($xmlProduct->minimal_price < $minimalProductPrice->value ||
                    $xmlProduct->target_price < $minimalProductPrice->value ||
                    (!is_null($xmlProduct->price_without_discount) && $xmlProduct->price_without_discount < $minimalProductPrice->value) ||
                    (!is_null($xmlProduct->selling_price) && $xmlProduct->selling_price < $minimalProductPrice->value) ||
                    $xmlProduct->formula_set_price < $minimalProductPrice->value
                ) {
                    $deletedProducts[] = $xmlProduct->product_link_id;
                    $errorDatabaseText .= __('admin_messages.warning_minimum_price_settings', array('minimalProductPrice' => (float)$minimalProductPrice->value)) . "<br>";
                }
                
                // 11 пункт
                if ($xmlProduct->formula_set_price > 10000) {
                    $deletedProducts[] = $xmlProduct->product_link_id;
                    $errorDatabaseText .= __('admin_messages.warning_selling_price_high');
                }
                
            }
            
            //make checks xml
            if (!$setPriceToLimit->value) {
                $priceAfterDiscountVal = ($xmlProduct->selling_price) ? $xmlProduct->selling_price : $xmlProduct->formula_set_price;
                $priceAfterDiscountVal = bcdiv($priceAfterDiscountVal, 1, 2);
                $priceVal = ($xmlProduct->price_without_discount < $priceAfterDiscountVal) ? $priceAfterDiscountVal : $xmlProduct->price_without_discount;
                $priceVal = bcdiv($priceVal, 1, 2);
            } else {
                $priceAfterDiscountVal = 9999;
                $priceVal = 9999;
            }
            
            if (!empty($xmlProduct->delivery_date)) {
                $origin = date_create($xmlProduct->delivery_date);
                $target = date_create(date('Y-m-d'));
                $interval = date_diff($origin, $target);
                $days = (int)$interval->format('%a');
                $coefficient = ($creatingXmlSpecifyX->value) ? $creatingXmlSpecifyX->value : 0;
                $maxVal1 = (int)round($days * $coefficient);
                $calculatedNumberOfDaysYVal2 = ($calculatedNumberOfDaysY) ? $calculatedNumberOfDaysY->value : 0;
                $collectionHoursVal = max($maxVal1, $calculatedNumberOfDaysYVal2) * 24;
            } else {
                $collectionHoursVal = 2400;
            }
            
            $xmlEan = json_decode($xmlProduct->pigu_barcode);
            $eanVal = (!empty($xmlEan)) ? array_shift($xmlEan) : NULL;
            
            if ($priceVal < (float)$minimalProductPrice->value || $priceAfterDiscountVal < (float)$minimalProductPrice->value) {
                $deletedProducts[] = $xmlProduct->product_link_id;
                $errorText .= __('admin_messages.warning_minimal_sell_price') . ' ' . (float)$minimalProductPrice->value . '<br>';
            }
            
            if ($priceVal < $priceAfterDiscountVal) {
                $deletedProducts[] = $xmlProduct->product_link_id;
                $errorText .= __('admin_messages.less_calculated_price') . '<br>';
            }
            
            
            if ((!$xmlProduct->less_8_euro) && ($priceVal < (float)$sellPriceVal->value || $priceAfterDiscountVal < (float)$sellPriceVal->value)) {
                $deletedProducts[] = $xmlProduct->product_link_id;
                $errorText .= __('admin_messages.warning_minimal_less_8_euro', array('sellPriceVal' => (float)$sellPriceVal->value)) . '<br>';
            }
            
            if (empty($xmlProduct->local_sku)
                || empty($eanVal)
                || empty($priceVal)
                || empty($priceAfterDiscountVal)
                || is_null($xmlProduct->xml_count)
                || is_null($collectionHoursVal)
            ) {
                $deletedProducts[] = $xmlProduct->product_link_id;
                $errorText .= __('admin_messages.warning_values_null') . '<br>';
            }
            
            if (empty($xmlProduct->selling_price)) {
                if ($priceVal < $xmlProduct->minimal_price || $priceAfterDiscountVal < $xmlProduct->minimal_price) {
                    $deletedProducts[] = $xmlProduct->product_link_id;
                    $errorText .= __('admin_messages.warning_price_less_minimum_selling') . '<br>';
                }
            }
            
            $divPrice = (!empty($xmlProduct->selling_price)) ? $xmlProduct->selling_price / 3 : 0;
            
            if ($divPrice > $xmlProduct->target_price) {
                $minimalPrice = ($priceVal && $priceAfterDiscountVal) ? min([$priceVal, $priceAfterDiscountVal]) : 0;
                
                if ($minimalPrice > $xmlProduct->target_price) {
                    $deletedProducts[] = $xmlProduct->product_link_id;
                    $errorText .= __('admin_messages.warning_greater_than_target_price') . '<br>';
                }
            }
            
            
            if (!empty($errorDatabaseText) && $xmlProduct->is_xml) {
                $errorsDatabase[$xmlProduct->product_link_id]['error'] = $errorDatabaseText;
                $errorsDatabase[$xmlProduct->product_link_id]['product'] = $xmlProduct;
            }
            
            //
            
            if (!empty($errorText)) {
                $errors[$xmlProduct->product_link_id]['error'] = $errorText;
                ProductPrices::where('id', $xmlProduct->id)->update(['warning' => $errorText]);
            }
            
            $errorFullText = $errorDatabaseText . $errorText;
            ProductPrices::where('id', $xmlProduct->id)->update(['warning' => $errorFullText]);
            
            if (!$xmlProduct->is_xml) {
                $errorsXml[$xmlProduct->product_link_id]['error'] = __('admin_messages.warning_is_not_xml');
                ProductPrices::where('id', $xmlProduct->id)->update(['warning' => __('admin_messages.warning_is_not_xml')]);
                $deletedProducts[] = $xmlProduct->product_link_id;
                unset($errors[$xmlProduct->product_link_id]);
            }
            
            switch ($xmlProduct->type) {
                case 'LT':
                    $product = $dom->createElement('product');
                    
                    $domAttribute = $dom->createAttribute('id');
                    $domAttribute->value = $xmlProduct->product_link_id;
                    $product->appendChild($domAttribute);
                    
                    $sku = $dom->createElement('sku', $xmlProduct->local_sku);
                    $product->appendChild($sku);
                    
                    
                    $ean = $dom->createElement('ean', $eanVal);
                    $product->appendChild($ean);
                    
                    $priceLt = $dom->createElement('price_lt', $priceVal);
                    $product->appendChild($priceLt);
                    
                    $priceAfterDiscountLt = $dom->createElement('price_after_discount_lt', $priceAfterDiscountVal);
                    $product->appendChild($priceAfterDiscountLt);
                    
                    $root->appendChild($product);
                    break;
                case 'LV':
                    $priceLv = $dom->createElement('price_lv', $priceVal);
                    $product->appendChild($priceLv);
                    
                    $priceAfterDiscountLv = $dom->createElement('price_after_discount_lv', $priceAfterDiscountVal);
                    $product->appendChild($priceAfterDiscountLv);
                    
                    break;
                case 'EE':
                    $priceEe = $dom->createElement('price_ee', $priceVal);
                    $product->appendChild($priceEe);
                    
                    $priceAfterDiscountEe = $dom->createElement('price_after_discount_ee', $priceAfterDiscountVal);
                    $product->appendChild($priceAfterDiscountEe);
                    break;
                case 'FI':
                    $priceFi = $dom->createElement('price_fi', $priceVal);
                    $product->appendChild($priceFi);
                    
                    $priceAfterDiscountFi = $dom->createElement('price_after_discount_fi', $priceAfterDiscountVal);
                    $product->appendChild($priceAfterDiscountFi);
                    
                    $stock = $dom->createElement('stock', (int)$xmlProduct->xml_count);
                    $product->appendChild($stock);
                    
                    $collectionHours = $dom->createElement('collection_hours', $collectionHoursVal);
                    $product->appendChild($collectionHours);
                    break;
            }
        }
        
        $dom->appendChild($root);
        
        //delete elements
        if ($setPriceToLimit->value)
            unset($deletedProducts);
        
        if (!empty($deletedProducts)) {
            $theDocument = $dom->documentElement;
            $list = $theDocument->getElementsByTagName('product');
            $deletedProducts = array_unique($deletedProducts);
            
            $nodeToRemove = NULL;
            foreach ($list as $domElement) {
                $attrValue = (int)$domElement->getAttribute('id');
                $sku = (!empty($domElement->getElementsByTagName('sku')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('sku')->item(0)->nodeValue : '';
                
                
                if (in_array($attrValue, $deletedProducts)) {
                    $nodeToRemove[] = $domElement;
                    
                    $currentProduct = array(
                        'id' => $attrValue,
                        'local_name' => $localNames[$attrValue],
                        'sku' => $sku,
                        'ean' => (!empty($domElement->getElementsByTagName('ean')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('ean')->item(0)->nodeValue : '',
                        'price_lt' => (!empty($domElement->getElementsByTagName('price_lt')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_lt')->item(0)->nodeValue : '',
                        'price_after_discount_lt' => (!empty($domElement->getElementsByTagName('price_after_discount_lt')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_after_discount_lt')->item(0)->nodeValue : '',
                        'price_lv' => (!empty($domElement->getElementsByTagName('price_lv')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_lv')->item(0)->nodeValue : '',
                        'price_after_discount_lv' => (!empty($domElement->getElementsByTagName('price_after_discount_lv')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_after_discount_lv')->item(0)->nodeValue : '',
                        'price_ee' => (!empty($domElement->getElementsByTagName('price_ee')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_ee')->item(0)->nodeValue : '',
                        'price_after_discount_ee' => (!empty($domElement->getElementsByTagName('price_after_discount_ee')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_after_discount_ee')->item(0)->nodeValue : '',
                        'price_fi' => (!empty($domElement->getElementsByTagName('price_fi')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_fi')->item(0)->nodeValue : '',
                        'price_after_discount_fi' => (!empty($domElement->getElementsByTagName('price_after_discount_fi')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('price_after_discount_fi')->item(0)->nodeValue : '',
                        'stock' => (!empty($domElement->getElementsByTagName('stock')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('stock')->item(0)->nodeValue : '',
                        'collection_hours' => (!empty($domElement->getElementsByTagName('collection_hours')->item(0)->nodeValue)) ? $domElement->getElementsByTagName('collection_hours')->item(0)->nodeValue : '',
                    );
                    
                    if (!empty($errorsXml[$attrValue]['error']))
                        $errorsXml[$attrValue]['product'] = $currentProduct;
                    
                    if (!empty($errors[$attrValue]['error']))
                        $errors[$attrValue]['product'] = $currentProduct;
                    
                    ProductLinks::where('local_sku', $sku)->update(['xml_located' => 0]);
                    
                } else {
                    
                    ProductLinks::where('local_sku', $sku)->update(['xml_located' => 1]);
                }
                
            }
            
            if ($nodeToRemove) {
                foreach ($nodeToRemove as $item) {
                    $theDocument->removeChild($item);
                }
            }
        }
        
        //var_dump($errors); die;
        //echo $dom->saveXML();
        //die;
        
        Storage::put('xml/1.xml', $dom->saveXML());
        
        $toEmail = Settings::where('key', 'email_send_sell_product')->first();
        
        //send mails
        // xonzonex@gmail.com $toEmail->value
        if (!empty($toEmail)) {
            if (!empty($errors)) {
                Mail::send('admin/mails/xml_mail', array('errors' => $errors), function ($message) use ($toEmail) {
                    $message->to($toEmail->value)->subject
                    (__('admin_mails.mail_xml_subject'));
                    $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                });
            }
            if (!empty($errorsXml)) {
                Mail::send('admin/mails/xml_mail', array('errors' => $errorsXml), function ($message) use ($toEmail) {
                    $message->to($toEmail->value)->subject
                    (__('admin_mails.mail_xml_subject_no_xml'));
                    $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                });
            }
            if (!empty($errorsDatabase)) {
                Mail::send('admin/mails/database_mail', array('errors' => $errorsDatabase), function ($message) use ($toEmail) {
                    $message->to($toEmail->value)->subject
                    (__('admin_mails.mail_xml_subject_no_xml'));
                    $message->from(env('MAIL_FROM_ADDRESS'), __('admin_mails.mail_xml_from'));
                });
            }
        }
        
        Import::where('id', 4)->update(['updated_at' => DB::raw('NOW()')]);
    }
}
