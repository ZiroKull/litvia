<?php

namespace App\Jobs;

use App\Models\Admin\Import;
use App\Models\Admin\AvailabilityGoodsStock;
use App\Models\Admin\AvailabilityGoodsStockTemp;
use App\Models\Admin\ProductLinks;
use App\Models\Admin\Settings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Batchable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;


class UpdateAvailabilityGoodsStock implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $timeout = 0;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...
            
            return;
        }
        //1. add to database
        $productLinksData = ProductLinks::all();
        foreach ($productLinksData as $productLink) {
            //var_dump($productLink); die;
            AvailabilityGoodsStock::updateOrCreate(
                [
                    'local_sku' => $productLink->local_sku,
                    'date' => date('Y-m-d')
                ],
                [
                    'is_stock' => ($productLink->pigu_product_count > 0) ? 1 : 0,
                    'sales_count' => 0,
                ]
            );
        }
        
        
        //2. update data
        $response = Http::withBody('{"username": "' . env('PIGU_LOGIN') . '","password": "' . env('PIGU_PASSWORD') . '"}', 'application/json')->post('https://pmpapi.pigugroup.eu/v2/login');
        
        if ($response->successful()) {
            
            $tokenData = $response->json();
            
            if (!empty($tokenData['token'])) {
                $daysAgo = [4, 6, 10, 20, 40, 60, 90, 120];
                
                //
                // $curDate = date('Y-m-d');
                
                foreach ($daysAgo as $day) {
                    
                    //if offset call to next
                    // tested dates
                    /*
                     * 2022-10-08 (интересуют заказы 86596571, 16716533, 16715417)
                       2022-10-01 (интересует заказ 16648730)
                       2022-07-11 (интересует 82481671)
                     */
                    $limit = 20;
                    $offset = 0;
                    $createdAtFrom = date("Y-m-d", strtotime("-" . $day . " days"));
                    //var_dump($createdAtFrom); die;
                    $isExistDate = AvailabilityGoodsStock::where('date', $createdAtFrom)->count();
                    
                    if ($isExistDate > 0) {
                        $responseCountOffers = Http::withHeaders([
                            'Authorization' => 'Pigu-mp ' . $tokenData['token'],
                        ])->get('https://pmpapi.pigugroup.eu/v2/sellers/' . env('PIGU_SELLER_ID') . '/orders', ['limit' => $limit, 'offset' => $offset, 'created_at_from' => $createdAtFrom, 'created_at_to' => $createdAtFrom]);
                        
                        
                        $ordersData = $responseCountOffers->json();
                        
                        $pages = (!empty($ordersData['meta']['fbp_orders_count'])) ? (int)round($ordersData['meta']['fbp_orders_count'] / $limit) : 0;
                        
                        for ($i = 0; $i <= $pages; $i++) {
                            
                            $responseOffers = Http::withHeaders([
                                'Authorization' => 'Pigu-mp ' . $tokenData['token'],
                            ])->get('https://pmpapi.pigugroup.eu/v2/sellers/' . env('PIGU_SELLER_ID') . '/orders', ['limit' => $limit, 'offset' => $offset, 'created_at_from' => $createdAtFrom, 'created_at_to' => $createdAtFrom]);
                            
                            $ordersData = $responseOffers->json();
                            //var_dump($ordersData);
                            
                            $offset += $limit;
                            
                            if (!empty($ordersData['orders'])) {
                                foreach ($ordersData['orders'] as $order) {
                                    if (!empty($order['statuses'])) {
                                        foreach ($order['statuses'] as $status) {
                                            
                                            if ($status['status'] == 'completed') {
                                                if (!empty($order['products'])) {
                                                    foreach ($order['products'] as $product) {
                                                        AvailabilityGoodsStockTemp::create(['amount' => $product['amount'], 'sku' => (int)$product['modification']['sku'], 'is_completed' => TRUE]);
                                                    }
                                                }
                                                //die;
                                            }
                                        }
                                    }
                                    //
                                    
                                }
                            }
                        }
                        
                        //get temp data
                        $tempGroupedData = AvailabilityGoodsStockTemp::select(DB::raw('sum(amount) as sum, sku'))->groupBy('sku')->get();
                        
                        // add to availability_goods_stock table
                        
                        // calculate fields
                        
                        foreach ($tempGroupedData as $data) {
                            AvailabilityGoodsStock::where('local_sku', $data->sku)->where('date', $createdAtFrom)->update(['sales_count' => $data->sum]);
                        }
                        
                        AvailabilityGoodsStockTemp::truncate();
                        //die;
                    }
                }
                
            }
            
            
        } else {
            \Log::channel('UpdateAvailabilityGoodsStock')->error('Server error: not connect to pigu API. ' . print_r($response->json(), TRUE));
        }
        
        //3. calculate availability_goods_stock
        $calculationAverageSales = Settings::where('key', 'calculation_average_sales')->first();
        $startDate = date("Y-m-d", strtotime("-5 days"));
        
        foreach ($productLinksData as $productLink) {
            
            $limit = (!empty($calculationAverageSales->value)) ? $calculationAverageSales->value : 30;
            
            $resData = AvailabilityGoodsStock::where('local_sku', $productLink->local_sku)
                ->where('date', '<=', $startDate)
                ->where('is_stock', 1)
                ->orderBy('date', 'DESC')
                ->limit($limit)
                ->get();
            
            $salesDaysCount = 0;
            
            $count = count($resData);
            
            foreach ($resData as $data) {
                $salesDaysCount += $data->sales_count;
            }
            
            if ($count < $limit && $productLink->pigu_product_count == 0) {
                $count = ($count > 0) ? $count : 1;
                $avgSales = round(($salesDaysCount / $count), 3);
            } else {
                $avgSales = (!empty($salesDaysCount)) ? round(($salesDaysCount / $limit), 3) : 0;
            }
            
            if ($salesDaysCount == 0) {
                $avgSales = 0.001;
            }
            
            ProductLinks::where('local_sku', $productLink->local_sku)->update(['sales_30_days_count' => $salesDaysCount, 'avg_sales' => $avgSales]);
            
            //die;
        }
        
        // 3. recalculate many_days_product_last
        $productLinksData = ProductLinks::all();
        
        foreach ($productLinksData as $productLink) {
            $manyDaysProductLast = ($productLink->avg_sales > 0) ? round(($productLink->pigu_product_count / $productLink->avg_sales)) : 55555;
            ProductLinks::where('id', $productLink->id)->update(['many_days_product_last' => $manyDaysProductLast]);
        }
        //var_dump(111); die;
        
        Import::where('id', 5)->update(['updated_at' => DB::raw('NOW()')]);
    }
}
