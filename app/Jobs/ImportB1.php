<?php

namespace App\Jobs;

use App\Models\Admin\Product;
use App\Models\Admin\Import;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class ImportB1 implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 0;

    public function __construct()
    {
        //
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...

            return;
        }
        //
        $response = Http::withHeaders([
            'B1-Api-Key' => env('B1_API_KEY'),
        ])->post('https://www.b1.lt/api/reference-book/items/list', [
            'page' => 1,
            'rows' => 10,
            'filters' => [
                'groupOp' => 'AND',
                'rules' => [
                    [
                        'field' => 'attributeName',
                        'op' => 'nc',
                        'data' => 'Paslaugos'
                    ]
                ],
            ],
        ]);
        
        if ($response->successful()) {
            $products = $response->json();
            //var_dump($products); die;
            if (!empty($products['pages'])) {
                for ($i = 1; $i <= $products['pages']; $i++) {

                    $response = Http::withHeaders([
                        'B1-Api-Key' => env('B1_API_KEY'),
                    ])->post('https://www.b1.lt/api/reference-book/items/list', [
                        'page' => $i,
                        'rows' => 10,
                        'filters' => [
                            'groupOp' => 'AND',
                            'rules' => [
                                [
                                    'field' => 'attributeName',
                                    'op' => 'nc',
                                    'data' => 'Paslaugos'
                                ]
                            ],
                        ],
                    ]);
    
                    if ($response->successful()) {
                        
                        $products = $response->json();
                        
                        foreach ($products["data"] as $productItem) {
        
                            $isProductExist = Product::where('b1_id', $productItem['id'])->exists();
        
                            // var_dump($isProductExist);
                            if ($isProductExist) {
                                // update product
                                Product::where('b1_id', $productItem['id'])->update(['product_name' => $productItem['name'],
                                    'cost_per_documents' => (!empty($productItem['cost'])) ? $productItem['cost'] : 0,
                                    'balance_of_documents' => (!empty($productItem['stock'])) ? $productItem['stock'] : 0,
                                   ]);
                            } else {
                                // create product
                                $product = new Product();
                                $product->b1_id = $productItem['id'];
                                $product->change_user_id = 0;
                                $product->product_name = $productItem['name'];
                                $product->cost_per_documents = (!empty($productItem['cost'])) ? $productItem['cost'] : 0;
                                $product->balance_of_documents = (!empty($productItem['stock'])) ? $productItem['stock'] : 0;
                                $product->save();
                            }
        
                        }
                    }
                    
                }
                
            }
            Import::where('id', 1)->update(['updated_at' =>  DB::raw('NOW()')]);
        } else {
            \Log::channel('importB1')->error('Data: '.print_r($response->json(), true));
        }
    }
}
