<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateCompetitorsCount;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;

class UpdateCompetitorsCountCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCompetitorsCount:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Competitors count';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //UpdateCompetitorsCount::dispatchSync();
        Bus::batch([
            new UpdateCompetitorsCount
        ])->then(function (Batch $batch) {
        
        })->catch(function (Batch $batch, Throwable $e) {
            //var_dump($e->getMessage()); die;
        })->name('Update Competitors Count')->dispatch();
    
        return Command::SUCCESS;
    }
}
