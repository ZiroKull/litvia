<?php

namespace App\Console\Commands;

use App\Jobs\UpdateAvailabilityGoodsStock;
use Illuminate\Console\Command;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;


class AvailabilityGoodsStockCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateAvailabilityGoodsStock:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process availability of goods in stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //UpdateAvailabilityGoodsStock::dispatchSync();
        Bus::batch([
            new UpdateAvailabilityGoodsStock
        ])->then(function (Batch $batch) {
        
        })->catch(function (Batch $batch, Throwable $e) {
            //var_dump($e->getMessage()); die;
        })->name('Availability Goods Stock')->dispatch();
    
        return Command::SUCCESS;
    }
}
