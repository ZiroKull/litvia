<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateCompetitors;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;

class UpdateCompetitorsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCompetitors:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Competitors table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //UpdateCompetitors::dispatchSync();
        Bus::batch([
            new UpdateCompetitors
        ])->then(function (Batch $batch) {

        })->catch(function (Batch $batch, Throwable $e) {
            //var_dump($e->getMessage()); die;
        })->name('Update Competitors')->dispatch();

        return Command::SUCCESS;
    }
}
