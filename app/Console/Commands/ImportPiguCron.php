<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Jobs\ImportPigu;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;

class ImportPiguCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportPigu:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Pigu cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Bus::batch([
            new ImportPigu
        ])->then(function (Batch $batch) {

        })->catch(function (Batch $batch, Throwable $e) {
            //var_dump($e->getMessage()); die;
        })->name('Import Pigu')->dispatch();

        return Command::SUCCESS;
    }
}
