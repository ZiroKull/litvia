<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ImportB1;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;

class ImportB1Cron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportB1:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products From B1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Bus::batch([
            new ImportB1
        ])->then(function (Batch $batch) {

        })->catch(function (Batch $batch, Throwable $e) {
            //var_dump($e->getMessage()); die;
        })->name('Import B1')->dispatch();

        return Command::SUCCESS;
    }
}
