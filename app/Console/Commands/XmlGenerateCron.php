<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\XmlGenerate;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;


class XmlGenerateCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'XmlGenerate:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Xml Generate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //XmlGenerate::dispatchSync();
        Bus::batch([
            new XmlGenerate()
        ])->then(function (Batch $batch) {

        })->catch(function (Batch $batch, Throwable $e) {
            //var_dump($e->getMessage()); die;
        })->name('Xml Generate')->dispatch();

        return Command::SUCCESS;
    }
}
