<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\UpdateCompetitorsCron::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('ImportB1:cron')->dailyAt('3:00')->withoutOverlapping(1);
         $schedule->command('ImportPigu:cron')->dailyAt('4:00')->withoutOverlapping(1);
         $schedule->command('UpdateCompetitors:cron')->dailyAt('7:00')->withoutOverlapping(1);
         $schedule->command('XmlGenerate:cron')->cron('0 0,1,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,23 * * *')->withoutOverlapping(1);
         $schedule->command('UpdateAvailabilityGoodsStock:cron')->dailyAt('4:30')->withoutOverlapping(1);
         $schedule->command('UpdateCompetitorsCount:cron')->dailyAt('3:30')->withoutOverlapping(1);
         $schedule->command('translations:export competitors_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export import_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export menu_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export price_and_sell_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export products_links_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export products_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export settings_other_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export before_purchase_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_buttons')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_competitors')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_import')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_mails')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_messages')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_price_and_sell')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_products')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_products_links')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_settings')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_translations')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_validation_errors')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_before_update')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export suppliers_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_suppliers')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export hs_codes_tooltips')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('translations:export admin_hs_codes')->everyFiveMinutes()->withoutOverlapping(1);
         $schedule->command('backup:run --only-db')->dailyAt('5:30')->withoutOverlapping(1);
         $schedule->command('backup:run')->weeklyOn(1, '5:00')->withoutOverlapping(1);
         //$schedule->command('queue:work')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
