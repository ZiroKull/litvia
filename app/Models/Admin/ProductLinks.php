<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProductLinks extends Model
{
    use HasFactory;
    protected $table = 'product_links';
    protected $fillable = ['pigu_barcode', 'pigu_product_count'];

    public function prices()
    {
        return $this->belongsToMany('App\Model\Admin\ProductPrices', '', 'id', 'product_link_id');
    }

    public static function getProductLinksWithPrices($id = NULL, $data = NULL)
    {
        $productLinksData = ProductLinks::leftJoin('product_prices', 'product_links.id', '=', 'product_prices.product_link_id')
            ->select('product_links.local_name',
                'product_links.local_sku',
                'product_links.pigu_barcode',
                'product_links.pigu_product_count',
                'product_prices.id',
                'product_links.id as product_page_id',
                'product_prices.product_link_id',
                'product_links.product_id',
                'product_links.is_xml',
                'product_links.delivery_date',
                'product_links.xml_count',
                'product_links.xml_located',
                'product_links.less_8_euro',
                'product_links.warning',
                'product_prices.type',
                'product_prices.competitors_count',
                'product_prices.minimal_price',
                'product_prices.target_price',
                'product_prices.price_without_discount',
                'product_prices.selling_price',
                'product_prices.formula_set_price',
                'product_prices.warning'
            );

        if (!empty($id)) {
            $productLinksData->where('product_prices.id', $id);
        }

        if (!empty($data['type'])) {
            $productLinksData->where('product_prices.type', $data['type']);
        }

        if (!empty($data['product_link_id'])) {
            $productLinksData->where('product_prices.product_link_id', $data['product_link_id']);
        }

        if (!empty($data['local_name'])) {
            $productLinksData->where('product_links.local_name', $data['local_name']);
        }

        if (!empty($data['is_xml'])) {
            $productLinksData->where('product_links.is_xml', $data['is_xml']);
        }

        return $productLinksData;
    }

    public static function getBeforePurchase($id = NULL, $data = NULL)
    {
        $productLinksData = ProductLinks::leftJoin('products', 'product_links.product_id', '=', 'products.id')
            ->leftJoin(DB::raw('(
                      SELECT
                        sub1.*
                    FROM
                        (
                        SELECT
                            DATE AS average_calculation_started_from,
                            availability_goods_stock.local_sku
                        FROM
                            `availability_goods_stock`
                        GROUP BY
                            availability_goods_stock.local_sku
                    ) AS t1
                    INNER JOIN LATERAL(
                        SELECT
                            MIN(
                                average_calculation_started_from
                            ) as average_calculation_started_from,
                            local_sku
                        FROM
                            (
                            SELECT
                                DATE AS average_calculation_started_from,
                                local_sku,
                                is_stock
                            FROM
                                `availability_goods_stock`
                            WHERE
                                DATE <= "'.date("Y-m-d", strtotime("-5 days")).'" AND is_stock = 1 AND local_sku = t1.local_sku
                            ORDER BY
                                `date`
                            DESC
                        LIMIT 30
                        ) AS ts
                    ) AS sub1
                    ON
                        sub1.local_sku = t1.local_sku
                    ORDER BY
                        sub1.local_sku
                    DESC
                    ) AS avgs'), function ($join) {
                $join->on('avgs.local_sku', '=', 'product_links.local_sku');
            })
            ->select('products.product_name',
                'products.id',
                'product_links.local_name',
                'product_links.avg_sales',
                'product_links.local_sku',
                'product_links.pigu_barcode',
                'product_links.pigu_product_count',
                'product_links.many_days_product_last',
                'avgs.average_calculation_started_from');


        return $productLinksData;
    }

}
