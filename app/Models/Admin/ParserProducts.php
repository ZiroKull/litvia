<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParserProducts extends Model
{
    use HasFactory;
    protected $fillable = ['seller_min_cart_sum'];
}
