<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HSCodes extends Model
{
    use HasFactory;

    protected $table = 'hs_codes';

    protected $guarded = ['id'];
}
