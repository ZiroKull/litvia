<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PiguFees extends Model
{
    use HasFactory;

    protected $table = 'pigu_fees';

    protected $guarded = ['id'];
}
