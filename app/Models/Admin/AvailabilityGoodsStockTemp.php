<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvailabilityGoodsStockTemp extends Model
{
    use HasFactory;
    
    protected $table = 'availability_goods_stock_temp';
    protected $guarded = [];
}
