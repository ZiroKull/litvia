<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    public static function getProducts()
    {
        $productData = Product::leftJoin('product_links', 'products.id', '=', 'product_links.product_id')->select('products.*', 'product_links.id as product_page_id', 'product_links.local_sku', 'product_links.local_name'  , 'product_links.pigu_barcode', 'product_links.pigu_product_count');
        return $productData;
    }

    public function  hsCode()
    {
       return $this->hasOne(HSCodes::class, 'id', 'hs_code_id');
    }
}
