<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Competitors;

class ProductPrices extends Model
{
    use HasFactory;
    protected $table = 'product_prices';
    protected $fillable = ['product_link_id'];
    
    public function productLinks(){
        return $this->belongsTo(ProductLinks::class, 'product_link_id', 'id');
    }
    
    public static function updateCompetitorsCount(){
        $productPricesData = ProductPrices::all();
        foreach ($productPricesData as $value){
            if(!empty($value->productLinks->local_sku) && !empty($value->type)) {
                $count = Competitors::getCompetitorsCountBySkuAndType($value->productLinks->local_sku, $value->type);
                ProductPrices::where('id', $value->id)->update(['competitors_count' => $count]);
            }
            //var_dump($count); die;
        }
    }
}
