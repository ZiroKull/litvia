<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupplierLinks extends Model
{
    use HasFactory;

    protected $table = 'smallest_groups_suppliers';

    protected $guarded = ['id'];

    public static function getProducts($supplier_id  = NULL, $product_id  = NULL)
    {
        $productData = SupplierLinks::leftJoin('products', 'products.id', '=', 'smallest_groups_suppliers.product_id')
            ->leftJoin('suppliers', 'suppliers.id', '=', 'smallest_groups_suppliers.supplier_id')
            ->select(
                'smallest_groups_suppliers.*',
                'products.product_name',
                'suppliers.local_company_name as supplier_name',
            );

        if (!empty($supplier_id)) {
            $productData->where('smallest_groups_suppliers.supplier_id', $supplier_id);
        }

        if (!empty($product_id)) {
            $productData->where('smallest_groups_suppliers.product_id', $product_id);
        }

        return $productData;
    }
}
