<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTime;
use DB;

class AvailabilityGoodsStock extends Model
{
    use HasFactory;
    
    protected $table = 'availability_goods_stock';
    protected $guarded = [];
    
    public static function getAvailabilityStockDatesBySku($sku)
    {

        $temp = '';
        $i = 0;
        $dateString = '';
        $salesCount = 0;
        
        $resData = self::getAvailabilityStockDatesBySkuTable($sku);
        
        foreach ($resData as $key => $data) {
            //var_dump($temp, $data->date);
            $salesCount = $data->sales_30_days_count;
            
            if ($key == 0) {
                $startDateInterval = $data->date;
                $dateString = date("Y/m/d", strtotime($startDateInterval));
            }
            
            if (!empty($temp) && !empty($data->date)) {
                
                $interval = date_diff(new DateTime($temp), new DateTime($data->date));
                $intervalDay = (int)$interval->format('%a');
                
                if ($intervalDay == 1) {
                    $dateString .= ' - ' . date("Y/m/d", strtotime($data->date));
                } else {
                    $startDateInterval = $data->date;
                    $dateString .= ', ' . date("Y/m/d", strtotime($startDateInterval));
                    $i++;
                }
                
            }
            $temp = $data->date;
        }
        
        //var_dump($dateString);
        $preDates = explode(',', $dateString);
        //var_dump($preDates);
        foreach ($preDates as $key => $date) {
            $expDate = explode('-', $date);
            if (count($expDate) > 2) {
                $preDates[$key] = current($expDate) . '-' . end($expDate);
            }
        }
        
        $resDates = (!empty($preDates)) ? implode(',', $preDates) : '';
        
        return array('count' => count($resData), 'resDates' => $resDates, 'salesCount' => $salesCount);
    }
    
    public static function getLatestAvailabilityStockDatesBySku($sku){
        
        $resData = self::getAvailabilityStockDatesBySkuTable($sku);
        $latestDate = null;
        
        if(count($resData)){
            $latestDate = current(current($resData));
        }

        return (!empty($latestDate->date)) ? $latestDate->date : null;

    }
    
    private static function getAvailabilityStockDatesBySkuTable($sku){
        $calculationAverageSales = Settings::where('key', 'calculation_average_sales')->first();
        
        $startDate = date("Y-m-d", strtotime("-5 days"));
        $limit = (!empty($calculationAverageSales->value)) ? $calculationAverageSales->value : 30;
        
        $resData = DB::table(DB::raw("(
            SELECT * FROM `availability_goods_stock`
                WHERE
                `date` <= '".$startDate."' AND
                `local_sku` = '".$sku."'
                AND `is_stock` = 1
                ORDER BY date DESC
                LIMIT ".$limit."
            ) as sub"))
            ->leftJoin('product_links', 'sub.local_sku', '=', 'product_links.local_sku')
            ->orderBy('sub.date', 'ASC')
            ->select('sub.*','product_links.sales_30_days_count')
            ->get();
        return $resData;
    }
}
