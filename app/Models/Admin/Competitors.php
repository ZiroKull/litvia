<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\ProductPrices;



class Competitors extends Model
{
    use HasFactory;

    public static function getCompetitorsWithPrices($id = null, $data = null){
        $productCompetitorsData = Competitors::leftJoin('product_links', 'product_links.id', '=', 'competitors.product_link_id')
            //->leftJoin('product_prices', 'product_links.id', '=', 'product_prices.product_link_id')
            ->leftJoin('product_prices', function($query){
                $query->on('product_links.id', '=', 'product_prices.product_link_id');
                $query->on('competitors.type', '=', 'product_prices.type');
            })
            ->select('product_links.local_name',
                'product_links.local_sku',
                'product_links.pigu_barcode',
                'product_links.pigu_product_count',
                'product_links.id as product_page_id',
                'product_prices.minimal_price',
                'product_prices.target_price',
                'product_prices.price_without_discount',
                'product_prices.selling_price',
                'product_prices.type',
                'product_prices.formula_set_price',
                'competitors.id',
                'competitors.coefficient_price',
                'competitors.url',
                'competitors.competitor_name',
                'competitors.competitor_price',
                'competitors.competitor_hours_delivery',
                'competitors.competitor_min_order_amount');

        if(!empty($id)){
            $productCompetitorsData->where('competitors.id', $id);
        }

        //$productCompetitorsData->where('competitors.type', \DB::raw('product_prices.type'));

        return $productCompetitorsData;
    }

    public static function getMinCompetitorPriceByType(){
        $minCompetitorPrices = Competitors::select(\DB::raw('MIN(changed_competitor_price) as min, product_link_id, type'))->groupBy('product_link_id','type');
        return $minCompetitorPrices;
    }

    //refactor to eloquent
    public static function getMinCompetitorPriceByTypeAll(){
        $minCompetitorPricesAll = \DB::table(\DB::raw('(SELECT DISTINCT competitors.id, t1.min, t1.product_link_id, t1.TYPE, competitors.`competitor_price`
                FROM (
                  SELECT MIN(changed_competitor_price) AS MIN, product_link_id, TYPE
                  FROM competitors
                  GROUP BY product_link_id, TYPE
                ) AS t1
                INNER JOIN competitors ON (
                  t1.MIN = competitors.changed_competitor_price
                  AND t1.TYPE = competitors.type
                  AND t1.product_link_id = competitors.product_link_id
                )
                ORDER BY `t1`.`product_link_id` ASC) as tr'))
        ->select('tr.id', 'tr.min', 'tr.product_link_id', 'tr.type', 'tr.competitor_price');
        return $minCompetitorPricesAll;
    }

    public static function getCompetitorsCountBySkuAndType($sku, $type){
        $competitorsData =  Competitors::leftJoin('product_links', 'product_links.id', '=', 'competitors.product_link_id')
            ->where('product_links.local_sku', $sku)
            ->where('competitors.type', $type)->get();
        return (int)$competitorsData->count();
    }
    /*
     * SELECT
       *
        FROM
            `competitors`
        LEFT JOIN `product_prices` ON product_prices.product_link_id = `competitors`.`product_link_id`
        LEFT JOIN `product_links` ON competitors.product_link_id = product_links.id
        WHERE product_links.local_sku=170 AND competitors.type='LT'
        GROUP BY
     */
    protected static function booted()
    {
        static::created(function ($competitor) {
            //var_dump($competitor->product_link_id, $competitor->type); die;
            ProductPrices::where('product_link_id', $competitor->product_link_id)->where('type', $competitor->type)->update(['competitors_count' => \DB::raw('competitors_count + 1')]);
        });
        static::deleted(function ($competitor) {
            //ProductPrices::updateCompetitorsCount();
            ProductPrices::where('product_link_id', $competitor->product_link_id)->where('type', $competitor->type)->update(['competitors_count' => \DB::raw('competitors_count - 1')]);
        });
    }
}
