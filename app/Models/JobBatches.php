<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobBatches extends Model
{
    use HasFactory;
    protected $table = 'job_batches';

    public static function getProcessedBatches($name)
    {
        $processed = JobBatches::where('name', $name)->where('pending_jobs', 1)->where('failed_jobs', '!=' , 1);
        $processedCount = $processed->count();
        return ($processedCount) ? true : false;
    }
}
