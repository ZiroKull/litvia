<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

class Helper
{
    public static function calculateCompetitorPrice($data)
    {
        $result = array();

        if ((is_numeric($data['more_than_x_hours']) && $data['more_than_x_hours'] >= 0) &&
            (is_numeric($data['customer_pays_x_euros']) && $data['customer_pays_x_euros'] >= 0) &&
            (is_numeric($data['divide_by_x_specify_x']) && $data['divide_by_x_specify_x'] > 0)) {
            $val1 = (($data['competitor_hours_delivery'] > $data['more_than_x_hours'])) ? ($data['competitor_hours_delivery'] - $data['more_than_x_hours']) / 24 * $data['customer_pays_x_euros'] : 0;
            $val2 = (($data['competitor_price'] < $data['competitor_min_order_amount'])) ? ($data['competitor_min_order_amount'] - $data['competitor_price']) / $data['divide_by_x_specify_x'] : 0;

            $exp = $data['competitor_price'] * (float)$data['coefficient_price'];
            if (is_numeric($exp) && $exp > 0) {
                $result['changed_competitor_price'] = bcdiv(($data['competitor_price'] * (float)$data['coefficient_price'] + $val1 + $val2), 1, 2);
            } else {
                $result['error'] = '(Ошибка умножения: competitor_price: '.$data['competitor_price'].', coefficient_price: '.$data['coefficient_price'].')';
            }
        } else {
            $result['error'] = '(Ошибка проверки: more_than_x_hours: '.$data['more_than_x_hours'].', customer_pays_x_euros: '.$data['customer_pays_x_euros'].', divide_by_x_specify_x: '.$data['divide_by_x_specify_x'].')';
        }
        
        return $result;
    }
    
    public static function getLocalNameUrl($localSku, $barCode, $piguProductCount, $productId, $localName)
    {
        $barCodeData = json_decode($barCode);
        $barCode = (!empty($barCodeData)) ? implode("," ,$barCodeData) : '';
    
        $productLink = '<a title="SKU: '.$localSku.'|'.$barCode.'|На складе: '.$piguProductCount.'" href="/admin/products/links/product/' . $productId . '">' .$localName . '</a>';
        
        return $productLink;
    }
    
    public static function setFilter($query, $keyword){
        $query->whereRaw("product_links.local_sku LIKE ?", ["{$keyword}"]);
        $query->orWhereRaw("product_links.local_name LIKE ?", ["%{$keyword}%"]);
        
        if(mb_strlen($keyword) > 10)
            $query->orWhereRaw("product_links.pigu_barcode LIKE ?", ["%{$keyword}%"]);
        
    }
    
    public static function floorDec($number, $precision = 2, $separator = '.')
    {
        $numberPart = explode($separator, $number);
        $numberPart[1] = substr_replace($numberPart[1], $separator, $precision, 0);
        if ($numberPart[0] >= 0) {
            $numberPart[1] = substr(floor('1' . $numberPart[1]), 1);
        } else {
            $numberPart[1] = substr(ceil('1' . $numberPart[1]), 1);
        }
        $ceilNumber = array($numberPart[0], $numberPart[1]);
        return implode($separator, $ceilNumber);
    }
}