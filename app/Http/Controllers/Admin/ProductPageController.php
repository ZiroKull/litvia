<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\ProductLinks;
use App\Validators\ProductPage\UpdateProductPageRequest;

class ProductPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tooltipsJs = \Lang::get('products_tooltips');
        $product = ProductLinks::leftJoin('products', 'product_links.product_id', '=', 'products.id')
            ->select('product_links.*', 'products.product_name as product_name')->where('product_links.id', $id)->first();

        $piguBarcode = json_decode($product->pigu_barcode, TRUE);
        $product->pigu_barcode = collect($piguBarcode)->implode(', ');

        if (empty($product->id)) {
            abort(404);
        }

        return view('admin/product_page/index', compact('product', 'tooltipsJs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductPageRequest $request, $id)
    {
        $hsCodes = ProductLinks::find($id);
        $hsCodes->local_name = $request->input('local_name');
        $hsCodes->save();

        $request->session()->flash('message', 'Данные сохранены!');

        return redirect()->back();
    }
}
