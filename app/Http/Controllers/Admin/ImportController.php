<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ImportPigu;
use App\Jobs\UpdateCompetitors;
use App\Jobs\XmlGenerate;
use App\Models\Admin\Settings;
use App\Models\Admin\ParserProducts;
use Illuminate\Http\Request;
use App\Jobs\ImportB1;
use App\Models\Admin\Import;
use App\Models\JobBatches;
use App\Exports\ParserProductsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;


class ImportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index()
    {
        $tooltipsJs = \Lang::get('import_tooltips');

        $b1ImportTime = Import::where('id', 1)->first();
        $piguImportTime = Import::where('id', 2)->first();
        $updateCompetitors = Import::where('id', 3)->first();
        $generateXml = Import::where('id', 4)->first();

        $setPriceToLimit = Settings::where('key', 'set_price_to_limit')->first();
        //var_dump($setPriceToLimit->value); die;

        return view('admin/import/import', ['b1ImportTime' => $b1ImportTime,
            'piguImportTime' => $piguImportTime,
            'updateCompetitors' => $updateCompetitors,
            'generateXml' => $generateXml,
            'setPriceToLimit' => $setPriceToLimit,
            'tooltipsJs' => $tooltipsJs]);
    }

    public function importB1(Request $request)
    {
        //ImportB1::dispatch();
        $isProcessed = JobBatches::getProcessedBatches('Import B1');

        if(!$isProcessed) {
            Bus::batch([
                new ImportB1
            ])->then(function (Batch $batch) {

            })->catch(function (Batch $batch, Throwable $e) {
                //var_dump($e->getMessage()); die;
            })->name('Import B1')->dispatch();

            $request->session()->flash('message', 'Импорт из B1 успешно завершен!');

        } else {
            $request->session()->flash('error', 'Импорт из B1 уже запущен!');
        }



        return redirect('admin/import');
    }

    public function importPigu(Request $request)
    {
        $isProcessed = JobBatches::getProcessedBatches('Import Pigu');

        if(!$isProcessed) {
            Bus::batch([
                new ImportPigu
            ])->then(function (Batch $batch) {

            })->catch(function (Batch $batch, Throwable $e) {
                //var_dump($e->getMessage()); die;
            })->name('Import Pigu')->dispatch();

            $request->session()->flash('message', 'Импорт из Pigu успешно завершен!');
        } else {
            $request->session()->flash('error', 'Импорт из Pigu уже запущен!');
        }



        return redirect('admin/import');
    }

    public function importCompetitorsPrices(Request $request)
    {
        $isProcessed = JobBatches::getProcessedBatches('Update Competitors');

        //UpdateCompetitors::dispatch();

        if(!$isProcessed) {
            Bus::batch([
                new UpdateCompetitors
            ])->then(function (Batch $batch) {

            })->catch(function (Batch $batch, Throwable $e) {
                //var_dump($e->getMessage()); die;
            })->name('Update Competitors')->dispatch();

            $request->session()->flash('message', 'Цены конкурентов обновлены!');

        } else {
            $request->session()->flash('error', 'Цены в процессе обновления!');
        }



        return redirect('admin/import');

    }

    public function generateXml(Request $request)
    {
        $isProcessed = JobBatches::getProcessedBatches('Xml Generate');

        if(!$isProcessed) {
            Bus::batch([
                new XmlGenerate
            ])->then(function (Batch $batch) {

            })->catch(function (Batch $batch, Throwable $e) {
                //var_dump($e->getMessage()); die;
            })->name('Xml Generate')->dispatch();

            $request->session()->flash('message', 'Генерируется XML файл!');
        } else {
            $request->session()->flash('error', 'Процесс генерации уже запущен!');
        }

        return redirect('admin/import');

    }

    public function generateDownloadXls(Request $request)
    {
        ini_set('memory_limit', '-1');
        return (new ParserProductsExport)->download('parser_products.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function clearParserTable(Request $request)
    {
        ParserProducts::truncate();

        $request->session()->flash('message', 'Таблица парсера очищена!');

        return redirect('admin/import');
    }
}
