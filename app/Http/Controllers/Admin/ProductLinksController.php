<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductLinksTable;
use App\DataTables\SupplierProductLinksTable;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use App\Models\Admin\ProductLinks;
use App\Validators\ProductLinks\StoreProductLinksRequest;
use Illuminate\Http\Request;

class ProductLinksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $product = Product::with('hsCode')->find($id);
        if (empty($product->id)) {
            abort(404);
        }

        return view('admin/product_links/index', compact('product'));
    }

    public function getLinks(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return ProductLinksTable::getTable($request);
        } else {
            abort(404);
        }
    }

    public function getSupplierProductLinks(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return SupplierProductLinksTable::getTable($request);
        } else {
            abort(404);
        }
    }

    public function store(StoreProductLinksRequest $request)
    {
        $trader = new ProductLinks;
        $trader->product_id = $request->id;
        $trader->local_name = $request->local_name;
        $trader->local_sku = $request->local_sku;
        $trader->save();

        $request->session()->flash('message', 'Товар успешно добавлен!');

        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        if(request()->ajax()) // This is check ajax request
        {
            ProductLinks::destroy($request->id);

            return response()->json([
                'id' => (int)$request->id,
                'status' => TRUE
            ]);
        }else {
            abort(404);
        }
    }
}
