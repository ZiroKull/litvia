<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PiguFeesTable;
use App\Http\Controllers\Controller;
use App\Models\Admin\PiguFees;
use App\Validators\PiguFees\StorePiguFeesRequest;
use App\Validators\PiguFees\UpdatePiguFeesRequest;
use Illuminate\Http\Request;

class PiguFeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tooltipsJs = \Lang::get('pigu_fees_tooltips');
        return view('admin/pigu_fees/index', compact('tooltipsJs'));
    }

    public function getPiguFees(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return PiguFeesTable::getTable();
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePiguFeesRequest $request)
    {
        $input = $request->all();

        $hsCodes = new PiguFees();
        $hsCodes->fill($input);
        $hsCodes->save();

        $request->session()->flash('message', 'Номер pigu категори успешно добавлен!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePiguFeesRequest $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            $updateData = $request->post('data');

            foreach ($updateData as $id => $item) {
                $piguFees = PiguFees::find($id);

                if(array_key_exists('category_name',$item)){
                    $piguFees->category_name = (isset($item['category_name'])) ? $item['category_name'] : null;
                }

                if(array_key_exists('pigu_fee',$item)){
                    $piguFees->pigu_fee = (isset($item['pigu_fee'])) ? (float)$item['pigu_fee'] : null;
                }

                $piguFees->save();

                return PiguFeesTable::getTable();
            }
        } else {
            abort(404);
        }
    }
}
