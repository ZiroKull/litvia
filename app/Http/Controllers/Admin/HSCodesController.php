<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\HSCodesTable;
use App\Http\Controllers\Controller;
use App\Models\Admin\HSCodes;
use App\Validators\HSCodes\StoreHSCodesRequest;
use App\Validators\HSCodes\UpdateHSCodesRequest;
use Illuminate\Http\Request;

class HSCodesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tooltipsJs = \Lang::get('hs_codes_tooltips');
        return view('admin/hs_codes/index', compact('tooltipsJs'));
    }

    public function getHSCodes(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return HSCodesTable::getTable();
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHSCodesRequest $request)
    {
        $hsCodes = new HSCodes();
        $hsCodes->fill($request->all());
        $hsCodes->save();

        $request->session()->flash('message', 'HS Код успешно добавлен!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHSCodesRequest $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            $updateData = $request->post('data');

            foreach ($updateData as $id => $item) {
                $hsCodes = HSCodes::find($id);
                $hsCodes->tariff = (isset($item['tariff'])) ? (float)$item['tariff'] : null;
                $hsCodes->save();

                return HSCodesTable::getTable();
            }
        } else {
            abort(404);
        }
    }
}
