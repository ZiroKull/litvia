<?php

namespace App\Http\Controllers\Admin;

use App\Actions\PiguFeesActions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MenuWarningsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWarnings(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            $data = [
                'menu_pigu_fees' => (new PiguFeesActions())->handle(),
            ];

            return response()->json($data);
        } else {
            abort(404);
        }
    }
}
