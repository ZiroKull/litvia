<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SupplierLinksTable;
use App\DataTables\SuppliersTable;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use App\Models\Admin\Supplier;
use App\Models\Admin\SupplierLinks;
use App\Validators\Suppliers\StoreSuppliersRequest;
use App\Validators\Suppliers\UpdateSuppliersRequest;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tooltipsJs = \Lang::get('suppliers_tooltips');
        return view('admin/suppliers/index', compact('tooltipsJs'));
    }

    public function getSuppliers(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return SuppliersTable::getTable();
        } else {
            abort(404);
        }
    }

    public function getSupplierLinks(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return SupplierLinksTable::getTable($request);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tooltipsJs = \Lang::get('suppliers_tooltips');
        return view('admin/suppliers/create', ['tooltipsJs' => $tooltipsJs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSuppliersRequest $request)
    {
        $supplier = new Supplier();
        $supplier->local_company_name = $request->local_company_name;
        $supplier->save();

        $request->session()->flash('message', 'Поставщик успешно добавлен!');

        return redirect()->route('suppliers.edit', $supplier->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->touch();

        $tooltipsJs = \Lang::get('suppliers_tooltips');

        return view('admin/suppliers/edit', compact('supplier', 'tooltipsJs'));
    }

    public function open(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            $data = $request->all();

            if(!empty($data['id'])) {
                $supplier = Supplier::findOrFail($data['id']);
                $supplier->touch();
            }

            return response()->json(array('status' => 'ok'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSuppliersRequest $request, $id)
    {
        $input = $request->all();
        $supplier = Supplier::findOrFail($id);
        $supplier->fill($input);
        $supplier->export_license = null !== $request->input('export_license') ? $request->input('export_license')[0] : null;
        $supplier->save();

        $request->session()->flash('message', 'Данные успешно обновлены!');

        return redirect()->route('suppliers.edit', $supplier->id)->with('supplier', $supplier);
    }

    public function searchSmallestGroup(Request $request)
    {
        if (request()->ajax()) // This is check ajax request
        {
            $query = $request->q;

            $outputData = array();
            $productQuery = Product::where('product_name', 'like', '%' . $query . '%')
                ->orderBy('product_name','asc')
                ->get();

            foreach ($productQuery as $key => $product) {
                $outputData[$key]['id'] = $product->id;
                $outputData[$key]['title'] = $product->product_name;
                $outputData[$key]['value'] = $product->product_name;
            }

            return response()->json($outputData);

        } else {
            abort(404);
        }
    }

    public function addSmallestGroup(Request $request) {
        if (request()->ajax()) // This is check ajax request
        {
            $input = $request->all();

            $alreadyExistSameProduct = SupplierLinks::where('supplier_id', $input['supplier_id'])
                ->where('product_id', $input['product_id'])
                ->first();

            if (null !== $alreadyExistSameProduct) {
                return response()->json([
                    'status' => false
                ]);
            }

            $existSupplierQuality = SupplierLinks::where('supplier_id', $input['supplier_id'])->first();

            $supplierLinks = new SupplierLinks();
            $supplierLinks->fill($input);
            $supplierLinks->supplier_quality = null !== $existSupplierQuality ? 20 : 30;
            $supplierLinks->save();

            return response()->json([
                'status' => true
            ]);

        } else {
            abort(404);
        }
    }
}
