<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\BeforePurchaseTable;
use App\Models\Admin\AvailabilityGoodsStock;


class BeforePurchaseController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $tooltipsJs = \Lang::get('before_purchase_tooltips');
        
        return view('admin/before_purchase/before_purchase',  ['tooltipsJs' => $tooltipsJs]);
    }
    
    public function getPurchase(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            //return response()->json(array('data' => array(), 'recordsTotal' => 0, 'recordsFiltered' => 0, 'draw' => 0));
            return BeforePurchaseTable::getTable();
        } else {
            abort(404);
        }
    }
    
    public function getHistoryBySku($sku, Request $request){
        if ($request->ajax()) // This is check ajax request
        {
            $data = AvailabilityGoodsStock::getAvailabilityStockDatesBySku($sku);
            return response()->json($data);
        } else {
            abort(404);
        }
    }
}
