<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PriceAndSell;
use App\Models\Admin\ProductLinks;
use App\Models\Admin\ProductPrices;
use App\Validators\PriceAndSell\UpdatePriceAndSellRequest;
use Illuminate\Http\Request;
use App\Models\Admin\Settings;
use Validator;
use App\DataTables\PriceAndSellTable;

class PriceAndSellController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sellPriceVal =  Settings::where('key', 'sell_and_price_x_val')->first();
        $tooltipsJs = \Lang::get('price_and_sell_tooltips');
        return view('admin/price_and_sell/price_and_sell', ['sellPriceVal' => $sellPriceVal, 'tooltipsJs' => $tooltipsJs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\PriceAndSell  $priceAndSell
     * @return \Illuminate\Http\Response
     */
    public function show(PriceAndSell $priceAndSell)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\PriceAndSell  $priceAndSell
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceAndSell $priceAndSell)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\PriceAndSell  $priceAndSell
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePriceAndSellRequest $request, PriceAndSell $priceAndSell)
    {
        //
        if ($request->ajax()) // This is check ajax request
        {
            $updateData = $request->post('data');

            // update table
            foreach ($updateData as $id => $item) {
                $productPrice = ProductPrices::find($id);

                if(array_key_exists('minimal_price', $item)){

                    if(((float)$item['minimal_price'] > $productPrice->minimal_price) && ((float)$item['minimal_price'] > $productPrice->selling_price)){
                        ProductPrices::where('id', $productPrice->id)->update(['selling_price' => null]);
                    }
                    $productPrice->minimal_price = (isset($item['minimal_price'])) ? (float)$item['minimal_price'] : null;
                }

                if(array_key_exists('target_price',$item)){
                    $productPrice->target_price = (isset($item['target_price'])) ? (float)$item['target_price'] : null;
                }

                if(array_key_exists('price_without_discount',$item)){
                    $productPrice->price_without_discount = (isset($item['price_without_discount'])) ? (float)$item['price_without_discount'] : null;
                }

                if(array_key_exists('selling_price',$item)){
                    $productPrice->selling_price = (isset($item['selling_price'])) ? (float)$item['selling_price'] : null;
                }

                $productPrice->save();

                if(array_key_exists('is_xml', $item)){
                    ProductLinks::where('id', $productPrice->product_link_id)->update(['is_xml' => ($item['is_xml']) ? 1 : 0]);
                }

                if(array_key_exists('delivery_date', $item)){
                    ProductLinks::where('id', $productPrice->product_link_id)->update(['delivery_date' => ($item['delivery_date']) ? $item['delivery_date'] : null]);
                }

                if(array_key_exists('xml_count', $item)){
                    ProductLinks::where('id', $productPrice->product_link_id)->update(['xml_count' => ($item['xml_count']) ? $item['xml_count'] : 0]);
                }

                if(array_key_exists('less_8_euro', $item)){
                    ProductLinks::where('id', $productPrice->product_link_id)->update(['less_8_euro' => ($item['less_8_euro']) ? $item['less_8_euro'] : 0]);
                }

                if(array_key_exists('xml_located', $item)){
                    ProductLinks::where('id', $productPrice->product_link_id)->update(['xml_located' => ($item['xml_located']) ? $item['xml_located'] : 0]);
                }

                // get product
                return PriceAndSellTable::getTable();
            }


        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\PriceAndSell  $priceAndSell
     * @return \Illuminate\Http\Response
     */
    public function destroy(PriceAndSell $priceAndSell)
    {
        //
    }

    public function getPriceAndSell(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            //return response()->json(array('data' => array(), 'recordsTotal' => 0, 'recordsFiltered' => 0, 'draw' => 0));
            return PriceAndSellTable::getTable();
        } else {
            abort(404);
        }
    }
}
