<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Competitors;
use App\Models\Admin\ProductLinks;
use App\Validators\Competitors\StoreCompetitorRequest;
use App\Validators\Competitors\UpdateCompetitorRequest;
use Illuminate\Http\Request;
use App\Services\PiguServices;
use App\DataTables\CompetitorsTable;


class CompetitorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tooltipsJs = \Lang::get('competitors_tooltips');
        return view('admin/competitors/competitors', ['tooltipsJs' => $tooltipsJs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompetitorRequest $request, PiguServices $piguServices)
    {
        //parse url create service
        $url = $request->url;

        $piguData = array();

        if(!empty($url)) {
           $responseData = $piguServices->getResponse($url);
           $piguData = $piguServices->getProductData($responseData, $request->all());
        }

        $productLink = ProductLinks::where('local_sku', 'LIKE', $request->local_name)->orWhere('local_name', 'LIKE', $request->local_name)->first();

        //save to table
        if(!empty($piguData)) {
            if (empty($piguData['errors'])) {
                $competitor = new Competitors;
                $competitor->product_link_id = (!empty($productLink->id)) ? $productLink->id : NULL;
                $competitor->coefficient_price = (!empty($request->coefficient_price)) ? $request->coefficient_price : 0;
                $competitor->url = (!empty($url)) ? $url : '';
                $competitor->competitor_name = (!empty($piguData['competitor_name'])) ? $piguData['competitor_name'] : NULL;
                $competitor->competitor_price = (!empty($piguData['competitor_price'])) ? $piguData['competitor_price'] : NULL;
                $competitor->competitor_hours_delivery = (isset($piguData['competitor_hours_delivery'])) ? $piguData['competitor_hours_delivery'] : 2400;
                $competitor->competitor_min_order_amount = (!empty($piguData['competitor_min_order_amount'])) ? $piguData['competitor_min_order_amount'] : 0;
                $competitor->changed_competitor_price = (isset($piguData['changed_competitor_price'])) ? $piguData['changed_competitor_price'] : NULL;

                $urlExp = explode(".", parse_url($url, PHP_URL_HOST));
                $type = strtoupper(end($urlExp));
                $competitor->type = (!empty($type)) ? $type : 'LT';

                $competitor->save();
            } else {
                $request->session()->flash('error', $piguData['errors']);
                return redirect('admin/competitors');
            }

        } else {
            $request->session()->flash('error', __('admin_mails.mail_pigu_server_error_text'));
            return redirect('admin/competitors');
        }

        $request->session()->put('local_name', $request->local_name);
        $request->session()->put('coefficient_price', $request->coefficient_price);
        $request->session()->flash('message', 'Товар конкурента добавлен!');

        return redirect('admin/competitors');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Competitors  $compretitors
     * @return \Illuminate\Http\Response
     */
    public function show(Competitors $compretitors)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Competitors  $compretitors
     * @return \Illuminate\Http\Response
     */
    public function edit(Competitors $compretitors)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Competitors  $compretitors
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompetitorRequest $request, Competitors $competitors)
    {
        //
        if ($request->ajax()) // This is check ajax request
        {
            $updateData = $request->post('data');

            foreach ($updateData as $id => $item) {
                $competitor = Competitors::find($id);

                if(array_key_exists('coefficient_price',$item)){
                    $competitor->coefficient_price = (isset($item['coefficient_price'])) ? (float)$item['coefficient_price'] : null;
                }

                $competitor->save();

                return CompetitorsTable::getTable();

            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Competitors  $compretitors
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Competitors $compretitors)
    {
        //
        if(request()->ajax()) // This is check ajax request
        {
            Competitors::destroy((int)$request->id);
            return response()->json([
                'id' => (int)$request->id,
                'status' => true
            ]);
        }else {
            abort(404);
        }
    }

    public function getCompetitors(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return CompetitorsTable::getTable();
            /*return response()->json(array('data' => array(), 'recordsTotal' => 0, 'recordsFiltered' => 0, 'draw' => 0));*/
        } else {
            abort(404);
        }
    }

    public function checkUniqueUrl(Request $request){
        if ($request->ajax()) // This is check ajax request
        {
            $parts  = parse_url($request->url);
            parse_str($parts['query'], $query);
            $urlExp = explode(".", $parts['host']);
            $type = strtoupper(end($urlExp));

            $count = Competitors::leftJoin('product_links', 'product_links.id', '=', 'competitors.product_link_id')
                ->where('product_links.local_name', $request->local_name)
                ->where('url', 'like' , '%id='.$query['id'].'%')
                ->where('type' , $type)
                ->count();
            return response()->json(array('count' => $count));
        } else {
            abort(404);
        }
    }
}
