<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductsTable;
use App\Http\Controllers\Controller;
use App\Models\Admin\HSCodes;
use App\Models\Admin\Product;
use App\Models\Admin\ProductLinks;
use App\Validators\Products\UpdateProductsRequest;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tooltipsJs = \Lang::get('products_tooltips');
        return view('admin/products/products', ['tooltipsJs' => $tooltipsJs]);
    }

    public function getProducts(Request $request)
    {
        if ($request->ajax()) // This is check ajax request
        {
            return ProductsTable::getTable();
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductsRequest $request, $id)
    {
        $product = Product::find($id);
        $product->rohs = null !== $request->input('rohs') ? $request->input('rohs')[0] : null;
        $product->ce   = null !== $request->input('ce') ? $request->input('ce')[0] : null;
        $product->forwarder_description = null !== $request->input('forwarder_description') ? $request->input('forwarder_description') : null;
        $product->weight_g   = null !== $request->input('weight_g') ? $request->input('weight_g') : null;
        $product->lenght_cm   = null !== $request->input('lenght_cm') ? $request->input('lenght_cm') : null;
        $product->height_cm   = null !== $request->input('height_cm') ? $request->input('height_cm') : null;
        $product->width_cm   = null !== $request->input('width_cm') ? $request->input('width_cm') : null;
        $product->hs_code_id = null;

        if (null !== $request->input('hs_code')) {
            $hsCode = HSCodes::firstOrNew([
                'hs_code' => $request->input('hs_code')
            ]);
            $hsCode->save();

            $product->hs_code_id = $hsCode->id;
        }

        $product->save();

        $request->session()->flash('message', 'Данные сохранены!');

        return redirect()->back();
    }

    public function searchProductLink(Request $request)
    {
        if (request()->ajax()) // This is check ajax request
        {
            $query = $request->q;

            $outputData = array();
            $productLinkDataQuery = ProductLinks::where('local_name', 'like', '%' . $query . '%');
            $productLinkDataQuery->orWhere('local_sku', 'like',  $query);
            if(mb_strlen($query) > 10)
                $productLinkDataQuery->orWhere('pigu_barcode', 'like',  '%' . $query . '%');
            $productLinkDataQuery->orderBy('local_name','asc');
            $productLinkData = $productLinkDataQuery->get();

            foreach ($productLinkData as $key => $product) {
                $outputData[$key]['id'] = $product->id;
                $outputData[$key]['title'] = $product->local_name;
                $outputData[$key]['value'] = $product->local_name;
            }

            return response()->json($outputData);

        } else {
            abort(404);
        }
    }

    public function getProductPricesById(Request $request)
    {
        if (request()->ajax()) // This is check ajax request
        {
            $type = $request->type;

            $productName =  $request->local_name;
            //$product_link_id = $request->product_link_id;
            //$product_link_id = ProductLinks::where('local_name', $productName)->first();

            $productLinkData = ProductLinks::getProductLinksWithPrices(NULL, array('type' => $type, 'local_name' => $productName))->first();
            //var_dump($productLinkData);
            $outputData = array();

            if($productLinkData) {
                $outputData['minimal_price'] = number_format($productLinkData->minimal_price, 2);
                $outputData['target_price'] = number_format($productLinkData->target_price, 2);
                $outputData['selling_price'] = number_format(($productLinkData->selling_price) ? $productLinkData->selling_price : $productLinkData->formula_set_price, 2);
            }


            return response()->json($outputData);

        } else {
            abort(404);
        }
    }

}
