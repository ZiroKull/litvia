<?php

return [

    'main_navigation'               => 'ГЛАВНОЕ МЕНЮ',
    'blog'                          => 'Блог',
    'pages'                         => 'Страницы',
    'account_settings'              => 'НАСТРОЙКИ ПРОФИЛЯ',
    'profile'                       => 'Профиль',
    'change_password'               => 'Изменить пароль',
    'multilevel'                    => 'Многоуровневое меню',
    'level_one'                     => 'Уровень 1',
    'level_two'                     => 'Уровень 2',
    'level_three'                   => 'Уровень 3',
    'labels'                        => 'Метки',
    'important'                     => 'Важно',
    'warning'                       => 'Внимание',
    'information'                   => 'Информация',
    'search_placeholder'            => 'Поиск...',
    // menu
    'update_b1_and_pigu'            => 'Обновление B1 и pigu',
    'database'                      => 'База данных',
    'sale_price_and_date'           => 'Цена продажи и дата',
    'competitors'                   => 'Конкуренты',
    'suppliers'                     => 'Поставщики',
    'hs_codes'                      => 'HS Коды',
    'settings'                      => 'Настройки',
    'general_settings'              => 'Общие настройки',
    'translations'                  => 'Переводы',
    'before_purchase'               => 'Перед закупкой',
    'pigu_fees'                     => 'Комиссия пигу'
];
