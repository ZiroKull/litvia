<?php
return [
    'product_group_title' => 'Группа товаров',
    'product_group' => 'База данных (группа товаров)',
    'product_group_name' => 'Название мельчайшей группы:',
    'product_group_block_products' => 'Продукты, входящие в данную мельчайшую группу',
    'new_product_name' => 'Название нового продукта',
    'product_group_barcode_pigu' => 'баркод из pigu',
    'product_group_pigu_count' => 'остаток на pigu складе',
];
