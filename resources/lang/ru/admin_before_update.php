<?php

return array (
  'beforePurchase' => 'Перед закупкой',
  'local_product_name' => 'Локальное название продукта',
  'local_vendor_name' => 'Локальное название поставщика',
  'large_group' => 'Большая группа',
  'smallest_group' => 'Мельчайшая группа',
  'balance_pigu_stock' => 'Остаток в лигу складе',
  'average_calculation_started_from' => 'Расчёт средних начался с',
  'enough_x_days' => 'Хватит на X дней',
  'average_sales_per_day' => 'Средние продажи в день',
);
