<?php

return array (
  'import_update' => 'Автоматически скачивает информацию из В1 в 3 часа ночи.  Если хотим продавать новую мельчайшую группу, сперва она должна быть добавлена в В1 а чтобы она появилась в этой проге нужно "Обновить В1"',
  'import_date_time_pigu' => 'Помимо остатков на складе скачивает баркоды новых товаров. Запускается автоматически в 4часа ночи. Если при скачивании возникают какие либо ошибки рекомендуется скачать сначала данные из В1 ручным способом.',
  'import_competitor_price_updates' => 'Автоматически запускается в 7 утра. Настроен так, чобы цены всех конкурентов обновлял за 18часов (если конкурентов становится больше то переходит по ссылка быстрее и всёравно обновляет за 18часов +-час)',
  'import_update_sale_price_in_pigu' => '<p>Нажав <span style="color: red;">эту кнопку</span> обновляется XML файл по ссылке <a href="https://svajokliunamai.eu/admin/import/downloadXml" target="_blank">https://svajokliunamai.eu/admin/import/downloadXml</a> Его сканирует PIGU <br> и таким образом в PIGU обновляется цена товара, время доставки.</p>',
  'import_competitor_sales_statistics' => 'Прога ежедневно скачивает остатки товаров у конкурентов, при помощи них можно вычислить сколько конкуренты продают товаров. Кнопка "очистить базу" стирает всю собранную статистику.',
  'import_set_price_to_limit' => 'Если прога неправильно установила цены товарам в pigu нажмите галочку а затем кнопку с лева "обносить сейчас". Цены всем (и даже тем, у которых стоит галочка "не добавлять в XML") товарам в Pigu установятся 9999 (по факту обновляется XML файл для PIGU). - Это даст время решить проблему. Потом просто убрать галочку и обновить еще раз.',
);
