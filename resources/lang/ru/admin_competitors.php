<?php
return [
    'competitors' => 'Конкуренты',
    'loc_sku_name' => 'Локальное название или SKU продукта',
    'competitor_url' => 'Ссылка конкурента',
    'competitor_url_placeholder' => 'Ссылка',
    'price_koef' => 'Коэфицент цены',
    'country' => 'Cтрана',
    'koef' => 'Коэффециент',
    'competitor_name' => 'Название конкурента',
    'competitor_price' => 'Цена конкурента',
    'competitor_hours_delivery' => 'Часы доставки конкурента',
    'competitor_min_order_amount' => 'Минимальная сумма заказа',
];