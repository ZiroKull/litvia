<?php

return array (
  'products_links_product_group_name' => 'Это название мельчайшей группы которую вы сейчас просматриваете. На этой странице можно добавлять новые товары в эту мельчайшую группу.',
  'products_links_product_group_block_products' => 'Продукты, входящие в данную мельчайшую группу',
  'products_links_local_sku' => 'Это же supplier code в pigu. Необходим чтобы связывать товар на сайте pigu c товаром в Системе.',
  'products_links_local_name' => 'Это название должно быть на английском т.к. его видит поставщик чтобы понять какой это товар. Экспедитор его не видит.',
  'products_links_product_group_barcode_pigu' => 'Баркод указанный на сайте pigu (его программа скачивает с их сайта автоматически)',
  'products_links_product_group_pigu_count' => 'Количество на pigu FBP складе. Это количество берётся с сайта pigu обновляется каждый день.',
  'products_links_new_product_name' => 'Локальное название товара обязательно должно быть уникальным и написано на английском языке.Тут точно должны быть описаны основные характеристи продукта. Поставщик видит именно это название когда мы заказываем у него товар. Поэтому изходя из названия он должен понять какой товар необходим от него.  Экспедитор его не видит.',
  'products_links_rohs' => 'Rohs докумет',
  'products_links_ce' => 'CE докумет',
  'products_links_forwarder_description' => 'Литовское описание экспедитору',
  'products_links_hs_code' => 'HS Код',
  'products_links_weight_g' => 'Вес в граммах',
  'products_links_lenght_cm' => 'Длина (см)',
  'products_links_height_cm' => 'Высота (см)',
  'products_links_width_cm' => 'Ширина (см)',
  'products_links_size' => 'Размеры (Длина (см) Х Высота (см) Х Ширина (см))',
);
