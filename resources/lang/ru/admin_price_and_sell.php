<?php

return array (
  'price_and_sell' => 'Цена продажи и дата доставки',
  'local_name_p' => 'Локальное название продукта',
  'minimal_price' => 'Мин. цена',
  'target_price' => 'Целевая цена',
  'price_without_discount' => 'Цена без скидки',
  'selling_price' => 'Текущая цена продажи',
  'xml' => 'Добавлять в XML',
  'delivery_date' => 'Дата доставкиТЕСТ',
  'xml_count' => 'Количество для XML',
  'less_8_euro' => 'Одна из цен меньше :sellPriceVal евро',
  'xml_located' => 'В XML добавилось',
  'warning' => 'Внимание',
  'competitors_count' => 'Количество конкурентов',
);
