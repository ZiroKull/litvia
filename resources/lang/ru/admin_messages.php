<?php
return [
    'warning_minimal_sell_price' => 'Минимальная цена любого продукта должна в ХМЛ быть больше чем это указано в настройках',
    'warning_minimal_less_8_euro' => 'Цена в ХМЛ меньше чем :sellPriceVal евро, т.к. галочка не стоит предположительно тут ошибка',
    'less_calculated_price' => 'Цена без скидки меньше чем подсчитанная цена для XML',
    'warning_values_null' => 'Одно из значений XML - null',
    'warning_price_less_minimum_selling' => 'Посчитаная/указаная цена продажи данного товара меньше чем минимальная цена продажи для этого товара.',
    'warning_is_not_xml' => 'Продукт в XML не добавляется т.к. не проставлена галочка "Добавлять в XML"',
    'warning_greater_than_target_price' => 'Посчитаная/указаная цена продажи данного товара больше чем целевая цена',
    'warning_db_minimum_price_target_price' => 'Минимальная цена или целевая цена не заполнены или формула не смогла расчитать цену продажи',
    'warning_lower_price_minimum_price' => 'В одном из столбцов (target_price / price_without_discount / formula_set_price) указана более низкая цена чем в столбце "минимальная цена"',
    'warning_formula_set_price_greater_target_price' => 'Рассчитанная по формуле цена formula_set_price больше чем целевая цена, а так быть не должно',
    'warning_one_prices_is_less_x' => 'Одна из цен меньше чем :sellPriceVal евро но галочка об этом не стоит',
    'warning_minimum_price_settings' => 'Одна из цен меньше чем минимально разрешенная в настройках цена (:minimalProductPrice евро)',
    'warning_selling_price_high' => 'Расчитанная формулой цена продажи слишком большая',
    'warning_target_price_greater_undiscounted_price' => 'Целевая цена больше, чем цена без скидок, так что не должно быть',
];