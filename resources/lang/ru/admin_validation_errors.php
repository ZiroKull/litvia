<?php
return [
    'info' => 'Информация',
    'error' => 'Ошибка',
    'data_be_a_number' => 'Данные должны быть числом',
    'number_be_gt_0' => 'Число должно быть больше 0',
    'number_be_gte_0' => 'Число должно быть больше или равно 0',
    'enter_valid_email_format' => 'Введите верный формат email',
    'div_by_24' => 'Введите число кратное 24',
    'only_integer_numbers' => 'Только целые числа',
];