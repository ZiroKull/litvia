<?php
return [
    'update_b1_and_pigu' => 'Обновление B1 и pigu', // Заголовок https://www.screencast.com/t/ljq4uJ5Qj1
    'update_now' => 'Обновить сейчас', // Кнопка Обновить сейчас (текст)
    'date_time_download_from_pigu' => 'Дата и время скачивания  из pigu', // Заголовок Дата и время скачивания  из pigu
    'competitor_price_updates' => 'Обновление цен конкурентов', // Заголовок Обновление цен конкурентов
    'update_sale_price_in_pigu' => 'Обновить цену продажи в pigu (обновить XML)', // Заголовок Обновить цену продажи в pigu (обновить XML)
    'download_xml' => 'Скачать XML',
    'competitor_sales_statistics' => 'Статистика продаж конкурентов',
    'download_excel_button' => 'Скачать эксель',
    'clear_database_button' => 'Очистить базу данных',
];