<?php

return array (
  'mail_xml_subject' => 'Возникла ошибка при генерации XML',
  'mail_xml_subject_no_xml' => 'Не проставлена галочка "Добавлять в XML"',
  'mail_db_subject_error' => 'Ошибка в БД',
  'mail_xml_from' => 'svajokliunamai',
  'mail_xml_text' => 'Возникла ошибки при генерации следующих товаров',
  'mail_update_competitor_price_error_submit' => 'ChangedCompetitorPrice не обновился',
  'mail_update_competitor_price_error' => 'ChangedCompetitorPrice не обновился изза ошибки формулы, необходимо выяснить в чём проблема" при первой найденной такой ошибке. Необходимо проверить логи сервера для устранения ошибки. Это критическая ошибка (ID :competitorID)',
  'mail_pigu_server_error' => 'Pigu сервер лёг',
  'mail_pigu_server_error_text' => 'Комтетитор не обновился тк pigu сервер лёг.',
  'mail_pigu_server_error_text_comp_id' => 'ID компетитора: :competitorID',
  'mail_pigu_server_error_text_comp_link' => 'Cсылка на товар: :competitorLink',
  'mail_pigu_xml_comp_err_subject' => 'ChangedCompetitorPrice не обновился при генерации XML',
  'mail_pigu_xml_comp_err_error' => 'ChangedCompetitorPrice не обновился изза ошибки формулы ПРИ ГЕНЕРАЦИИ XML, необходимо выяснить в чём проблема. Необходимо проверить логи сервера для устранения ошибки. Это критическая ошибка',
  'mail_backup_error_subject' => 'Ошибка при создании бэкапа',
);
