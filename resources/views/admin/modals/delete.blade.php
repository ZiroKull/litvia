<div class="modal modal-danger fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Удалить?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Вы действительно хотите удалить?
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-outline-primary" data-dismiss="modal" value='Отмена'/>
                <button type="button" id="delete-product-link"  class="btn btn-outline-danger" data-dismiss="modal">Удалить</button>
            </div>
        </div>
    </div>
</div>