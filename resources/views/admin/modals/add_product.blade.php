<div class="modal fade" id="add-smollest-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Добавить мельчайшую группу?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        {!! Form::label('name', __('admin_suppliers.products')) !!}
                        <i class="far fa-fw fa-question-circle notification-con d-inline" id="suppliers_noti_products_modal"></i>
                        <div class="name-container">
                            {!! Form::text('local_name', null, ['class' => 'form-control', 'placeholder'=>  __('admin_suppliers.products'), 'id'=>'smallest-group', 'autocomplete' => 'off']) !!}
                            <button type="button" class="btn bg-transparent field-button">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" id="save-smallest-group" class="btn btn-outline-primary" data-dismiss="modal">Добавить</button>
            </div>
        </div>
    </div>
</div>
