@extends('adminlte::page')

@section('title', __('admin_products_links.product_group_title'))

@section('content_header')
    <h1>{{__('admin_products_links.product_group')}}</h1>
@stop

@section('content')
    {!! Form::hidden('product_id', $product->id, ['class' => 'product_id']) !!}
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="callout callout-danger">
                    <h4>Возникли ошибки</h4>
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('message'))
                <div class="col-md-12 p-0 mt-2">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <h3 class="card-title"><b>{{__('admin_products_links.product_group_name')}} {{$product->product_name}}</b> <i class="far fa-fw fa-question-circle notification-con" style="display: inline-block; padding: 0;" id="products_links_product_group_name"></i></h3>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'patch']) !!}
                    <div class="form-group row ">
                        {!! Form::label('rohs', __('admin_products.rohs'), ['class' => 'col-4 col-md-1 control-label']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="products_links_rohs"></i>
                        <div class="col-12 col-md-4">
                            <span class="info">{!! $product->rohs === 1 ? 'Нужен' : ($product->rohs === 0 ? 'Не нужен' : '') !!}</span>
                            <div class="edit-wrap" style="display: none;">
                                <label>
                                    {!! Form::checkbox('rohs[]', 1,  $product->rohs === 1) !!} Нужен
                                </label>
                                <br/>
                                <label>
                                    {!! Form::checkbox('rohs[]', 0,  $product->rohs === 0) !!} Не нужен
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('ce', __('admin_products.ce'), ['class' => 'col-4 col-md-1 control-label']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="products_links_ce"></i>
                        <div class="col-12 col-md-4">
                            <span class="info">{!! $product->ce === 1 ? 'Нужен' : ($product->ce === 0 ? 'Не нужен' : '') !!}</span>
                            <div class="edit-wrap" style="display: none;">
                                <label>
                                    {!! Form::checkbox('ce[]', 1,  $product->ce === 1) !!} Нужен
                                </label>
                                <br/>
                                <label>
                                    {!! Form::checkbox('ce[]', 0,  $product->ce === 0) !!} Не нужен
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3 align-items-end">
                        <div class="col-12 col-md-3">
                            {!! Form::label('forwarder_description', __('admin_products.forwarder_description'), ['class' => 'control-label']) !!}
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="products_links_forwarder_description"></i>
                            <div class="info">
                                {!! $product->forwarder_description; !!}
                            </div>
                            <div class="edit-wrap" style="display: none;">
                                {!! Form::textarea('forwarder_description', null,  ['rows' => 3, 'class' => 'form-control','placeholder'=> __('admin_products.forwarder_description')]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        {!! Form::label('hs_code', __('admin_products.hs_code'), ['class' => 'col-4 col-md-1 control-label info']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 info" id="products_links_hs_code" style="display: inline;"></i>
                        <div class="col-12 col-md-4 info">
                            {!! $product->hsCode->hs_code ?? '' !!}
                        </div>
                        <div class="col-12 col-md-3 edit-wrap" style="display: none;">
                            {!! Form::label('hs_code', __('admin_products.hs_code'), ['class' => 'control-label']) !!}
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="products_links_hs_code"></i>
                            {!! Form::text('hs_code', $product->hsCode->hs_code ?? null,  ['class' => 'form-control int-type','placeholder'=> __('admin_products.hs_code')]) !!}
                        </div>
                    </div>
                    <div class="row mb-3">
                        {!! Form::label('weight_g', __('admin_products.weight_g'), ['class' => 'col-4 col-md-1 control-label info']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 info" id="products_links_weight_g" style="display: inline;"></i>
                        <div class="col-12 col-md-4 info">
                            {!! $product->weight_g !!}
                        </div>
                        <div class="col-12 col-md-3 edit-wrap" style="display: none;">
                            {!! Form::label('weight_g', __('admin_products.weight_g'), ['class' => 'control-label']) !!}
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="products_links_weight_g"></i>
                            {!! Form::text('weight_g', null,  ['class' => 'form-control int-type','placeholder'=> __('admin_products.weight_g')]) !!}
                        </div>
                    </div>
                    <div class="row mb-3">
                        {!! Form::label('size', __('admin_products.size'), ['class' => 'col-4 col-md-1 control-label info']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 info" id="products_links_size" style="display: inline;"></i>
                        <div class="col-12 col-md-2 edit-wrap" style="display: none;">
                            {!! Form::label('lenght_cm', __('admin_products.lenght_cm'), ['class' => 'control-label']) !!}
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="products_links_lenght_cm"></i>
                            {!! Form::text('lenght_cm', null,  ['class' => 'form-control numeric-type','placeholder'=> __('admin_products.lenght_cm')]) !!}
                        </div>
                        <div class="col-12 col-md-2 edit-wrap" style="display: none;">
                            {!! Form::label('height_cm', __('admin_products.height_cm'), ['class' => 'control-label']) !!}
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="products_links_height_cm"></i>
                            {!! Form::text('height_cm', null,  ['class' => 'form-control numeric-type','placeholder'=> __('admin_products.height_cm')]) !!}
                        </div>
                        <div class="col-12 col-md-2 edit-wrap" style="display: none;">
                            {!! Form::label('width_cm', __('admin_products.width_cm'), ['class' => 'control-label']) !!}
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="products_links_width_cm"></i>
                            {!! Form::text('width_cm', null,  ['class' => 'form-control numeric-type','placeholder'=> __('admin_products.width_cm')]) !!}
                        </div>
                        <div class="col-12 col-md-3 mt-2 mt-md-0 align-self-end">
                            <span class="info">
                                {!! (isset($product->lenght_cm) && isset($product->height_cm) && isset($product->width_cm)) ? ($product->lenght_cm . 'х' . $product->height_cm . 'х' . $product->width_cm) : '' !!}
                            </span>
                            <button class="btn btn-primary edit btn-sm ml-2" type="button">
                                <i class="fas fa-edit"></i>
                            </button>
                            <div class="submit" style="display: none;">
                                {!! Form::submit( __('admin_buttons.save'), ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    {!! Form::open(['route' => ['productLinks.store', $product->id]]) !!}
                    <div class="row">
                        <div class="col-lg-3">
                            {!! Form::text('local_name', NULL , ['class' => 'form-control', 'placeholder'=>  __('admin_products_links.new_product_name'), 'id'=>'local-name']) !!}
                            <i class="far fa-fw fa-question-circle notification-con" style="display: inline-block; padding: 0; position: absolute; top: 11px; right: 15px;" id="products_links_new_product_name"></i>
                        </div>
                        <div class="col-lg-3 mt-2 mt-md-0">
                            {!! Form::text('local_sku', NULL , ['class' => 'form-control', 'placeholder'=>  __('admin_products.local_sku'), 'id'=>'local-sku']) !!}
                        </div>
                        <div class="col-lg-6 mt-2 mt-md-0">
                            {!! Form::submit( __('admin_buttons.submit'), ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 p-0 mt-2 mb-4">
                            <button class="btn btn-primary open-add-smollest-group" data-id="{{ request()->route('id') }}">
                                {{__('admin_suppliers.add_products')}}
                            </button>
                        </div>
                        <div class="col-md-12 p-0 mt-2">
                            <table id="supplier-product-links-table" class="table table-bordered table-hover dataTable"
                                   style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="tooltipable"><span id="suppliers_noti_supplier">{{__('admin_suppliers.supplier')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_supplier_quality">{{__('admin_suppliers.supplier_quality')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_rohs">{{__('admin_suppliers.rohs')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_ce">{{__('admin_suppliers.ce')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_comments">{{__('admin_suppliers.comments')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_quantity_per_carton">{{__('admin_suppliers.quantity_per_carton')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_carton_volume_m3">{{__('admin_suppliers.carton_volume_m3')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_carton_weight_kg">{{__('admin_suppliers.carton_weight_kg')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_is_accurate">{{__('admin_suppliers.is_accurate')}}</span></th>
                                </tr>
                                <tr>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead select selectOptionsQuality"></th>
                                    <th class="filterhead"></th>
                                    <th class="filterhead"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead select selectOptions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <b>{{__('admin_products_links.product_group_block_products')}}</b> <i class="far fa-fw fa-question-circle notification-con" style="display: inline-block; padding: 0;" id="products_links_product_group_block_products"></i>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="table-container">
                            <table id="products-links-table" class="table table-bordered table-hover dataTable" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th width="35%" class="tooltipable"><span id="products_links_local_sku">{{__('admin_products.local_sku')}}</span></th>
                                    <th width="25%" class="tooltipable"><span id="products_links_local_name">{{__('admin_products.local_name')}}</span></th>
                                    <th width="20%" class="tooltipable"><span id="products_links_product_group_barcode_pigu">{{__('admin_products_links.product_group_barcode_pigu')}}</span></th>
                                    <th width="15%" class="tooltipable"><span id="products_links_product_group_pigu_count">{{__('admin_products_links.product_group_pigu_count')}}}</span></th>
                                    <th width="5%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/products/products_links.js') }}"></script>
    <script type="application/javascript" src="{{ asset('js/admin/products/supplier_product_links.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode(\Lang::get('products_links_tooltips'), JSON_HEX_TAG) !!}];

        $(document).ready(function () {
            $('input[name="rohs[]"]').on('change', function () {
                $('input[name="rohs[]"]').not(this).prop('checked', false);
            });

            $('input[name="ce[]"]').on('change', function () {
                $('input[name="ce[]"]').not(this).prop('checked', false);
            });

            $('.edit').click(function () {
                $(this).hide();
                $('.info').hide();
                $('.edit-wrap').show();
                $('.submit').show();
                $('.form-edit').attr('type', 'text');
            });
        });
    </script>
@stop
