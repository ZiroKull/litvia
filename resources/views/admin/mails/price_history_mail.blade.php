@foreach ($mailTextHistory as $history)
    <p><strong>SKU:</strong> {{$history['productData']->local_sku}}<br>
       <strong>Local product name:</strong> {{$history['productData']->local_name}}<br>
       <strong>Country</strong> {{$history['productData']->type}}
    </p>
    <p>
        <a href="{{$history['productData']->url}}" target="_blank">{{$history['productData']->url}}</a>
    </p>
    <div style="border: 1px solid #ccc; padding: 10px">
        @foreach ($history['productHistory'] as $compHistory)
            <p>{{$compHistory->updated_at}} - {{$compHistory->competitor_price}}</p>
        @endforeach
    </div>
    <hr>
@endforeach