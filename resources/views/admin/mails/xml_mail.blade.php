<p>{{__('admin_mails.mail_xml_text')}}</p>
<p>
    @foreach ($errors as $error)
        @if (!empty($error['product']))
                <b>Ошибка:</b> <br>
                {!!$error['error']!!} <br>
                <br>
                <b>Название товара:</b> {{$error['product']['local_name']}}<br>
                <b>Товар ID:</b> {{$error['product']['id']}}<br>
                <b>sku:</b> {{$error['product']['sku']}}<br>
                <b>ean:</b> {{$error['product']['ean']}}<br>
                <b>price_lt:</b> {{$error['product']['price_lt']}}<br>
                <b>price_after_discount_lt:</b> {{$error['product']['price_after_discount_lt']}}<br>
                <b>price_lv:</b> {{$error['product']['price_lv']}}<br>
                <b>price_after_discount_lv:</b> {{$error['product']['price_after_discount_lv']}}<br>
                <b>price_ee:</b> {{$error['product']['price_ee']}}<br>
                <b>price_after_discount_ee:</b> {{$error['product']['price_after_discount_ee']}}<br>
                <b>price_fi:</b> {{$error['product']['price_fi']}}<br>
                <b>price_after_discount_fi:</b> {{$error['product']['price_after_discount_fi']}}<br>
                <b>stock:</b> @if($error['product']['stock']) {{$error['product']['stock']}} @else 0 @endif<br>
                <b>collection_hours:</b> {{$error['product']['collection_hours']}}<br>
                <br>
                <br>
        @endif
    @endforeach
</p>