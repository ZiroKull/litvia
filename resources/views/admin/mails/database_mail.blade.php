<p>{{__('admin_mails.mail_xml_text')}}</p>
<p>
    @foreach ($errors as $error)
        @if (!empty($error['product']))
            <b>Ошибка:</b> <br>
            {!!$error['error']!!} <br>
            <br>
            <b>Название продукта:</b> {{$error['product']['local_name']}}<br>
            <b>Локальное SKU продукта:</b> {{$error['product']['local_sku']}}<br>
            <b>Страна продукта:</b> {{$error['product']['type']}}<br>
            <b>minimal_price:</b> {{$error['product']['minimal_price']}}<br>
            <b>target_price:</b> {{$error['product']['target_price']}}<br>
            <b>price_without_discount:</b> {{$error['product']['price_without_discount']}}<br>
            <b>selling_price:</b> {{$error['product']['selling_price']}}<br>
            <b>formula_set_price:</b> {{$error['product']['formula_set_price']}}<br>
            <br>
        @endif
    @endforeach
</p>