@extends('adminlte::page')

@section('title', __('admin_before_update.beforePurchase'))

@section('content_header')
    <h1>{{__('admin_before_update.beforePurchase')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        @if(Session::has('message'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="table-container">
                        <table id="before-purchase-table" class="table table-bordered table-hover dataTable"
                               style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="tooltipable"><span id="before_purchase_local_vendor_name">{{__('admin_before_update.local_vendor_name')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_large_group">{{__('admin_before_update.large_group')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_smallest_group">{{__('admin_before_update.smallest_group')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_local_product_name">{{__('admin_before_update.local_product_name')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_balance_pigu_stock">{{__('admin_before_update.balance_pigu_stock')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_enough_x_days">{{__('admin_before_update.enough_x_days')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_average_sales_per_day">{{__('admin_before_update.average_sales_per_day')}}</span></th>
                                    <th class="tooltipable"><span id="before_purchase_average_calculation_started_from">{{__('admin_before_update.average_calculation_started_from')}}</span></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead input"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
     <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/before_purchase/before_purchase.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop

