@extends('adminlte::page')

@section('title', 'База данных')

@section('content_header')
    <h1>{{__('admin_products.database')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        @if(Session::has('message'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="table-container">
                            <table id="products-table" class="table table-bordered table-hover dataTable"
                                   style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="tooltipable"><span id="products_b1_id">{{__('admin_products.b1_id')}}</span></th>
                                    {{-- <th class="tooltipable"><span id="products_change_user_name">{{__('admin_products.change_user_name')}}</span></th>
                                    <th class="tooltipable"><span id="products_change_user_date">{{__('admin_products.change_user_date')}}</span></th>
                                    <th class="tooltipable"><span id="products_read_user_name">{{__('admin_products.read_user_name')}}</span></th>
                                    <th class="tooltipable"><span id="products_date_time_read">{{__('admin_products.date_time_read')}}</span></th> --}}
                                    <th class="tooltipable"><span id="products_product_name">{{__('admin_products.product_name')}}</span></th>
                                    <th class="tooltipable"><span id="products_cost_per_documents">{{__('admin_products.cost_per_documents')}}</span></th>
                                    <th class="tooltipable"><span id="products_balance_of_documents">{{__('admin_products.balance_of_documents')}}</span></th>
                                    <th class="tooltipable"><span id="products_local_sku">{{__('admin_products.local_sku')}}</span></th>
                                    <th class="tooltipable"><span id="products_local_name">{{__('admin_products.local_name')}}</span></th>
                                    <th class="tooltipable"><span id="products_pigu_barcode">{{__('admin_products.pigu_barcode')}}</span></th>
                                    <th class="tooltipable"><span id="products_pigu_product_count">{{__('admin_products.pigu_product_count')}}</span></th>
                                    {{-- <th></th> --}}
                                </tr>
                                <tr>
                                    <th class="filterhead input"></th>
                                    {{--<th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>--}}
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    {{-- <th></th> --}}
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/products/products.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop