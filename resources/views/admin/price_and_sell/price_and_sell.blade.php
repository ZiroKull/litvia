@extends('adminlte::page')

@section('title', __('admin_price_and_sell.price_and_sell'))

@section('content_header')
    <h1>{{__('admin_price_and_sell.price_and_sell')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        @if(Session::has('message'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="table-container">
                            <table id="price-and-sell-table" class="table table-bordered table-hover dataTable"
                                   style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="tooltipable" style="min-width: 250px"><span id="price_and_sell_local_name">{{__('admin_products.local_name')}}</span></th>
                                    <th style="min-width: 50px"></th>
                                    <th class="tooltipable" style="min-width: 100px"><span id="price_and_sell_minimal_price">{{__('admin_price_and_sell.minimal_price')}}</span></th>
                                    <th class="tooltipable" style="min-width: 100px"><span id="price_and_sell_target_price">{{__('admin_price_and_sell.target_price')}}</span></th>
                                    <th class="tooltipable" style="min-width: 100px"><span id="price_and_sell_price_without_discount">{{__('admin_price_and_sell.price_without_discount')}}</span></th>
                                    <th class="tooltipable" style="min-width: 100px"><span id="price_and_sell_selling_price">{{__('admin_price_and_sell.selling_price')}}</span></th>
                                    <th class="tooltipable"><span id="price_and_sell_competitors_count">{{__('admin_price_and_sell.competitors_count')}}</span></th>
                                    <th class="tooltipable"><span id="price_and_sell_xml">{{__('admin_price_and_sell.xml')}}</span></th>
                                    <th class="tooltipable"><span id="price_and_sell_delivery_date">{{__('admin_price_and_sell.delivery_date')}}</span></th>
                                    <th class="tooltipable" style="min-width: 100px"><span id="price_and_sell_xml_count">{{__('admin_price_and_sell.xml_count')}}</span></th>
                                    <th class="tooltipable"><span id="price_and_sell_less_8_euro">{{__('admin_price_and_sell.less_8_euro', ['sellPriceVal' => $sellPriceVal->value])}}</span></th>
                                    <th class="tooltipable"><span id="price_and_sell_xml_located">{{__('admin_price_and_sell.xml_located')}}</span></th>
                                    <th class="tooltipable"><span id="price_and_sell_warning">{{__('admin_price_and_sell.warning')}}</span></th>
                                </tr>
                                <tr>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead select"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead select"></th>
                                    <th class="filterhead select"></th>
                                    <th class="filterhead input"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/price_and_sell/price_and_sell.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop