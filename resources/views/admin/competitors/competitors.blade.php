@extends('adminlte::page')

@section('title', __('admin_competitors.competitors'))

@section('content_header')
    <h1>{{__('admin_competitors.competitors')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="callout callout-danger">
                    <h4>Ошибка</h4>
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title"></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                            {!! Form::open(['route' => ['competitors.store'], 'class'=>'competitors']) !!}
                            <div class="row">
                                <div class="col-lg-3">
                                        {!! Form::label('name', __('admin_competitors.loc_sku_name')) !!}
                                        <i class="far fa-fw fa-question-circle notification-con" id="competitors_noti_loc_sku_name"></i>
                                        <div class="name-container">
                                            {!! Form::text('local_name', \Session::get('local_name') , ['class' => 'form-control', 'placeholder'=>  __('admin_competitors.loc_sku_name'), 'id'=>'local-name', 'autocomplete' => 'off']) !!}
                                            <button type="button" class="btn bg-transparent field-button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                </div>
                                <div class="col-lg-3">
                                    {!! Form::label('name', __('admin_competitors.competitor_url')) !!}
                                    <i class="far fa-fw fa-question-circle notification-con" id="competitors_noti_competitor_url"></i>
                                    {!! Form::text('url', NULL , ['class' => 'form-control', 'placeholder'=>  __('admin_competitors.competitor_url_placeholder'), 'id'=>'url' , 'autocomplete' => 'off']) !!}
                                </div>
                                <div class="col-lg-1">
                                     {!! Form::label('name', __('admin_price_and_sell.minimal_price')) !!}
                                     <i class="far fa-fw fa-question-circle notification-con" id="competitors_noti_minimal_price"></i>
                                     {!! Form::text('minimal_price', number_format(0 , 2)  , ['readonly' => true, 'class' => 'form-control', 'id'=>'minimal_price']) !!}
                                </div>
                                <div class="col-lg-1">
                                    {!! Form::label('name', __('admin_price_and_sell.target_price')) !!}
                                    <i class="far fa-fw fa-question-circle notification-con" id="competitors_noti_target_price"></i>
                                    {!! Form::text('target_price', number_format(0 , 2) , ['readonly' => true, 'class' => 'form-control', 'id'=>'target_price']) !!}
                                </div>
                                <div class="col-lg-1">
                                    {!! Form::label('name', __('admin_price_and_sell.selling_price')) !!}
                                    <i class="far fa-fw fa-question-circle notification-con" id="competitors_noti_selling_price"></i>
                                    {!! Form::text('selling_price', number_format(0 , 2) , ['readonly' => true, 'class' => 'form-control',  'id'=>'selling_price']) !!}
                                </div>
                                <div class="col-lg-1">
                                    {!! Form::label('name', __('admin_competitors.price_koef')) !!}
                                    <i class="far fa-fw fa-question-circle notification-con" id="competitors_noti_price_koef"></i>
                                    {!! Form::text('coefficient_price', \Session::get('coefficient_price') ? \Session::get('coefficient_price') : number_format(0.9999 , 4) , ['class' => 'form-control numeric-type', 'placeholder'=>  '0.00', 'id'=>'coefficient_price']) !!}
                                </div>
                                <div class="col-lg-1">
                                    <label for="inputEmail4"></label>
                                    {!! Form::submit( __('admin_buttons.submit'), ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        @if(Session::has('error'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.error')}}</h5>
                                    <p>{{ Session::get('error') }}</p>
                                </div>
                            </div>
                        @endif
                        @if(Session::has('message'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="table-container competitors">
                            <table id="competitors-table" class="table table-bordered table-hover dataTable"
                                   style="width: 100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="tooltipable"><span id="competitors_loc_sku_name">{{__('admin_competitors.loc_sku_name')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_minimal_price">{{__('admin_price_and_sell.minimal_price')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_target_price">{{__('admin_price_and_sell.target_price')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_selling_price">{{__('admin_price_and_sell.selling_price')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_country">{{__('admin_competitors.country')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_koef">{{__('admin_competitors.koef')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_competitor_name">{{__('admin_competitors.competitor_name')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_competitor_price">{{__('admin_competitors.competitor_price')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_competitor_hours_delivery">{{__('admin_competitors.competitor_hours_delivery')}}</span></th>
                                    <th class="tooltipable"><span id="competitors_competitor_min_order_amount">{{__('admin_competitors.competitor_min_order_amount')}}</span></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="filterhead"></th>
                                    <th class="filterhead input"></th>
                                    <th style="min-width: 100px" class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/competitors/competitors.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop
