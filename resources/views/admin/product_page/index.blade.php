@extends('adminlte::page')

@section('title', 'Информация о продукте')

@section('content_header')
    <h1>{{__('admin_products.product_details')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="callout callout-danger">
                    <h4>Возникли ошибки</h4>
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('message'))
                <div class="col-md-12 p-0 mt-2">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::model($product, ['route' => ['productPage.update', $product->id], 'method' => 'patch']) !!}
                    <div class="row">
                        <label class="col-6 col-md-3 control-label">
                            Находится в мельчайшей группе:
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="product_page_noti_products"></i>
                        </label>
                        <div class="col-6 col-md-4">
                            <a href="{!! route('productLinks.show', ['id' => $product->product_id]) !!}">{!! $product->product_name; !!}</a>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-6 col-md-3 control-label">
                            {{__('admin_products.local_sku')}}:
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="product_page_noti_local_sku"></i>
                        </label>
                        <div class="col-6 col-md-4">
                            {!! $product->local_sku; !!}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-6 col-md-3 control-label">
                            {{__('admin_products.pigu_barcode')}}:
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="product_page_noti_pigu_barcode"></i>
                        </label>
                        <div class="col-6 col-md-4">
                            {!! is_array($product->pigu_barcode) ? implode($product->pigu_barcode)->implode(', ') : $product->pigu_barcode !!}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-6 col-md-3 control-label">
                            {{__('admin_products.pigu_product_count')}}:
                            <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="product_page_noti_pigu_product_count"></i>
                        </label>
                        <div class="col-6 col-md-4">
                            {!! $product->pigu_product_count !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 col-md-3">
                            <label class="control-label">
                                {{__('admin_products.local_name')}}:
                                <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1 d-inline" id="product_page_noti_local_name"></i>
                            </label>
                        </div>
                        <div class="col-4 col-md-3">
                            <span class="info">{!! $product->local_name !!}</span>
                            <button class="btn btn-primary edit btn-sm ml-2" type="button">
                                <i class="fas fa-edit"></i>
                            </button>
                            {!! Form::hidden('local_name', null,  ['class' => 'form-control form-edit','placeholder'=> __('admin_products.local_name')]) !!}
                        </div>
                        <div class="col-2 submit" style="display: none;">
                            {!! Form::submit( __('admin_buttons.save'), ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];

        $('.edit').click(function () {
            $(this).hide();
            $('.info').hide();
            $('.submit').show();
            $('.form-edit').attr('type', 'text');
        });
    </script>
@stop
