@extends('adminlte::page')

@section('title', 'Модуль комиссии Пигу')

@section('content_header')
    <h1>{{__('admin_pigu_fees.title')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="col-md-12 p-0 mt-2">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            @endif
            @if ($errors->any())
                <div class="callout callout-danger">
                    <h4>Ошибка</h4>
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title"></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => ['piguFee.store'], 'class'=>'competitors']) !!}
                    <div class="row">
                        <div class="col-lg-3">
                            {!! Form::label('category_number', __('admin_pigu_fees.category_number')) !!}
                            <i class="far fa-fw fa-question-circle notification-con" id="pigu_fees_noti_category_number"></i>
                            {!! Form::text('category_number', NULL , ['class' => 'form-control int-type', 'placeholder'=>  __('admin_pigu_fees.category_number'), 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="col-lg-1">
                            <label for="inputEmail4"></label>
                            {!! Form::submit( __('admin_buttons.submit'), ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="card col-md-8">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 p-0 mt-2">
                                <table id="pigu-fees-table" class="table table-bordered table-hover dataTable"
                                       style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th class="tooltipable"><span id="category_number">{{__('admin_pigu_fees.category_number')}}</span></th>
                                        <th class="tooltipable"><span id="category_name">{{__('admin_pigu_fees.category_name')}}</span></th>
                                        <th class="tooltipable"><span id="pigu_fee">{{__('admin_pigu_fees.pigu_fee')}}</span></th>
                                    </tr>
                                    <tr>
                                        <th class="filterhead input"></th>
                                        <th class="filterhead input"></th>
                                        <th class="filterhead filter input"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/pigu_fees/pigu_fees.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop
