@extends('adminlte::page')

@section('title', __('admin_import.update_b1_and_pigu'))

@section('content_header')
    <h1>{{__('admin_import.update_b1_and_pigu')}}</h1>
@stop

@section('content')
    @if(Session::has('error'))
        <div class="col-md-12 p-0 mt-2">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.error')}}</h5>
                <p>{{ Session::get('error') }}</p>
            </div>
        </div>
    @endif
    @if(Session::has('message'))
        <div class="col-md-12 p-0 mt-2">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-md-3 pl-0">
                    {{__('admin_import.update_b1_and_pigu')}} <i class="far fa-fw fa-question-circle" id="import_update"></i>
                </div>
                <div class="col-md-2 align-middle">
                    @if(!empty($b1ImportTime->updated_at))
                        {{ $b1ImportTime->updated_at }}
                    @else
                        -
                    @endif
                </div>
                <div class="col-md-4 p-0">
                    <a href="{{ url('admin/import/importB1') }}" class="btn btn-primary">{{__('admin_import.update_now')}}</a>
                </div>
            </div>
            <div class="row mt-2 align-items-center">
                <div class="col-md-3 pl-0">
                    {{__('admin_import.date_time_download_from_pigu')}}  <i class="far fa-fw fa-question-circle" id="import_date_time_pigu"></i>
                </div>
                <div class="col-md-2">
                    @if(!empty($piguImportTime->updated_at))
                        {{ $piguImportTime->updated_at }}
                    @else
                        -
                    @endif
                </div>
                <div class="col-md-4 p-0">
                    <a href="{{ url('admin/import/importPigu') }}" class="btn btn-primary">{{__('admin_import.update_now')}}</a>
                </div>
            </div>
            <div class="row mt-2 align-items-center">
                <div class="col-md-3 pl-0">
                    {{__('admin_import.competitor_price_updates')}} <i class="far fa-fw fa-question-circle" id="import_competitor_price_updates"></i>
                </div>
                <div class="col-md-2">
                    @if(!empty($updateCompetitors->updated_at))
                        {{ $updateCompetitors->updated_at }}
                    @else
                        -
                    @endif
                </div>
                <div class="col-md-4 p-0">
                    {{-- <a href="{{ url('admin/import/importCompetitorsPrices') }}" class="btn btn-primary">Обновить сейчас</a> --}}
                </div>
            </div>
            <div class="row mt-2 align-items-center">
                <div class="col-md-3 pl-0">
                    {{__('admin_import.update_sale_price_in_pigu')}} <i class="far fa-fw fa-question-circle" id="import_update_sale_price_in_pigu"></i>
                </div>
                <div class="col-md-2">
                    @if(!empty($generateXml->updated_at))
                        {{ $generateXml->updated_at }}
                    @else
                        -
                    @endif
                </div>
                <div class="col-md-2 p-0">
                    <a href="{{ url('admin/import/generateXml') }}" class="btn btn-primary">{{__('admin_import.update_now')}}</a>
                </div>
                <div class="col-md-4 price-con">
                    {{ Form::label('set_price_to_limit', __('admin_settings.set_price_to_limit'), ['class' => 'control-label']) }}
                    {!! Form::checkbox('settings[set_price_to_limit]', 1 , ($setPriceToLimit->value) ? true : false , ['id'=>'set_price_to_limit']) !!}
                    <i class="far fa-fw fa-question-circle" id="import_set_price_to_limit"></i>
                </div>
            </div>
            <div class="row mt-2 align-items-center">
                <div class="col-md-3 pl-0">
                    {{__('admin_import.competitor_sales_statistics')}} <i class="far fa-fw fa-question-circle" id="import_competitor_sales_statistics"></i>
                </div>
                <div class="col-md-2">

                </div>
                <div class="col-md-4 p-0">
                    <a href="{{ url('admin/import/generateDownloadXls') }}" class="btn btn-primary">{{__('admin_import.download_excel_button')}}</a>
                    {{-- <a href="{{ url('admin/import/clearParserTable')  }}" class="btn btn-primary">{{__('admin_import.clear_database_button')}}</a> --}}
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/import/import.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop
