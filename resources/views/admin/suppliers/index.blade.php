@extends('adminlte::page')

@section('title', 'Поставщики')

@section('content_header')
    <h1>{{__('admin_suppliers.suppliers')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 p-0 mt-2 mb-4 d-flex flex-row-reverse">
                            <a href="{!! route('suppliers.create') !!}" class="btn btn-primary">
                                Добавить нового поставщика
                            </a>
                        </div>
                        <div class="col-md-12 p-0 mt-2">
                            <table id="suppliers-table" class="table table-bordered table-hover dataTable"
                                   style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="tooltipable"><span id="local_company_name">{{__('admin_suppliers.local_company_name')}}</span></th>
                                    <th class="tooltipable"><span id="legal_company_name">{{__('admin_suppliers.legal_company_name')}}</span></th>
                                    <th class="tooltipable"><span id="manager_name">{{__('admin_suppliers.manager_name')}}</span></th>
                                    <th class="tooltipable"><span></span></th>
                                </tr>
                                <tr>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead input"></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/suppliers/suppliers.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop
