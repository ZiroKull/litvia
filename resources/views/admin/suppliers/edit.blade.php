@extends('adminlte::page')

@section('title', 'Редактирование Поставщики')

@section('content_header')
    <h1>{{__('admin_suppliers.edit_new_suppliers')}}</h1>
@stop

@section('content')
    {!! Form::hidden('id', $supplier->id, ['class' => 'supplier_id']) !!}
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="callout callout-danger">
                    <h4>Возникли ошибки</h4>
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('message'))
                <div class="col-md-12 p-0 mt-2">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::model($supplier, ['route' => ['suppliers.update', $supplier->id], 'method' => 'patch']) !!}
                    <div class="row mb-3">
                        {!! Form::submit( __('admin_buttons.save'), ['class' => 'btn btn-primary']) !!}
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('local_company_name', __('admin_suppliers.local_company_name'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_local_company_name"></i>
                        <div class="col-4">
                            {!! Form::text('local_company_name', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.local_company_name'),'readonly']) !!}
                        </div>
                        <div class="col-1">
                            <button class="btn btn-primary edit" type="button">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('legal_company_name', __('admin_suppliers.legal_company_name'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_legal_company_name"></i>
                        <div class="col-4">
                            {!! Form::text('legal_company_name', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.legal_company_name'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('manager_name', __('admin_suppliers.manager_fio'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_manager_fio"></i>
                        <div class="col-4">
                            {!! Form::text('manager_name', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.manager_fio'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('manager_phone', __('admin_suppliers.manager_phone'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_manager_phone"></i>
                        <div class="col-4">
                            {!! Form::text('manager_phone', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.manager_phone'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('manager_email', __('admin_suppliers.manager_email'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_manager_email"></i>
                        <div class="col-4">
                            {!! Form::text('manager_email', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.manager_email'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('chat_link', __('admin_suppliers.chat_link'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_chat_link"></i>
                        <div class="col-4">
                            {!! Form::text('chat_link', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.chat_link'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('shop_link', __('admin_suppliers.shop_link'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_shop_link"></i>
                        <div class="col-4">
                            {!! Form::text('shop_link', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.shop_link'),'readonly']) !!}
                        </div>
                    </div>

                    <div class="form-group row ">
                        {!! Form::label('warehouse_name', __('admin_suppliers.warehouse_name'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_warehouse_name"></i>
                        <div class="col-4">
                            {!! Form::text('warehouse_name', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.warehouse_name'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('warehouse_phone', __('admin_suppliers.warehouse_phone'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_warehouse_phone"></i>
                        <div class="col-4">
                            {!! Form::text('warehouse_phone', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.warehouse_phone'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('warehouse_email', __('admin_suppliers.warehouse_email'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_warehouse_email"></i>
                        <div class="col-4">
                            {!! Form::text('warehouse_email', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.warehouse_email'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('warehouse_adress', __('admin_suppliers.warehouse_adress'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_warehouse_adress"></i>
                        <div class="col-4">
                            {!! Form::text('warehouse_adress', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.warehouse_adress'),'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('export_license', __('admin_suppliers.export_license'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_export_license"></i>
                        <div class="col-4">
                            <label>
                                {!! Form::checkbox('export_license[]', 1,  $supplier->export_license === 1) !!} Есть
                            </label>
                            <br/>
                            <label>
                                {!! Form::checkbox('export_license[]', 0,  $supplier->export_license === 0) !!} Нету
                            </label>
                        </div>
                    </div>
                    <div class="row mt-3">
                        {!! Form::submit( __('admin_buttons.save'), ['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 p-0 mt-2 mb-4">
                            <button class="btn btn-primary open-add-smollest-group">
                                {{__('admin_suppliers.add_products')}}
                            </button>
                        </div>
                        <div class="col-md-12 p-0 mt-2">
                            <table id="supplier-links-table" class="table table-bordered table-hover dataTable"
                                   style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="tooltipable"><span id="suppliers_noti_products">{{__('admin_suppliers.products')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_supplier_quality">{{__('admin_suppliers.supplier_quality')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_rohs">{{__('admin_suppliers.rohs')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_ce">{{__('admin_suppliers.ce')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_comments">{{__('admin_suppliers.comments')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_quantity_per_carton">{{__('admin_suppliers.quantity_per_carton')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_carton_volume_m3">{{__('admin_suppliers.carton_volume_m3')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_carton_weight_kg">{{__('admin_suppliers.carton_weight_kg')}}</span></th>
                                    <th class="tooltipable"><span id="suppliers_noti_is_accurate">{{__('admin_suppliers.is_accurate')}}</span></th>
                                </tr>
                                <tr>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead select selectOptionsQuality"></th>
                                    <th class="filterhead"></th>
                                    <th class="filterhead"></th>
                                    <th class="filterhead input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead filter input"></th>
                                    <th class="filterhead select selectOptions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/supplier_links/supplier_links.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];

        $(document).ready(function () {
            $('input[type="checkbox"]').on('change', function() {
                $('input[type="checkbox"]').not(this).prop('checked', false);
            });

            $('.edit').click(function () {
                let input = $(this).parent().prev().find(':input');
                let readonly = input.prop('readonly');

                $('input').prop('readonly', !readonly)
            });
        });
    </script>
@stop
