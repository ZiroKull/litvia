@extends('adminlte::page')

@section('title', 'Добавление нового Поставщики')

@section('content_header')
    <h1>{{__('admin_suppliers.add_new_suppliers')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="callout callout-danger">
                    <h4>Возникли ошибки</h4>
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['route' => 'suppliers.store']) !!}
                    <div class="row mb-3">
                        {!! Form::submit( __('admin_buttons.submit'), ['class' => 'btn btn-primary']) !!}
                    </div>
                    <div class="form-group row ">
                        {!! Form::label('local_company_name', __('admin_suppliers.local_company_name'), ['class' => 'col-2 control-label text-right']) !!}
                        <i class="far fa-fw fa-question-circle notification-con mr-2 mt-1" id="suppliers_noti_local_company_name"></i>
                        <div class="col-4">
                            {!! Form::text('local_company_name', null,  ['class' => 'form-control','placeholder'=> __('admin_suppliers.local_company_name')]) !!}
                        </div>
                    </div>
                    <div class="row mt-3">
                        {!! Form::submit( __('admin_buttons.submit'), ['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop
