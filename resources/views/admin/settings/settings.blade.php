@extends('adminlte::page')

@section('title', __('admin_settings.global_settings'))

@section('content_header')
    <h1>{{__('admin_settings.settings')}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin_settings.sale_price_delivery_date')}} <i
                            class="far fa-fw fa-question-circle" id="settings_other_sale_price_delivery_date"></i></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        @if(Session::has('message'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.sell_and_price_x_val')}} <i
                                                class="far fa-fw fa-question-circle"
                                                id="settings_other_sell_and_price_x_val"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[sell_and_price_x_val]', (isset($valArray['sell_and_price_x_val'])) ? $valArray['sell_and_price_x_val'] : null  , ['class' => 'form-control numeric-type', 'placeholder'=>  '8', 'id'=>'sell_and_price_x_val']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label
                                            class="control-label">{{__('admin_settings.minimal_x_sell_product_price')}}
                                            <i class="far fa-fw fa-question-circle"
                                               id="settings_other_minimal_x_sell_product_price"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[minimal_x_sell_product_price]', (isset($valArray['minimal_x_sell_product_price'])) ? $valArray['minimal_x_sell_product_price'] : null , ['class' => 'form-control numeric-type', 'placeholder'=>  '2.98', 'id'=>'minimal_x_sell_product_price']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button(  __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.email_send_sell_product')}} <i
                                                class="far fa-fw fa-question-circle"
                                                id="settings_other_email_send_sell_product"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[email_send_sell_product]', (isset($valArray['email_send_sell_product'])) ? $valArray['email_send_sell_product'] : null , ['class' => 'form-control', 'placeholder'=>  'm.pavel0529@gmail.com', 'id'=>'email_send_sell_product']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button(  __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.creating_xml_specify_x')}} <i
                                                class="far fa-fw fa-question-circle"
                                                id="settings_other_creating_xml_specify_x"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[creating_xml_specify_x]', (isset($valArray['creating_xml_specify_x'])) ? $valArray['creating_xml_specify_x'] : null , ['class' => 'form-control numeric-type', 'placeholder'=>  '1.5', 'id'=>'creating_xml_specify_x']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button(  __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label
                                            class="control-label">{{__('admin_settings.calculated_number_of_days_y')}}
                                            <i class="far fa-fw fa-question-circle"
                                               id="settings_other_calculated_number_of_days_y"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[calculated_number_of_days_y]', (isset($valArray['calculated_number_of_days_y'])) ? $valArray['calculated_number_of_days_y'] : null , ['class' => 'form-control int-type', 'placeholder'=>  '5', 'id'=>'calculated_number_of_days_y']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button(  __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <!--<div class="card-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin_settings.competitor_price_recalc')}} <i
                            class="far fa-fw fa-question-circle" id="settings_other_competitor_price_recalc"></i></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        @if(Session::has('message'))
                            <div class="col-md-12 p-0 mt-2">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h5><i class="icon fas fa-check"></i>{{__('admin_validation_errors.info')}}</h5>
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.more_than_x_hours')}} <i
                                                class="far fa-fw fa-question-circle"
                                                id="settings_other_more_than_x_hours"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[more_than_x_hours]', (isset($valArray['more_than_x_hours'])) ? $valArray['more_than_x_hours'] : null , ['class' => 'form-control int-type', 'placeholder'=>  '96', 'id'=>'more_than_x_hours']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.customer_pays_x_euros')}} <i
                                                class="far fa-fw fa-question-circle"
                                                id="settings_other_customer_pays_x_euros"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[customer_pays_x_euros]', (isset($valArray['customer_pays_x_euros'])) ? $valArray['customer_pays_x_euros'] : null , ['class' => 'form-control numeric-type', 'placeholder'=>  '0.3', 'id'=>'customer_pays_x_euros']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.divide_by_x_specify_x')}} <i
                                                class="far fa-fw fa-question-circle"
                                                id="settings_other_divide_by_x_specify_x"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[divide_by_x_specify_x]', ($valArray['divide_by_x_specify_x']) ? $valArray['divide_by_x_specify_x'] : null , ['class' => 'form-control int-type', 'placeholder'=>  '4', 'id'=>'divide_by_x_specify_x']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.competitor_price_war')}} <i
                                                    class="far fa-fw fa-question-circle"
                                                    id="settings_other_competitor_price_war"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[competitor_price_war]', (isset($valArray['competitor_price_war'])) ? $valArray['competitor_price_war'] : null , ['class' => 'form-control int-type', 'placeholder'=>  '14', 'id'=>'competitor_price_war']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{__('admin_settings.competitor_price_war_min')}} <i
                                                    class="far fa-fw fa-question-circle"
                                                    id="settings_other_competitor_price_war_min"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[competitor_price_war_min]', (isset($valArray['competitor_price_war_min'])) ? $valArray['competitor_price_war_min'] : null , ['class' => 'form-control int-type', 'placeholder'=>  '3', 'id'=>'competitor_price_war_min']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                        <!--<div class="card-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>-->
                    </div>

                </div>
            </div>
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin_settings.calculation_average_sales')}} <i
                            class="far fa-fw fa-question-circle" id="settings_other_calculation_average_sales"></i></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label
                                            class="control-label">{!!__('admin_settings.days_information_when_calculating_average_sales')!!}
                                            <i class="far fa-fw fa-question-circle"
                                               id="settings_other_days_information_when_calculating_average_sales"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[calculation_average_sales]', (isset($valArray['calculation_average_sales'])) ? $valArray['calculation_average_sales'] : 30 , ['class' => 'form-control int-type', 'placeholder'=>  '30', 'id'=>'calculation_average_sales']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin_settings.other_settings')}} <i
                            class="far fa-fw fa-question-circle" id="settings_other_other_settings"></i></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label
                                            class="control-label">{!!__('admin_settings.backup_email')!!}
                                            <i class="far fa-fw fa-question-circle"
                                               id="settings_other_backup_email"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[backup_email]', (isset($valArray['backup_email'])) ? $valArray['backup_email'] : '' , ['class' => 'form-control', 'placeholder'=>  'test@example.com', 'id'=>'backup_email']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label
                                                class="control-label">{!!__('admin_settings.price_history_email')!!}
                                            <i class="far fa-fw fa-question-circle"
                                               id="settings_other_price_history_email"></i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        {!! Form::text('settings[price_history_email]', (isset($valArray['price_history_email'])) ? $valArray['price_history_email'] : '' , ['class' => 'form-control', 'placeholder'=>  'test@example.com', 'id'=>'price_history_email']) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::button( __('admin_buttons.save'), ['class' => 'btn save-setting btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}">
@stop

@section('js')
    <script type="application/javascript" src="{{ asset('js/admin/settings/settings.js') }}"></script>
    <script>
        let tooltips = [{!! json_encode($tooltipsJs, JSON_HEX_TAG) !!}];
    </script>
@stop
